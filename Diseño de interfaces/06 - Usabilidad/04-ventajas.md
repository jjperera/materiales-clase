# Ventajas
Algunas ventajas que tiene que nuestro sitio web sea usable:

1. La usabilidad permite mayor rapidez en la realización de tareas y reduce las pérdidas de tiempo.
2. El aprendizaje del manejo de la web es mucho más rápido e intuituvo, y el usuario enseguida se familiariza con la página, haciendo nuestros productos o servicios más visibles.
3. Los visitantes se sienten más seguros y necesitan menos ayuda por parte del soporte técnico de la web, con la consiguiente reducción de costes y esfuerzos y una percepción más positiva de la visitas.
4. Reducción de los costes de aprendizaje.
5. Optimización de los costes de diseño, rediseño y mantenimiento.
6. Disminución de los costes de ayuda al usuario.
7. Disminución en la tasa de errores cometidos por el usuario.
8. Aumento de la tasa de conversión de visitantes a clientes.
9. Aumento de la satisfacción, comodidad y facilidad de uso del usuario.
10. Mejora la imagen y el prestigio de la empresa.

