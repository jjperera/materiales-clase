# Evaluación
La evaluación es un proceso continuo que se realiza a lo largo de todo el desarrollo del proyecto. De esta forma, cuando se realiza antes de la creación del sitio web tien una función preventiva, durante el desarrollo sirve para ir corrigiendo problemas y cuando se realiza al final tiene función de validación.

Se pueden utilizar diferentes herramientas para ayudar a la validacion

## Herramientas para conocer al usuario
La forma más sencilla en que podemos obtener feedback es preguntar directamente a los usuarios. Para esto se pueden crear cuestionarios, comentarios o sugerencias. Esto es algo que se puede recoger mediante correos electrónicos o con algunas herramientas para crear encuentas. 

## Herramientas para crear y verificar prototipos
Algunas herramientas que se pueden utilizar para crear prototipos son:

- Microsoft Visio
- OmniGraffle
- Mockflow (dispone de versión gratuita)
- Balsamiq Mockups
- Axure
- Pencil. Es de código abierto. Se instala en el equipo.

En cuanto a herramientas para evaluar prototipos disponemos por ejemplo de navflow donde se puede subir un diseño para que los usuarios realicen un test. Posteriormente se genera un informe con el resultado. 

## Herramientas de categorización de contenido
Disponemos de algunas herramientas que permiten hacer cardsorting. Algunas de ellas serían:

- OptimalSort
- Websort
- xSort

## Herramientas de mapas de calor y analíticas web
Los mapas de calor son el resultado de superponer las evaluaciones de varios usuarios que han utilizando eye-tracking o click-tracking. Algunas herramientas que pueden utilizarse son:

- Clickdensity
- ClickHeat
- CrazyEgg
- Lucky Orange

En cuanto a las herramientas de analítica web, recolectan la mayor cantidad de información posible del comportamiento del usuario. Un ejemplo lo tenemos en Google Analitucs.