# Métodos

## Card sorting
Para realizar una organización de categorías centrada en el usuario, el arquitecto de información dispone de técnicas de ayuda en la toma de decisiones, como es la denominada 'card sorting' u ordenación de tarjetas.

La técnica de 'card sorting' se basa en la observación de cómo los usuarios agrupan y asocian entre sí un número predeterminado de tarjetas etiquetadas con las diferentes categorías temáticas del sitio web.

De esta forma, partiendo del comportamiento de los propios usuarios, es posible organizar y clasificar la información de un sitio web conforme a su modelo mental.

[![Card sorting](https://img.youtube.com/vi/-icR4aA7j84/0.jpg)](https://www.youtube.com/watch?v=-icR4aA7j84)


## Paseos cognitivos
El paseo cognitivo se plantea como una técnica de revisión donde los evaluadores expertos construyen escenarios para las tareas a partir de una especificación o de un prototipo temprano para desempeñar después el papel del usuario trabajando con la interfaz en cuestión (paseando a través de la interfaz). Actúan como si la interfaz estuviera completamente construida y ellos (en el papel del usuario tipo) estuvieran trabajando a través de las tareas que realizan. Se controla cada paso que ha de realizar el usuario: callejones sin salida en los que la interfaz bloquea al usuario y le impide completar su tarea indicarán que algo falta en la interfaz. Caminos complejos y sinuosos a través de las secuencias de funciones indicarán que la interfaz requiere de una nueva función que simplifique la tarea y evite el colapso.

[![Recorrido cognitivo](https://img.youtube.com/vi/3Wjo73W4W2U&t=78s/0.jpg)](https://www.youtube.com/watch?v=3Wjo73W4W2U&t=78s)


## Listas de comprobación
Las guías y las listas de comprobación ayudan a asegurar que los principios de usabilidad sean considerados en un diseño. Normalmente, las listas de comprobación se utilizan en combinación con algún método de inspección de usabilidad, proporcionando al inspector una base con la que comparar el producto.

## Evaluación eurística
Consiste en que uno o varios revisores inspeccionen el sitio web, comprobando si los elementos cumplen los principios de usabilidad. Una persona no experta en usabilidad también puede hacer las comprobaciones ayudado por las pautas de usabilidad de algún experto, como Jakob Nielsen.

Este método permite identificar muchos problemas de usabilidad de poca importancia, pero se detectan pocos problemas importantes, que son los que más afectan al usuario. De esta forma, este método nunca puede sustituir a las pruebas con usuarios.

## Seguimiento visual o eye tracking
Es una prueba empírica que, mediante herramientas hardware y software, permite monitorizar las zonas de una imagen que mira una persona. Está enfocada a analizar la atención visual del usuario y se puede registrar el tiempo durante el que se mira una determinada zona y el orden de visualización.

Esta técnica está recomendada para evaluar una interfaz ya terminada, porque pequeños cambios en el diseño o el color pueden cambiar completamente los resultados.

Existe una técnica llamada **click-tracking**, que es similar al eye-tracking pero hace el seguimiento del puntero del ratón, por lo que resulta mucho más económica.

[![Eye tracking](https://img.youtube.com/vi/ConsSlIf6n4/0.jpg)](https://www.youtube.com/watch?v=ConsSlIf6n4)

## Analítica web
El concepto de analítica web es un conjunto de herramientas y técnicas de investigación que analizan datos de uso de un sitio web. Su gran ventaja es que los resultados no se basan en muestras de unos pocos usuarios sino que se monitoriza a la totalidad de los usuarios de un sitio web. Es una técnica muy fiable y económica.

Permite analizar dónde hacen clic los usuarios, analizar las rutas de navegación de los usuarios, saber en qué campo de un formulario el usuario lo abandona y el vocabulario utilizado al usar el buscador interno del sitio web.

