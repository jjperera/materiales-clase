# Dificultades

- **Aplicación tardía**. En ocasiones se quiere hacer usable un sitio web que ya está completamente terminado. Esto implica unos costes mucho más altos. El proceso de usabilidad debe aplicarse durante todo el desarrollo.
- **Preocupación excesiva por la estética**. Aunque la estética es importante, no deben dejarse de lado los aspectos prácticos, que son los que realmente las hacen útiles.
- **Demasiadas funcionalidades**. Tener muchas funcionalidades no siempre es mejor. Es importante que las funcionalidades más frecuentes o importantes sean fácilmente accesibles.
- **Uso de nuevas tecnologías**. Dejar que los sistemas asuman la parte compleja de las distintas funcionalidades avanzadas, en lugar de los usuarios.
- **Testeo inadecuado**. El proceso de testeo de calidad se aplica igual que en otros parámetros, sin tener en cuenta que la usabilidad es algo especial. La usabilidad es subjetiva en algunos aspectos y es imprescindible contar con la ayuda de los usuarios finales.