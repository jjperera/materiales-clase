# Introducción
Accesibilidad y usabilidad, aunque son conceptos diferentes, van de la mano y están íntimamente relacionados. Un sitio web que es accesible es un buen punto de partida para conseguir que sea usable. 

La usabilidad busca optimizar la facilidad de uso y la facilidad de aprendizaje con la que se usa una herramienta o interfaz. Se aplica en el diseño de cualquier elemento con el que haya que interactuar, puede ser un dispositivo físico, como un electrodoméstico o un teléfono móvil, o puede ser una página web.

Existen técnicas y herramientas que ayudan a conocer a los usuarios y sus necesidades, así como a mejorar y evaluar la usabilidad.

