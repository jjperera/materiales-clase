# Diseño web centrado en el usuario
En el diseño tradicional el diseño es lineal. Partimos de unos requisitos y se finaliza el desarrollo cuando se han cumplido. Durante todo este proceso **la participación del usuario es escasa o nula**. 

Por el contrario, en el **diseño centrado en el usuario**, cuyo objetivo es desarrollar productos que cubran las necesidades de los usuarios consiguiendo que queden satisfechos y se sientan cómodos usándolos, **los usuarios participan durante todo el proceso de desarrollo** y se tienen en cuenta sus opiniones. Cuando se sigue este enfoque, se adapta el uso de nuestra aplicación a la forma de trabajo de los usuarios.

El diseño centrado en el usuario consta de estas etapas:

- **Análisis**. Deberemos conocer los diferentes perfiles de usuario que van a utilizar la aplicación y sus necesidades, capacidades, expectativas y el contexto de utilización de la aplicación. 
- **Conceptualización**. Consiste en organizar el proyecto. A partir de la información recopilada se diseñará todo el producto. Esto puede implicar diseños de la base de datos, mapas del sitio, diseño de pantallas, etc.
- **Prototipado**. Esta etapa implica la creación de un prototipo donde se puedan probar las funcionalidades del sitio web. En esta etapa el objetivo es tener una web funcional aunque no tiene que estar necesariamente completa. Entre las funcionalidades incluydas pueden estar botones, formularios, cajas de búsqueda etc. Por otra parte se hace un diseño del look&feel del sitio web de acuerdo con el libro de la estilo de la marca. 
- **Pruebas de usuario**. Implementado el prototipo, se pasa a las pruebas  con usuarios. En estas pruebas se pueden incluir herramientas como seguimiento de ojos para seguir la vista de los usuarios mientras que usan el sitio. El objetivo de esta etapa sería encontrar problemas en el diseño.
- **Implementación**. Validado el diseño y corregidos los problemas, se pasa al desarrollo e implementación del sitio web. 

Este flujo de trabajo, permite que se puedan encontrar errores en el diseño desde las primeras etapas del desarrollo haciendo que sea más barato que si hay que rediseñar todo el producto final. El proceso va a ser iterativo y en cada iteración se recogen las sugerencias de los usuarios y se identifican problemas.



