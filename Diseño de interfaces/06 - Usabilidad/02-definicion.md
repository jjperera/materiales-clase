# Definición
El término usabilidad no existe como tal en el diccionario, si bien podemos interpretarlo como facilidad de uso. Hace referencia a la facilidad con que se puede utilizar una herramienta.

## Definiciones
Aquí tenemos algunas definiciones:

- Jakob Nielsen está considerado como el padre de la usabilidad, y la define como: *La usabilidad web es el atributo de calidad que mide lo fáciles que son de usar las interfaces de usuario*.
- Según la norma ISO/IEC 9126: **La usabilidad se refiere a la capacidad de un software de ser comprendido, aprendido, usado y ser atractivo para el usuario, en condiciones específicas de uso.**

La usabilidad se refiere a la capacidad de un software de ser comprendido, aprendido, usado y ser atractivo para el usuario, **en condiciones específicas de uso**.

## Componentes de la usabilidad
Según Jakob Nielsen la usabilidad está definida en función de 5 componentes que incluyen en la calidad de un diseño:

1. **Facilidad de aprendizaje**. Que mide lo fácil que resulta llevar a cabo tareas básicas para usuarios que acceden por primera vez.
2. **Eficiencia**. Mide la rapidez con que los usuarios pueden realizar tareas cuando ya están familiarizados con el diseño.
3. **Memorabilidad**. Mide el recuerdo en el tiempo, la facilidad con la que los usuarios pueden volver a utilizar la interfaz cómodamente después de un periodo de no usarla.
4. **Tasa de errores**. Mide la cantidad de errores que cometen los usuarios, lo graves que son y con qué facilidad se puede recuperar el sistema de los errores.
5. **Satisfacción**. Mide lo agradable que es utilizar la interfaz.

Algunos atributos, como el tiempo que se tarda en realizar una tarea pueden medirse de forma objetiva pero otros, como la satisfacción del usuario, son subjetivos y pueden presentar variaciones de un usuario a otro.



