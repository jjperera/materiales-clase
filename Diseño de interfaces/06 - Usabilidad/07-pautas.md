# Pautas para la creación de un sitio web usable

# Pautas de Jakob Nielsen

- **Visibilidad del estado del sistema**. Feedback constante. Se debe mantener informado al usuario permanentemente mediante la retroalimentación adecuada y en un tiempo razonable. 
- **Correspondencia entre el sitio web y el mundo real**. En los contenidos del sitio web se deben usar palabras y conceptos familiares para los usuarios y organizar la información en orden lógico y natural.
- **Libertad y control del usuario**. Los usuarios cometen errores con frecuencia y se les debe proporcionar una forma clara y rápida de salir del estado no deseado. Son útiles las funciones de deshacer y rehacer o volver a la página principal. 
- **Consistencia y estándares**. Los usuarios no deberían dudar de si palabras o acciones diferentes significan lo mismo. Los colores y distribución de contenidos deben ser similares en todo el sitio web. 
- **Prevenir errores**. Hay que tener cuidado al diseñar para evitar errores al usuario, y los mensajes de error deben incluir una confirmación antes de efectuar las correcciones. 
- **Reconocimiento mejor que recuerdo**. Deben estar visibles las opciones necesarias en cada momento haciendo que no sea necesario recordar esa información. 
- **Flexibilidad y eficiencia**. El sistema debe adaptarse para permitir que los usuarios avanzados puedan utilizar el sistema de forma más rápida, pero esto no debe añadir dificultad para los novatos. 
- **Diseño minimalista**. Los diálogos no deben contener información innecesaria que compita por la atención con información necesaria. La presencia de cualquier elemento debe estar justificada. 
- **Ayuda a los usuarios para reconocer, diagnosticar y recuperarse de los errores**. Se debe proporcionar al usuario información en lenguaje sencillo, indicando claramente el error producido y la forma de solucionarlo.
- **Ayuda y documentación**. Cuando sea necesario ofrecer ayuda y documentación, esta debe ser fácil de encontrar, breve, concisa y enfocada a tareas concretas del usuario.

# Pautas de Bruce Tognazzini

- **Anticipación**. Hay que intentar anticiparse a las necesidades de los usuarios.
- **Autonomía y control**. El usuario debe tener el control y poder moverse con autonomía por el sitio web.
- **Precaución usando colores**. El uso del color no debe ser la única forma de presentar la información. Se deben incluir otros elementos pensando en usuarios que no distinguen los colores.
- **Consistencia**. Hay que ser consistente con los conocimientos previos y las expectativas del usuario.
- **Uso de valores por defecto**. Cuando tenga sentido, permitiendo cambiar su configuración con facilidad.
- **Eficiencia del usuario**. Hay que centrarse en la productividad del usuario.
- **Interfaces explorables que den libertad al usuario y reversibilidad**. Se debe permitir que el usuario deshaga acciones realizadas.
- **Ley de Fitts**. Cuando menos distancia haya que recorrer y mayor tamaño tenga un elemento, más fácil será interactuar con él.
- **Uso de estándares** y elementos familiares en la interfaz.
- **Reducción del tiempo de espera**. Se debe minimizar el tiempo de espera y mantener al usuario informado del tiempo que falta.
- **Minimizar el aprendizaje**. El aprendizaje necesario debe ser mínimo y el sitio web debe poder usarse desde el primer momento.
- **Uso adecuado de metáforas**. Con su uso, siempre que sean apropiadas, se mejora la comprensión.
- **Protección del trabajo de los usuarios**. Hay que asegurar que el trabajo de los usuarios no se pierda por causa de un error.
- **Legibilidad**. Hay que favorecer la legibilidad mediane el tamaño de fuente adecuado y suficiente contraste entre texto y fondo.
- **Seguimiento de las acciones de usuario**. Hay que guardar información sobre los usuarios para posteriormente permitir que las acciones que realiza con más frecuencia se puedan realizar más rápido.
- **Navegación visible**. Hay que evitar, o reducir al máximo, los elementos de navegación invisibles y presentarlos de forma clara.


