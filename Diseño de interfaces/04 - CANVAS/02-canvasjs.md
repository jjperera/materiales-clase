# Introducción
El canvas es un componente que permite llevar al cliente la creación de diversos tipos de contenido que anteriormente debían renderizarse en el servidor. Esto permite desarrolar APIs como la que nos ocupa que permite traspasar la creación de gráficos con datos a la parte cliente de nuestra aplicación web. De este modo, a parte de pasar carga de trabajo al cliente
y crear gráficos interactivos, se reduce el volumen de datos que hay que intercambiar entre cliente y servidor.

EL API al que hacemos referencia es este:

- https://canvasjs.com/


# Importar a nuestro proyecto
Lo primero será importar el API a nuestro proyecto. En el directorio de nuestro proyecto podremos utilizar la siguiente línea de comandos.

```cmd
npm install --prefix=. canvasjs
```


# Insertar nuestro gráfico
Este ejemplo lo he sacado directametne de la web del API aquí podemos ver la simplicidad del API
a la hora de definir las características de nuestro gráfico.

Partiendo de este sencillo ejemplo, experimenta y crea diferentes gráficos.

```html
<!DOCTYPE HTML>
<html>
    <head>

        <script src="node_modules/canvasjs/dist/canvasjs.min.js" defer> </script>

        <script type="text/javascript">
            window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                theme: "light1", // "light2", "dark1", "dark2"
                animationEnabled: false, // change to true		
                title:{
                    text: "Basic Column Chart"
                },
                data: [
                {
                    // Change type to "bar", "area", "spline", "pie",etc.
                    type: "column",
                    dataPoints: [
                        { label: "apple",  y: 100  },
                        { label: "orange", y: 15  },
                        { label: "banana", y: 25  },
                        { label: "mango",  y: 30  },
                        { label: "grape",  y: 28  }
                    ]
                }
                ]
            });
            chart.render();

            }
        </script>
    </head>
    <body>
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    </body>
</html>
```

# Generar gráficos a partir de la base de datos
Vamos a hacer algunos gráficos partiendo de datos en el servidor para que veamos como podríamos llevar a cabo el proceso.
