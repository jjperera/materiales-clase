# Tecnologías donde la accesibilidad es aplicable

## HTML
Cubre la estructura y contenido del documento. La accesibilidad se puede aplicar simplemente:
- Utilizando las etiquetas html correctas.
- Utilizar elementos article, header, section, footer, etc frente a otros genéricos como div.
- Haciendo un uso adecuado de elementos y atributos. Por ejemplo, disponemos de los siguientes atributos:
  - tabindex
  - alt
  - title

## CSS
Las hojas de estilos se encargan de la apariencia de la página y aportan grandes ventajas a la accesibilidad web. Algunas de las ventajas de utilizar css son:

- Control total del formato
- Aporta gran flexibilidad al permitir cambiar la apariencia completa de un sitio web.
- Consistencia entre navegadores.
- Permite escribir el contenido de forma secuencial, **que es la forma en que la leen los lectores de pantalla**, y posicionarla de otro modo.
- Permite que el texto cambie de tamalo fácilmente, lo que es muy útil para las personas con problemas de visión.

## JavaScript
JavaScript es una tecnología del lado del cliente que se utiliza para manipular los elementos de la interfaz a través del DOM y para crear aplicaciones dinámicas. Para que las interfaces que utilizan JavaScript sean accesibles, la aplicación debe avisar al usuario cuando se produce un cambio y permitir el acceso al nuevo contenido.

## Otras

- Flash
- PDF. Para que sea accesible debería ser un PDF etiquetado.
- XML/XSL
- Reproducción multimedia
- Texto plano
- SMIL
- ARIA