# Para ayudar a mejorar la accesibilidad

Ejemplo de addon que ayuda a mejorar la accesibilidad
- https://addons.mozilla.org/es/firefox/addon/si-accessibility-checker/


# Lectores de pantalla

- https://support.mozilla.org/es/kb/Accesibilidad Aquí se incluyen enlaces a algunos programas.
- https://nvda.es/
