# Ayudas para superar las barreras
Podemos tener diferentes tipos de ayuda hardware y software enfocadas a la superación de algunas de las barreras.

## Ayudas para personas ciegas

- **Línea braille**. Periférico de salida que permite la lectura de información.
- **Lectores de pantalla**. Aplicaciones software que leen en voz alta el texto en pantalla mediante un sintetizador de voz o que lo envían a una línea braille para que se pueda leer con los dedos.
- **Navegadores de voz**. Son navegadores con un sintetizador de voz incorporado para leer en voz alta el texto en pantalla. Son una alternativa a los lectores de pantalla.
- **Teclado como dispositivo de entrada**. Se puede usar el teclado como un dispositivo de entrada prescindiendo del ratón. Tabulador permite recorrer secuencialmente los enlaces, las opciones de un menú o los campos de un formulario. También se usan atajos de teclado para ir directamente a una zona determinada de la pantalla.


## Ayudas para personas con baja visión

- **Pantallas grandes**.
- **Ampliadores de pantalla** que permiten ampliar una zona en concreto de la pantalla.
- **Combinaciones específicas de colores** para fondo y texto.
- **Tipos de letra más legibles** para nuestros contenidos.


## Ayudas para personas con daltonismo
Las personas com daltonismo pueden utilizar sus propias hojas de estilo para modificar colores de fuente y fondo de las páginas. De esta forma adaptan los colores de la web a sus necesidades. Por esta razón **es muy importante no incluir estilos en línea en el propio código HTML**.


## Ayudas para personas con discapacidad motriz

- **Ratones especiales**. Por ejemplo, ratones de bola, de cabeza, apuntadores de boca, etc.
- **Licornio**, un casco con varilla incorporada que permite utilizar el reclado tradicional con movimientos de la cabeza.
- **Teclados alternativos**. Con una disposición de teclas adaptadas a la capacidad de movimientos.
- **Teclado en pantalla**. Permiten usar un teclado virtual en la pantalla que se controla con un puntero.
- **Software de reconocimiento de voz**. Permite controlar el dispositivo únicamente con la voz.
- **Sistemas de seguimiento de ojos**. Permiten monitorizar la posición de los ojos en la pantalla o usarlo para controlar aplicaciones o dispositivos.


## Ayudas para personas con discapacidad cognitiva y neurológica

- **Lector de pantalla** para facilitar la comprensión a personas con dificultad de lectura.
- **Subtítulos** que ayuden a comprender el contenido audible.
- **Desactivación de elementos multimedia** para concentrarse en el contenido o evitar ataques como en el caso de enfermos de epilepsia.
