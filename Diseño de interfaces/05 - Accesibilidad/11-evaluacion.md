# Evaluación
En el documento [Easy Checks - A First Review of Web Accessibility](https://www.w3.org/WAI/test-evaluate/preliminary/#contrast) se proporcionan unos pasos rápidos para ayudar a evaluar si un sitio web es accesible. Permite realizar una revisión preliminar de los aspectos más evidentes, antes de un informe más profundo.

En el año 2012, la W3C publicó el documento de la [“Metodología de Evaluación de Conformidad con la Accesibilidad en sitios Web” (WCAG-EM). WCAG-EM](https://www.w3.org/WAI/test-evaluate/conformance/wcag-em/) proporciona un enfoque y una metodología común internacionalmente de evaluación de sitios web que son conformes a las WCAG 2.0.

# Ejemplo
Sitio web del w3c donde tenemos una web demostrativa con una versión antes de la accesibilidad y después de la accesibilida.

https://www.w3.org/WAI/demos/bad/