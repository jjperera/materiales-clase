# Normativa

## Introducción

- En España existe legislación desde 2002 para garantizar el derecho de las personas con discapacidad de poder acceder a internet en igualdad de condiciones y de manera autónoma. Garantizar el acceso a internet para todos es una obligación legal para las Administraciones Públicas, según establece la **Ley 34/2002 de Servicios de la Sociedad de la Información y el Comercio Electrónico**.

- La Unión Europea lleva tiempo planteándose una legislación común sobre accesibilidad web. A finales de 1999, puso en marcha la iniciativa eEurope “Una sociedad de la Información para Todos”, en la que uno de sus **objetivos era “garantizar que en la sociedad de la información no se produzca exclusión social”**.
  - eEurope 2002. “Accesibilidad de los sitios Web públicos y de su Contenido”. 
  - eEurope 2005. “Una sociedad de la información para todos”.


## Normas y estándares que han ido apareciendo

- En 1998, se crea en España la norma UNE 139802:1998 EX, primera norma en todo el mundo que hace referencia a la creación accesible de páginas web (fue anulada por la norma UNE 139802:2003).
- En 1999, **aparecen las Pautas de Accesibilidad para el Contenido Web (WCAG) 1.0 del Consorcio World Wide Web (W3C)** que indica cómo crear contenido web accesible para personas discapacitadas.
- En 2002 la Ley 34/2002 de Servicios de la Sociedad de la Información y el Comercio Electrónico (LSSICIE). Tenía el problema de que no se definía claramente cuál era el nivel mínimo de accesibilidad, y los sitios web no llegaron a cumplirla.
- En 2003, Ley 51/2003, de 2 de diciembre, de Igualdad de Oportunidades, No Discriminación y Accesibilidad Universal de las Personas con Discapacidad (LIONDAU).
- En 2004, la Norma UNE 139803:2004, Aplicaciones informáticas para personas con discapacidad. Requisitos de accesibilidad para contenidos en la Web (Actualmente ha sido sustituida por la norma UNE 139803:2012).
- En 2006, La norma CWA 15554:2006, Especificaciones para el esquema de la evaluación de la conformidad y marca de calidad sobre accesibilidad Web. Es la base de la certificación europea en Accesibilidad Web del Comité Europeo de Normalización (CEN).
- En 2006, Real Decreto 1414/2006, de 1 de diciembre, por el que se determina la consideración de persona con discapacidad a los efectos de la Ley 51/2003, de 2 de diciembre, de Igualdad de oportunidades, no discriminación y accesibilidad universal de las personas con discapacidad.
- En 2007, Real Decreto 366/2007, de 16 de marzo, por el que se establecen las condiciones de accesibilidad y no discriminación de las personas con discapacidad en sus relaciones con la Administración General del Estado.
- En 2007, Ley 11/2007, de 22 de junio, de acceso electrónico de los ciudadanos a los Servicios Públicos.
- En 2007, Real Decreto 1494/2007 por el que se aprueba el Reglamento sobre las condiciones básicas para el acceso de las personas con discapacidad a las tecnologías, productos y servicios relacionados con la sociedad de la información y medios de comunicación social.
- En 2007, Ley 27/2007, de 23 de octubre, por la que se reconocen las lenguas de signos españolas y se regulan los medios de apoyo a la comunicación oral de las personas sordas, con discapacidad auditiva y sordociegas.
- En 2007, Ley 49/2007, de 26 de diciembre, por la que se **establece el régimen de infracciones y sanciones en materia de igualdad de oportunidades, no discriminación y accesibilidad universal** de las personas con discapacidad.
- En 2007, Ley 56/2007, de 28 de diciembre, de Medidas de Impulso de la Sociedad de la Información (MISI). Esta ley extiende la obligación de accesibilidad a las páginas de internet de las empresas que presten servicios al público en general de especial trascendencia económica.
- En 2008, **la última versión de las Pautas de Accesibilidad para el Contenido Web (WCAG) 2.0 del Consorcio World Wide Web (W3C).**
- 2010, Ley 7/2010, de 31 de marzo, General de la Comunicación Audiovisual.
- 2011, Ley 26/2011, de 1 de agosto, de adaptación normativa a la Convención Internacional sobre los Derechos de las Personas con Discapacidad.
- En 2012, **ISO finalizó el proceso de estandarización de WCAG 2.0** el 12 de octubre de 2012, cuando ISO/IEC 40500:2012 alcanzó el estado “Estándar Internacional publicado”.
- En 2012, **Norma UNE 139803:2012, Requisitos de Accesibilidad para contenidos en la web**. Se basa en WCAG 2.0 y esta norma con la que corresponde completamente punto por punto.

En España, la Ley 49/2007 establece las sanciones por incumplimiento de las normas de accesibilidad web, pero **a día de hoy en España no se ha impuesto ninguna multa por motivos de accesibilidad web**. 

