# Ventajas

- La mayoría de adaptaciones necesarias para que un sitio web sea accesible **beneficia a todos los usuarios** y no solo a los que tienen alguna minusvalía. 
- **Incremento de la cuota de mercado** y audiencia de la web. Si se trata de un comercio, esto puede suponer un incremento económico. Hay un gran porcentaje de la población que sufre alguna minusvalía y es un mercado que utiliza mucho internet. 
- **Mejora el posicionamiento en buscadores**. Si se realiza un sitio web accesible, se hace una mejor identificación de los contenidos y este sitio web se va a posicionar mejor, ya que va a estar más optimizado para los motores de búsqueda como Google.
- Permite **reutilizar contenidos**. Si se diseña cumpliendo estándares de accesibilidad, se garantiza que los contenidos se puedan ver sin problemas en distintos navegadores y dispositivos. 
- **Mejora la eficiencia y el tiempo de respuesta**. Las páginas que están optimizadas y libres de código inútil o poco eficiente tardan menos tiempo en cargar porque pesan menos. 
- Se **cumplen los estándares web**, consiguiendo una mayor compatibilidad con navegadores y dispositivos. 
- **Reduce costes** en mantenimiento, ya que un sitio web accesible es un sitio bien hecho, es menos propenso a contener errores y más fácil de mantener. 
- **Reduce el tiempo de carga de las páginas web y la carga del servidor web**. Al separar la estructura y el contenido (HTML) de la presentación (CSS) de una página web se reduce el tamaño, el tiempo de carga y se facilita su mantenimiento. 
- **Demuestra responsabilidad social**, preocupación y atención hacia todos los usuarios, permitiendo su participación y no discriminando por razones de discapacidad. Esto supone un elemento diferenciador y un refuerzo positivo para la imagen de la empresa. 
- **Se cumple la ley**. La accesibilidad no es solo una buena idea, ya existen leyes para regularla y hay que cumplirlas. 
- Permite obtener **ayudas y subvenciones** u obtener trabajo de las administraciones públicas. 
- Aunque no son lo mismo, la accesibilidad está estrechamente relacionada con la usabilidad. Al cumplir requisitos de accesibilidad básicos en los elementos técnicos de una web (estructura de contenidos, vínculos, contraste de color, efectos y movimientos, formularios, tablas, etc.), se **mejora la usabilidad** para la mayoría de las personas, con los beneficios que esto supone.

Pero la única razón para hacer un sitio web accesible, y la más importante, debe ser porque **es la forma correcta de hacerlo**.

