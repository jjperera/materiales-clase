# Barreras
En los sitios web puede haber diferentes barreras que, dependiendo de las limitaciones del usuario, le afectarán en mayor o menor medida.

## Barreras para personas ciegas

- **Imágenes sin texto alternativo** para describir su contenido.
- **Imágenes complejas** con datos sin una descripción en texto de lo que se muestra.
- **Elementos multimedia** sin una descripción textual o sonora.
- **Tablas** con contenido incomprensible si se lee de forma secuencial.
- **Falta de independencia del dispositivo de E/S** utilizado para navegar.
- **Formatos no accesibles de documentos**. Por ejemplo, documentos PDF que no cumplan la normativa de accesibilidad.


## Barreras para personas con baja visión

- **Tamaño de letra** con medidas absolutas que no permiten redefinirlo.
- **Diseño de páginas** que al modificar el tamaño de letra estropean la maquetación complicando la navegación por el sitio web.
- **Poco contraste** entre el fonto, las imágenes y el texto.
- **Texto añadido mediante imágenes**. Esto dificulta la lectura y complica cambiar el tamaño.


## Barreras que impiden el acceso a personas con daltonismo

- **Uso de color para resaltar texto** sin utilizar otro elemento de formato adicional (cursiva, negrita, subrayado).
- **Poco contraste en las imágenes** con texto o entre el texto y el color de fondo de la página.
- **Navegadores** sin soporte para hojas de estilo definidas por el usuario.


## Barreras que impiden el acceso a personas con sordera o hipoacusia

- **La falta de subtítulos** o transcripciones de los contenidos.
- **La falta de imágenes que ayuden a la comprensión del contenido de las páginas**. Las imágenes favorecen la comprensión a las personas cuyo idioma principal es el lenguaje de signos.
- **Necesidad de entrada de voz** en algunos sitios web. Por ejemplo, cuando se pide al usuario que hable a través del microfono.


## Barreras que impiden el acceso a las personas con discapacidad motriz

- Iconos, botones y otros **elementos de interacción demasiado pequeños**, que dificultan su uso a personas con poca destreza en sus movimientos.
- Falta de **independencia de dispositivos**, que impide usar correctamente la web con el teclado en lugar de con el ratón.
- **Tiempos de respuesta** limitados para interactuar con la página. Por ejemplo, sitios donde hay que realizar un test con un tiempo máximo de finalización.

## Barreras que impoden el acceso a personas con discapacidad cognitiva y neurológica

- Los **elementos visuales o sonoros que no se pueden desactivar** cuando se desee y pueden distraer a las personas con déficit de atención.
- La **falta de una organización clara** y coherente de la información que ayude a las personas con problemas de memoria o con escasa capacidad cognitiva.
- El uso de un **lenguaje complejo**.
- La **ausencia de gráficos** en los sitios web que complementen la información textual.
- **Tamaño de letra** fijo que no se puede aumentar.
- **Destellos** o parpadeos con altas frecuencias que pueden provocar incluso ataques de epilepsia.



