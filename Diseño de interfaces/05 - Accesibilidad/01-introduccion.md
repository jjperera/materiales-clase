# Introducción
En el mundo real existen una serie de barreras que hay personas que no pueden superar o que, de hacerlo, lo hacen con dificultades. Ejemplos:

- Un hombre en silla de ruedas no puede subir una escalera
- Un invidente no puede ver un semáforo

En internet pasa lo mismo. Puede haber personas que por edad, por la velocidad de conexión, por dispositivo utilizado para acceder, por edad, etc, no puedan utilizar un sitio web con normalidad. 

La accesibilidad no es solo quitar barreras para personas con discapacidad sino que también tendría que ofrecer alguna alternativa para estas personas.

