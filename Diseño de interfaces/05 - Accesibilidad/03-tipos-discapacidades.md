# Limitaciones de los usuarios

## Tipos de discapacidades

- Discapacidad visual: ceguera, daltonismo, baja visión
- Discapacidad auditiva: sordera, hipoacusia
- Discapacidades motrices: distrofia muscular, distonía muscular, parkinson, parálisis cerebral
- Discapacidad neurológica o cognitiva: dislexia, discalculia, déficit de atención, falta de memoria, etc
- Discapacidades derivadas del envejecimiento

## Limitaciones derivadas del entorno
- Navegadores antiguos
- Navegadores de texto
- Conexiones lentas
- Pantallas pequeñas
- Monitores monocromos
- Entornos de trabajo que no permiten escuchar (por ruido ambiente, etc)
- Mala iluminación
- Ausencia de ratón

