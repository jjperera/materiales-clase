# Dificultades

- La **ignorancia de muchas empresas**, que piensan que no tienen usuarios ciegos o con otras discapacidades que accedan a sus sitios web.
- La mayoría de desarrolladores web actuales y personas que suben contenido a internet **no se preocupan** de la parte de la población que necesita ayuda para acceder a la web o no saben cómo ofrecer la información de forma accesible.
- Hay muchos **escépticos** que no creen que hacer las cosas más accesibles beneficie a todo el mundo.
- Muchos desarrolladores sienten **temor** de que hacer un sitio web accesible suponga más trabajo.
- Muchos diseñadores piensan erróneamente que está reñido realizar un buen **diseño** para personas con discapacidad y para el resto de personas, y que al hacer el diseño más accesible será menos atractivo para las personas sin discapacidad.
- Desarrollar un sitio web accesible **va a ser más caro** que uno que no lo sea. Esto es así si se tiene en cuenta que se necesita invertir más tiempo o recursos para su implementación. Las cosas mal hechas siempre son más baratas.
    
    Steven Pemberton, miembro de la W3C, dijo: **Tu usuario más importante es ciego. La mitad de las visitas a tu sitio vienen de Google, y Google solo ve lo que un ciego puede ver. Si tu sitio no es accesible, tendrás menos visitas. Fin de la historia.**


Si se desarrolla un sitio web pensando en la accesibilidad desde el principio, el coste extra de implementar estas mejoras será mínimo. Si el sitio ya existía y se quiere hacer más accesible supone un mayor coste, pero igualmente va a traer unos beneficios. La accesibilidad web no debe verse como un gasto, sino como una oportunidad de llegar a diferentes audiencias que de otra forma no se puede llegar.

