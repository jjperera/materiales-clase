# Organismos
Existen diversos oganismos de distintos niveles que regulan la accesibilidad en la web. Estos organismos pueden ser reconocidos en un ámbito global o local.

## El consorcio W3C

- WAI Iniciativa de accesibilidad a la web. Trabaja en cinco áreas
    - Tecnología
    - Pautas
    - Herramientas de validación y reparación
    - Educación y difusión
    - Investigación y desarrollo
- Pautas para la accesibilidad en la web. Se consideran los siguientes aspectos.
    - **Pautas de accesibilidad al contenido en la web**. WCAG, que muestran cómo hacer el contenido web accesible.
    - **Pautas de accesibilidad para las herramientas de edición**. ATAG, que muestran cómo hacer que estas herramientas sean accesibles.
    - **Pautas de accesibilidad para los agentes de usuario**. UUAG, para que las aplicaciones sean accesibles.

## AENOR
Asociación española de normalización y certificación. Fue reconocida en el año 1986 como entidad autorizada para el desarrollo de tareas de normalización y certificación en españa. **Se encarga de la elaboración, desarrollo y difusión de las normas españolas UNE** (Una norma española). 


## ISO
Organización internacional de normalización. Promueve el desarrollo de normas internacionales de fabricación de productos y servicios, comercio y comunicación para todas las ramas industriales. 


## CEN
Comité Europeo de Normalización es una organización que promueve los estándares europeos y especificaciones técnicas. Es la única organización europea reconocida para la planificación, elaboración y desarrollo y distribución de las normas europeas en todos los ámbitos de la actividad económica a excepción de electrotécnica y las telecomunicaciones.


