# Introducción
Hemos estado hablando previamente sobre la estructura de un documento HTML. Un documento HTML, como se ha explicado, es una jerarquía de elementos. Estos elementos describen el documento, que será renderizado por el navegador. En este documento vamos a ver cuál es la estructura de un documento.

# Estructura de un documento
Un elemento en HTML respondería a la estructura que podemos ver en la imagen. 

![Estructura de un elemento HTML](res/htmlexp.png)

De acuerdo con la imagen, un elemento contendrá:

- Etiqueta de apertura
- Atributos
- Contenido
- Etiqueta de cierre

## Etiqueta de apertura
La etiqueta de apertura define el elemento de que se trata. Podemos ver todas las etiquetas disponibles en cualquiera de las referencias HTML que tenemos a nuestra disposición. Por ejemplo, la del [w3c](https://dev.w3.org/html5/html-author/).

Ejemplos de etiquetas válidas podrían ser:
- **a** para definir un enlace
- **img** para definir una imagen
- **p** para definir un párrafo


## Atributos
Los atributos dan información adicional sobre el elemento. Si consideramos un elemento como un objeto, los atributos definirían las propiedades del objeto permitiendo asignarles un valor diferente a su valor por defecto. En la referencia de cada elemento se especificará cuáles son los atributos válidos. En la imagen tenemos un ejemplo de elemento con un atributo.

![Atrinutos en un elemento](res/htmlatributos.png)

Así, aquí tenemos varios ejemplos:

### img
Permite mostrar una [imagen](https://www.w3schools.com/tags/tag_img.asp). Podemos especificar diferentes astributos entre los que se encuentran src y alt para indicar donde se encuentra la imagen a mostrar y el texto alternativo en caso de que no se encuentre o no se pueda mostrar.

```html
    <img src="res/foto.jpg" alt="Texto alternativo a mostrar"/>
```

### a
Define un [hiperenlace](https://www.w3schools.com/tags/tag_a.asp). En este caso sería fundamental indicar la url destino. 

```html
    <a href="www.google.es">Ir a google</a>
```

## Contenido
Un documento, como se dijo previamente, es un árbol de elementos. El contenido hace referencia al anidamiento de elementos. Así, ciertos elementos permiten anidar otros elementos en su interior. Un ejemplo lo tenemos en la etiqueta [a](https://www.w3schools.com/tags/tag_a.asp), donde podemos utilizar por ejemplo una imagen como enlace.

```html
    <a href="www.google.es">
        <img src="res/boton_google.jpg" alt="Ir a google"/>    
    </a>
```

También podemos anidar directamente texto.

```html
    <a href="www.google.es">
        Ir a google
    </a>
```

---
#### Ejercicio html0301
Crea una página con diversos enlaces a páginas de internet. Prueba a utilizar texto o imágenes en el enlace. Prueba además con los siguientes atributos de a y comprueba los efectos:

- rel
- target
- download
---

### Elementos vacíos
Diversos elementos no pueden contener otros elementos. Un ejemplo sería el elemento img donde no es posible anidar ningún otro elemento. El formato en este caso sería como sigue:

```html
    <img src="res/foto.jpg" alt="Texto alternativo a mostrar"/>
```

Vemos que solo hay etiqueta de apertura, atributos y la propia etiqueta se puede cerrar al final, aunque no sería necesario.


## Etiqueta de cierre
La etiqueta de cierre se utiliza para indicar donde termina el elemento. Esta etiqueta tendrá sentido cuando un elemento puede contener otros. Esto permite delimitar donde hemos terminado de definir el contenido de un elemento. El contenido de un elemento será lo que hay entre la etiqueta de apertura y la de cierre.

