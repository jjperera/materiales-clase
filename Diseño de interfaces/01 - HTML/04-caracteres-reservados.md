# Introducción
Al escribir un documento HTML, puede ocurrir que queramos escribir caracteres que están reservados para escribir nuestro documento. Ejemplos de estos caracteres podrían ser < y > que se usan para delimitar los diferentes elementos. En este texto vamos a ver como se haría para mostrar estos caracteres.

# Entidad
Una entidad HTML es una cadena de caracteres que comienza con un '&' y termina con un ';'. Las entidades son utilizadas para imprimor por pantalla caracteres reservados que, de otro modo, serían interpretados por el navegador. También pueden ser utilizados para representar cualquier caracter aunque no esté en nuestro teclado.

# Caracteres reservados

| Caracter | Entidad | Nota |
| -- | -- | -- |
| & | \&amp; | Interpretado como el comienzo de una entidad HTML |
| < | \&lt; | Interpretado como la apertura de una etiqueta |
| > | \&gt; | Interpretado como el cierre de una etiqueta |
| " | \&quot; | Interpretado como apertura o cierre de un valor de un atributo |

# Referncia de entidades en HTML
En el siguiente documento tenemos una lista con las entidades válidas en HTML junto con los carácteres que representan.

[Referencia de entidades en HTML](https://html.spec.whatwg.org/multipage/named-characters.html#named-character-references)

# Ejercicios
## Ejercicio html0401
Crea un documento HTML que al cargarlo muestre un documento HTML con la estructura mínima de un documento HTML. Para que se muestre correctamente tendrás que utilizar las entidades HTML para representar los caracteres reservados.

# Herramientas
Cuando utilizamos un IDE, este puede ofrecernos la posibilidad de transformar texto en entidades. Esto puede facilitar enormemente la tarea de escribir las entidades en textos largos evitando además que cometamos errores. 

En VS Code, tenemos disponible la extensión [vscode-html-entities](https://marketplace.visualstudio.com/items?itemName=christopherstyles.html-entities) que permite llevar a cabo esta tarea de forma sencilla.
