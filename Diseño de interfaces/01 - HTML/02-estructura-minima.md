# Introducción
En este documento se revisa la estructura de un documento HTML válido. En los siguientes apartados vamos a ir añadiendo a nuestro documento diferentes elementos hasta tener la estructura completa válida de un documento HTML.


# El elemento doctype
Aunque no se requiere para que nuestro documento sea mostrado por un navegador, es recomendable que la primera línea de nuestro documento contenga este elemento. Este elemento le indica al navegador el tipo de documento de que se trata. De este modo **el navegador sabrá que estándares aplicar a la hora de procesar el documento**. Ponerlo tal y como se ve en el siguiente ejemplo es lo recomendable para que sea válido tanto en HTML como en XHTML e indica que el documento está en formato HTML5.

```html
<!DOCTYPE html>
```

# El raíz del documento
Un documento HTML se puede ver como un árbol de elementos, de modo que, el primer elemento en nuestro documento será la raíz del árbol. La raíz de todo documento HTML será el elemento html. Todos los elementos que describen el documento colgarán de este nodo raíz.

De acuerdo con esto, nuestro documento quedaría de la siguiente forma.

```html
<!DOCTYPE html>
<html>
    <!-- Contenido del documento --> 
</html>
```

Dentro del elemento html irían el resto de elementos del documento. Una buena práctica, es dar al navegador toda la información necesaria sobre cada elemento. En este caso, sería adecuado indicar en que idioma estaría escrito nuestro documento HTML. Para ello podemos utilizar el atributo [lang](https://developer.mozilla.org/es/docs/Web/HTML/Global_attributes/lang). Añadiendo dicho atributo a nuestro elemento html, podemos indicar el idioma en que está escrito nuestro documento. Este sería nuestro nuevo documento.

```html
<!DOCTYPE html>
<html lang=es>
    <!-- Contenido del documento --> 
</html>
```

Ahora, un navegador que abra nuestro documento html sabrá que está escrito en lenguaje español. Esto es más útil de lo que podemos pensar. Si, por ejemplo, disponemos de un programa que lea nuestro documento, podrá utiliza el idioma correcto.


# Metainformación
La metainformación es información acerga del propio documento. Esta información puede ser utilizada por los navegadores o buscadores cuando interpretan el documento. Esta información se incluye colgando del elemento head de nuestro documento. En este elemento, incluiremos toda la información relevante sobre nuestro documento. En el siguiente ejemplo tenemos como iría quedando nuestro documento en estos momentos.

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <!-- Metadatos --> 
    </head>
</html>
```

## Codificación del documento
Un ejemplo de información necesaría que deberíamos incluir sería la codificación utilizada para escribir el documento. La codificación ayudará al navegador a interpretar y renderizar correctamente el texto y evitar problemas derivados de la interpretación incorrecta del texto. La recomendación sería utilizar UTF-8 de modo que nuestro elemento meta quedaría como en uno de los siguientes ejemplos:

```html
<meta charset=utf-8>
<META charset=utf-8>
<META charset=UTF-8>
<META charset="UTF-8">
<META charset="UTF-8" />
```

Todos los elementos meta del ejemplo son válidos. Debemos recordar que HTML no es XML de modo que no necesitamos utilizar comillas en los valores de los atributos ni cerrar los elementos, de modo que, nuestro documento HTML quedaría actualmente como sigue:

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <meta charset=utf-8>
    </head>
</html>
```

Un navegador que abra ese documento entenderá que está escrito siguiendo la especificación del estándar HTML5 y codificado en UTF-8. Evidentemente, el editor utilizado debe guardar el documento utilizando esta codificación o la que hayamos utilizado.

## Añadiendo un título
El siguiente elemento que podemos añadir a nuestro documento sería un título. Esto se hace con el elemento title. Dicho elemento debe contener únicamente texto y será mostrado en la barra de título de la ventana del navegador o de la pestaña en que se encuentre la página cargada. 

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <meta charset=utf-8>

        <title>Estructura básica de un documento HTML</title>
    </head>
</html>
```

Es recomendable evitar los títulos excesivamente cortos y aquellos que sean excesivamente largos. Por ejemplo, los buscadores mostrarán alrededor de unos 50 caracteres del título. Además el título debería ser suficientemente claro y explicativo.


## Otros metadatos
En la [referencia de w3shools](https://www.w3schools.com/tags/tag_meta.asp) sobre el tag meta podemos ver algunos de los atributos que podemos asignar para nuestro documento. Los añado a nuestro documento en el siguiente ejemplo.

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <!-- La codificación. Considéralo una buena práctica -->
        <meta charset=utf-8>

        <!-- Información adicional para buscadores y navegadores -->
        <meta name="author" content="Nombre del autor">
        <meta name="description" content="Estructura de un documento HTML5">

        <!-- Añade un título -->
        <title>Estructura básica de un documento HTML</title>
    </head>
</html>
```

Observa que los elementos meta no están cerrados. Recuerda que no es un documento XML por lo que no es necesario. En la referencia indicada tienes un ejemplo más completo, así como todos los posibles atributos que puedes asignar. Aunque es imprescindible, es recomendable que añadamos toda la información que tangamos. Dicha información será empleada por navegadores, buscadores, aplicaciones web, etc cuando carguen nuestro documento.


# El cuerpo del documento
La otra parte que contiene nuestro documento es el cuerpo. Podemos considerar el cuerpo como la parte de nuestro documento que el navegador va a renderizar. Todos los elementos del documento estarán por debajo del elemento [body](https://www.w3schools.com/tags/tag_body.asp). Nuestro documento quedará entonces del siguiente modo.

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <!-- La codificación. Considéralo una buena práctica -->
        <meta charset=utf-8>

        <!-- Información adicional para buscadores y navegadores -->
        <meta name="author" content="Nombre del autor">
        <meta name="description" content="Estructura de un documento HTML5">

        <!-- Título del documento -->
        <title>Estructura básica de un documento HTML</title>

    </head>
    <body>
        <!-- Aquí iría la descripción del documento -->
    </body>
</html>
```

En este punto, ya tenemos el documento HTML5 mínimo que podríamos tener. 

# Ejercicios

## Ejercicio. html0201
Crea un documento con la estructura mínima que muestre un mensaje de texto en pantalla. Cárgalo con el navegador.

## Ejercicio. html0202
Incluir el idioma en la etiqueta HTML se recomienda, como hemos visto anteriormente. Además, debería reflejar el idioma en que nuestro documento HTML se ha escrito. Vamos ahora a comprobar como efectivamente debemos ser cuidadosos al suministrar información sobre nuestro documento. Para este ejercicio, vamos a instalar el addon [Texto a Voz](https://addons.mozilla.org/es/firefox/addon/read-aloud/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) de firefox. Este plugin permite leer en voz alta una página web. Una vez instalado aparecerá un botón con un megáfono en la parte superior derecha.

Probaremos ahora a crear una página que contenta únicamente el texto "Hola Mundo". Ahora cambiaremos el idioma a:

- es: la pronunciación será correcta
- en: la pronunciación será incorrecta

Esto pone de manifiesto la inportancia de incluir toda la información posible y que además sea correcta.


