# Introducción.
En este documento vamos a tratar los elementos que podemos utilizar para describir un documento HTML. Para ello nos vamos a basar directamente en la [referencia para desarrolladores](https://developer.mozilla.org/es/docs/Web/HTML/Element) de Mozilla.


# Metadatos
Esta sección hace referencia a la información sobre el propio documento. Hemos visto algunas de las etiquetas que podemos utilizar pero hay algunos más que suministran aún más información al navegador. 

## Ejercicio html0501
Vamos a crear un documento html que contenga al menos los siguientes elementos en la sección meta
- **Elemento base**. Además, necesitaremos un segundo documento HTML al que haremos referencia luego en algún enlace. 
- **meta**: Vamos a incluir *todos los atributos meta que pueden definirse*.
- **title**: Vamos a asignar un título



# Seccionamiento del contenido
Estos elementos tienen como objetivo organizar los contenidos del documento en partes lógicas. Deberemos utilizar estos elementos para dar la mayor información posible sobre la organización de nuestro documento. 

Resuelve los siguientes ejercicios utilizando los conceptos que ya conoces. Si es necesario, recurre a la utilización de tablas o etiquetas <div>. No te preocupes si no puedes solucionarlo ahora. Seguiremos trabajando sobre estos ejercicios.

## Ejercicio html0502
![Secciones 0](res/htmlsecciones0.jpg)

## Ejercicio html0503
![Secciones 1](res/htmlsecciones1.jpg)

## Ejercicio html0504
![Secciones 2](res/htmlsecciones2.jpg)

## Ejercicio html0505
![Secciones 3](res/htmlsecciones3.jpg)

## Ejercicio html0506
![Secciones 4](res/htmlsecciones4.jpg)

## Ejercicio html0507
Identifica la estructura de [menéame](res/htmlsecciones5.jpg) y crea una página HTML donde se muestre una disposición similar del contenido utilizando los elementos HTML adecuados.

## Sobre los elementos para seccionamiento del contenido
Anteriormente, cuando se quería hacer un diseño como el que vemos en los anteriores ejercicios, se recurría al uso de tablas. Utilizando la combinación de filas o columnas se configuraba una tabla del modo que necesitábamos. El problema es que a medida que los sitios web han ido ganando complejidad en su diseño, utilizar tablas presentaba problemas debido a:

- El mayor tamaño de los archivos
- Las dificultades al mantemiento al ser una tabla una estructura más rígida y ser el desarrollador el que tiene que configurar la tabla del modo deseado.

Estos problemas impulsaron la separación entre estructura del documento y presentación. Ahora, en lugar de utilizar tablas, se pasó a utilizar elementos <div> que serían configuradas desde CSS. CSS es lo suficientemente potente como para ofrecer la flexibilidad necesaria para crear sitios y aplicaciones complejas con esta separación. Con esto surgió lo que se conoce como modelo de caja tradicional que será estudiado en el tema de CSS.

Adicionalmente, en HTML5 se introdujeron nuevos elementos que, además de permitir ser utilizados para estructurar nuestro documento, aportan un significado al mismo. Entre ellos están los que se han propuesto para los ejemplos.

# Texto
Estas etiquetas permiten organizar la información de tipo texto. Esta información es portante tanto para accesibilidad como para SEO.

## Ejercicio html0508
Crea un documento que contenga varias citas con un enlace al documento original.

## Ejercicio html0509
Crea un documento donde aparezcan varias figuras.
## Ejercicio html0510
Busca el elemento más adecuado para mostrar código fuente. Escribe un documento donde se muestre un un programa.


# Semántica del texto
Añade significado al texto. 

## Ejercicio html0511
1. Define en un documento el elemento base en los metadatos del mismo.
2. Ahora define varios enlaces a otros documentos utilizando el elemento a. 

Prueba a definir los enlaces como ruta absoluta o relativa. Comprueba como el elemento base afecta c omo se interpretan las URL.

## Ejercicio html0512
Define un documento en que incluyas varias abreviaturas. ¿Qué ventaja tiene marcar una abreviatura como tal respecto a no hacerlo?

## Ejercicio html0513
Prueba el elemento <sample>. ¿Para qué sirve?

## Ejercicio html0514
¿Para qué sirve el elemento span? ¿Podrías dar algún ejemplo de uso?

## Ejercicio html0515
Prueba el elemento <time>. ¿Para qué sirve? ¿Aporta alguna ventana sobre no utilizarlo?


# Imagen y multimedia
Permiten mostrar contenido multimedia en nuestro documento. 

## Ejercicio html0516
Carga varias imágenes en un documento y prueba el efecto de los diferentes atributos.

- ¿Qué formatos de imagen se pueden utilizar?


# Scripting
Más adelante profundizaremos en estos elementos que permite, entre otras cosas, introducir código JavaScript en nuestra web.


# Tablas
Permiten manejar datos en tablas.

## Ejercicio html0517
Crea un documento HTML que muestre la siguiente tabla.

![Tabla 0](res/htmltable0.jpg)

## Ejercicio html0518
Crea un documento HTML que muestre la siguiente tabla (no es necesario mostrar los colores).

![Tabla 1](res/htmltable1.jpg)

## Ejercicio html0519
Partiendo de la información en la base de datos que has creado para la parte de desarrollo cliente, diseña una tabla que muestre como se organizaría la información.


# Formularios
Los formularios permiten recoger información del usuario.

## Ejercicio html0520
Crea un formulario como el de la imagen. No es necesario que incluyas los colores.

![Formulario 1](res/htmlform1.jpg)

Responde a las siguientes cuestiones:
- ¿Cómo se envían los datos al servidor?
- Si dos campos se llaman igual, ¿Cuál se envía?