# Introducción
Por validación entendemos el proceso por el comprobamos que nuestro documento está bien escrito y es conforme a las reglas del estándar que estamos utilizando. No es un paso obligatorio ya que, incluso si nuestro documento está mal escrito, es bastante probable que sea visualizado correctamente por los distintos navegadores. Aún así, es altamente recomendable para detectar errores que se nos hayan pasado y garantizar que nuestro sitio web está correctamente descrito.

# ¿Qué queremos validar?
Cuando validamos un documento, podemos buscar errores de los siguientes tipos:

- Documento mal formado: cuando el documento HTML no es correcto de acuerdo a la especificación por incluir elementos que no existen o no ser acorde al estándar.
- Estamos utilizando características obsoletas. Estas características, aunque pueden ser utilizadas, no se recomienda para garantizar la compatibilidad del sitio a largo plazo.
- El documento no es conforme al estándar que declara incluyendo elementos que pertenecen a otras versiones.

# ¿Cómo podemos validar?
Para llevar a cabo la validación podemos recurrir a diferentes herramientas. Si estamos utilizando un IDE para crear nuestros documentos HTML es posible que el propio IDE nos haga sugerencias o nos asista para que creemos código correcto. Un par de herramientas que podemos utilizar son:

- [El servcio de validación del W3C](http://validator.w3.org/): permite valizar documentos a partir de la url, subir documentos o validar html en línea.
- [Html Validator](https://addons.mozilla.org/es/firefox/addon/html-validator/) para Firefox: es un complemento para el navegador que nos puede ayudar a validar documentos HTML.

## Servicio de validación
Si accedemos a la web de validator veremos una pantalla como la que sigue. En nuestro caso, como probaremos código antes de subirlo, lo más sencillo sería recurrir a la pestaña **Direct Input** donde nos será posible escribir código directamente.

![w3c HTML validator](res/htmlw3cvalidator.jpg)


## Html Validator
Tras instalar el plugin podremos ver el icono que se muestra en la imagen. Para utilizar el validator tendremos que abrir las herramientsa de desarrollador e ir a la pestaña de validación de html.

![HTML validator](res/htmlvalidator.jpg)


# Ejercicios
En este apartado os incluyo algunos ejercicios para practicar con las herramientas y acostumbrarnos a su salida. Además, deberemos revisar el error mostrado y analizarlo para comprender las consecuencias del error.

## Ejercicio html0601
Pega un documento HTML que contenga un mensaje hola mundo. El documento debe tener la estructura mínima válida que hemos visto previamente. Valídalo con ambas herramientas. Prueba a cometer algunos errores:

- Quita alguna etiqueta de cierre
- escribe algún nombre de atributo incorrecto
- abre comillas y no las cierres
- duplica o elimina algún <
- Elimina la \ de alguna etiqueta de cierre

Responde a las siguientes cuestiones.
- ¿Son detectados correctamente todos los errores?
- Si los incluimos todos, ¿es capaz el validador de detectar todos los errores al mismo tiempo incluso cuando tengamos varios que afecten a la estructura del documento?

## Ejercicio html0602
Elimina ahora la etiqueta DOCTYPE y comprueba que ocurre. 

## Ejercicio html0603
Incluir el idioma en la etiqueta HTML se recomienda, como hemos visto anteriormente. Prueba a eliminarla y valida el documento.

## Ejercicio html0604
Las herramientas también pueden validar que el documento tiene una estructura correcta. Prueba por ejemplo a duplicar el cierre de la etiqueta </html>

## Ejercicio html0605
Como ocurre a medida que los estándares evolucionan, aparecen nuevos elementos y otros desaparecen. Una buena práctica es evitar la utilización de determinados elementos o atributos a medidas que se van volviendo obsoletos. En este ejercicio vamos a comprobar como se detectan estos elementos o atributos que se consideran obsoletos.

Ejemplos de elementos obsoletos en HTML5
- font
- big
- dir

Ejemplos de atributos obsoletos
- shape de \<a>
- rev de \<a>
- name de \<img>

Cuando un elemento o atributo se elimina, podemos encontrarnos con dos casos:
- Se elimina por no ser necesaria ya la funcionalidad
- Se elimina por recomendarse utilizar otra funcionalidad para obtener el mismo resultado.

En este ejercicio, para los elementos y atributos mostrados, vamos a:

- Buscar para qué servía este atributo o elemento
- Comprobar como se ha reemplazado esa funcionalidad
- Vamos a comprobar que muestra el validador si lo utilizamos

