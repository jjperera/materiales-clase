# Introducción
1 En 1998, el W3C decidió que no continuarían la evolución de HTML. Ellos pensaban que el futuro era XML de modo que congelaron HTML en la versión 4.01 y liberaron la especificación XHTML 1.0 que era una versión XML de HTML que requería seguir las reglas de XML, más estrictas. Por ejemplo, ahora era necesario encerrar los atributos entre comillas y cerrar todas las etiquetas. XHTML Transitional fue una especificación diseñada para ayudar a los desarrolladores a moverse a XHTML Strict. 

2 Sin embargo, un grupo en Opera no estaba tan convencido acerca de que XML fuera el futuro de la web. Empezaron entonces a trabajar en lo que con el tiempo se volvería Web Forms 2.0. Esta especificación más tarde sería incorporada a HTML5. 

3 En 2006, el W3C decidió que quizá habían sido optimistas esperando que todo el mundo se movería a XML. 

4 El resucitado HTML Working Group decidió basarse en el trabajo del WHATWG para la nueva especificación. 

# Referencias
En los siguientes enlaces tenemos los diferentes estándares junto con otras cuestiones.

- [Estándar del w3c en su última versión](www.w3.org/TR/html5/)
- [El standard HTML del WHATWG](http://whatwg.org/html)
- [Referencia que incluye elementos experimentales](http://developers.whatwg.org)
- [HTML Standard FAQ](https://github.com/whatwg/html/blob/main/FAQ.md)

# Filosofía

* Especificar el comportamiento de los navegadores de forma uniforme
* Espefificación para los autores
* Definir la gestión de errores. Por ejemplo, ¿Cómo deben gestionar los navegadores los documentos inválidos.
* El DOM ha sido extendido para facilitar aplicaciones web. Lo que es una web que imita el comportamiento de un programa impulsada por JavaScript
* HTML5 es un superset de HTML4 de modo que las web HTML4 no se rompan. Pero los autores deben evitar el marcado obsoleto.
* HTML5 no es XML. Existe un standard XHTML5 que tiene unas normas más estricas pero si es XML válido.






