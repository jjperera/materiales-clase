# Enunciado. Tarea HTML y CSS 1
Una empresa nos ha solicitado una plantilla web sitio de noticias. Tras una reunión hemos hecho un boceto del sitio web que luce como el siguiente:

![Sitio 1](sitio1.jpg)

![Sitio 2](sitio2.jpg)

El sitio web está compuesto de los siguientes elementos:

- Cabecera
- Menú horizontal
- Lista de noticias
- Panel derecho con
  - Noticias destacadas
  - Noticias más votadas
  - Sitios más visitados
- Pie del sitio web

Se esplicará en clase como funciona el sitio web.


# Entrega
La entrega se hará de acuerdo a las instrucciones en el Moodle.