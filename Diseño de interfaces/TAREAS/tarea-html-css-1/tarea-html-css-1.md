# Enunciado. Tarea HTML y CSS 1
Una empresa nos ha solicitado una plantilla web para un sitio de compras. Tras una reunión hemos hecho un boceto del sitio web que luce como el siguiente:

![Sitio](sitio.jpg)

El objetivo de esta tarea es generar los archivos HTML y CSS que obtendrán un resultado lo más parecido posible. La web está compuesta de los siguientes elementos:

- Cabecera. donde se encuenta el buscador del que puede hacerse uso.
- Menú horizontal. Que permite ir a las principales secciones del sitio.
- Menú de la izquierda. Permite filtrar los diferentes elementos del sitio.
- Lista de componentes. Muestra todos los elementos disponibles en la categoría seleccionada.
- Ofertas. A la derecha se muestran algunas ofertas disponibles relacionadas con el contenido seleccionado.
- Pie. donde se incluye información adicional sobre el sitio web.

En los siguientes apartados os decribo cada una de las partes del sitio web de modo que os sea más sencillo obtener resultados similares.


# Antes de comenzar
Se deben preparar los siguientes recursos:

- Se va a utilizar la fuente roboto
- Se va a utilizar font-awesome


# Descripción de los elementos

## Cabecera
La cabecera se encuentra en la parte superior. En la captura tenemos un ejemplo de como se ve dicha cabecera.

![Cabecera](cabecera.jpg)

Podéis poner cualquier fondo, pero el fondo debe estar estirado (sin repetición) de modo que ocupe todo el espacio. En este caso se ha utilizado un linear-gradient para hacer que la imagen aparezca degradada a la derecha. El único contenido de la cabecera es el buscador. Este será un formulario que se ve como en la captura. 

Tener en cuenta lo siguiente:
- El icono de la lupa utilizado se ha de obteber de font-awesome.
- Se debe utilizar CSS grid para colocar el campo de búsqueda.
- El atributo outline hace referencia a la línea por fuera del campo del formulario.

Diferentes resoluciones:
- Se adaptará el grid y el espacio ocupado por el formulario para que se adapte al menos
  a resoluciones por debajo y encima de 1000 puntos.


## Menú horizontal
El menú horizontal permite cambiar entre las principales secciones del sitio web. Dicho menú se ve como en la captura.

![Menú horizontal](menuhorizontal.jpg)

Se hará una demostración del funcionamiento de este menú y el efecto que debe observarse. Tener en cuenta lo siguiente:

- Se aplica un linear-gradient de izquierda a derecha.
- Las opciones tienen un ancho fijo.
- Al pasar por encima cambia el borde superior e inferior.


## Menú vertical
El menú vertical permite filtrar componentes por categoría dentro de cada sección del sitio. Aquí tenemos una captura. 

![Menú vetical](menuvertical.jpg)

Algunas consideraciones:
- Utiliza el modelo de caja flexible
- El menú solo será visible para resoluciones > 1000 px
- Utiliza selectores adecuados para aplicar bordes redondeados a primer y último elemento del menú.


## Lista de componentes
La lista de componentes va a mostrar los componentes disponibles en cada categoría. Mostrará en la clase el efecto que se debe conseguir.

![Lista de componentes](listacomponentes.jpg)

Ten en cuenta lo siguiente:
- Se puede usar CSS grid o float. Posiblemente float va a dar mejores resultados. Si se usa CSS grid, debes establecer saltos para 500 1000 1500 2000 y 2500 px donde se cambie el número de columnas.
- Puensa que cada componente es una caja. Dentro de la caja puedes utilizar varias cajas para organizar el contenido. El contenido es el nombre, el precio y la foto.


## Ofertas
Se ven como se ve en la foto. Son varias ofertas separadas por una línea.

![Ofertas](ofertas.jpg)

Las ofertas funcionan exactamente del mismo modo que la lista de componentes pero mostrando solo uno después de otro. Se debe poner una línea que separa las ofertas. El número de ofertas es indeterminado. Se debe tener en cuenta que puede crecer sin límite.

Esta parte del sitio web no se mostrará para resoluciones por debajo de 1000px.


## Pie
El pie muestra en tres columnas enlaces a información adicional sobre nuestro sitio web.

![Pie](pie.jpg)

Para resoluciones por debajo de 800px se mostrarán en una sola columna.

# Sitio web adaptativo
La web ha de adaptarse a diferentes resoluciones de pantalla.


# Entrega
La entrega se hará de acuerdo a las instrucciones en el Moodle.