# Introducción
En este texto vamos a hablar sobre el modelo de caja tradicional en HTML. Los navegadores, a la hora de renderizar un documento HTML, lo hacen según su tipo. Cuando maquetamos en HTML lo que hacemos es construir cajas y poner contenido dentro. Podemos encontrar básicamente dos tipos de componentes en un documento HTML.

- **Elementos de bloque** que van a ser colocados uno tras otro en la página. Estos elementos de bloque serían las cajas a que hacíamos referencia antes.
- **Elementos en línea**: que se colocal de lado a lado, uno junto al otro en la misma línea, sin ningún salto de línea salvo que no haya suficiente espacio horizontal.

Debemos comprender las diferencias entre estos dos tipos de elementos, ya que será imprescindible para contruir nuestras estructuras en CSS y no podremos tratarlos por igual.

## Los elementos de bloque
Los elementos de bloque son los contenedores que nos van a ayudar a organizar el contenido. Se caracterizan porque porque de forma predeterminada:

- Ocupan siempre todo el ancho disponible. Si se les asigna un ancho, el margen creado por defecto ocupará todo el espacio disponible hasta el final.
- Siempre empiezan en una nueva línea. 
- Las propiedades width y height se aplican.
- Los atributos relacionados con relleno, borde y márgenes empujarán otros elementos fuera de la caja.

De acuerdo con estas características, **en un documento sin formato todos los elementos se ordenan de forma vertical**.

Los siguientes son los elementos de bloque definidos por HTML:

- address
- blockquote
- center
- dir
- div
- dl
- fieldset
- form
- h1
- h2
- h3
- h4
- h5
- h6
- hr
- isindex
- menu
- noframes
- noscript
- ol
- p
- pre
- tab
- le
- ul

Adicionalmente, los siguientes son considerados de bloque

- dd 
- dt
- frameset
- li
- tbody
- td
- tfoot
- th
- thead
- tr

## Los elementos en línea
Si los elementos de bloque son los contenedores, los elementos en línea son el contenido. Del mismo modo que cuando escribimos, cada palabra, cada letra se coloca al lado de la otra, cuando colocamos un elemento en línea junto a otro se colocará a su lado siempre que el ancho disponible lo permita. Los elementos en línea, pueden ser considerados como texto porque fluyen como este. No están, por lo tanto, pensados para contener otros elementos y ocupan lo estrictamente necesario. Así, un elemento en línea no se extenderá para ocupar todo el ancho disponible. 

Los elementos en línea definidos por HTML son:

- a
- abbr
- acronym
- b
- basefont
- bdo
- big
- br
- cite
- code
- dfn
- em
- font
- i
- img
- input
- kbd
- label
- q
- s
- samp
- select
- small
- span
- strike
- strong
- sub
- sup
- textarea
- tt
- u
- var

## Elementos de bloque o línea
Hay diversos elementos que pueden comportarse como elementos de bloque o línea dependiendo de las circunstancias. 

- button
- del
- iframe
- ins
- map
- object
- script

