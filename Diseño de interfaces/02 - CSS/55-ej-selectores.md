
# Combinadores
En este apartado os propongo algunos ejercicios de combinadores.

Ejercicios relacionados con las diferentes propiedades en CSS. Los ejercicios deben subirse al GIT del alumno en la carpeta css/ejercicios/nombreejer. 


## Pseudoclases

### cssselpelementos01
Ejercicio [Ejercicios resueltos para prácticar 01/Clases/Greguerías 1]

Utilizar índice

- Cada línea de texto es un párrafo.
- Utiliza el selector :first-child para asignar negrita.
- Utiliza nth-child con even y odd para asignar una fuente diferente en función de si es par o impar.
- Utiliza nth-of-type para asignar el color rosa a un elemento
- Utiliza la pseudoclase :hover para cambiar un atributo de color sobre todos los elementos

### cssselpelementos02
En este ejercicio vamos a probar a crear una tabla sencilla para probar algunos de los selectores del mismo modo que en el ejercicicio anterior. Crea una tabla de 2 columnas y 5 filas y aplica los siguientes estilos:

- Utiliza el selector :first-child para asignar negrita.
- Utiliza nth-child con even y odd para asignar un color diferente de fondo. De modo que la lectura sea más sencilla.
- Utiliza nth-of-type para asignar el color de fondo rosa a un elemento
- Utiliza la pseudoclase :hover para cambiar un atributo de color sobre todos los elementos



