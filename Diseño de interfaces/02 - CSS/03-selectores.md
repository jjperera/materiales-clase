# Introducción
Ya se ha hablado anteriormente de que necesito algún mecanismo para que el navegador sepa a que elementos tiene que aplicar cada una de las reglas en nuestro fichero CSS. Para esto se debinen los selectores. Los selectores, que siempre preceden a una regla CSS los utiliza el navegador para aplicar reglas a los elementos. Podemos indicar un solo selector, o varios separados por comas.

```css
    h1 {
        color: blue;
    }

    .special {
        color: blue;
    } 

    h1, .special {
        color: blue;
    }     
```

Los selectores básicos que podemos utilizar serían del más general al más específico:

- tipo
- class
- id

Se debe tener en cuenta que además existirá una precedencia a la hora de aplicarlos. Esto es importante tenerlo en cuenta, ya que puede haber varias reglas que se apliquen a un mismo elemento, de modo que el orden de aplicación deberá ser tenido en cuenta a la hora de definir nuestras reglas.

# Selectores básicos
En esta sección vamos a desarrollar los métodos básicos que podemos utilizar para seleccionar el elemento HTML.
## tipo
De este ya hemos visto algunos ejemplos. Podemos espeficicar un nombre de un elemento. En el siguiente ejemplo se ven algunas reglas utilizando este método.

```css
p {
    color: rgb(255,255,255);
}

span {
    font: bold 12px arial, sans-serif;
}
```

Las reglas anteriores, se aplicarán a todos los elementos p y span respectivamente. Lo mismo puede definirse para todos los elementos disponibles en HTML.

## class
Utilizando el atributo class de los elementos html podemos indicar qué estilos queremos aplicar a un elemento de nuestro documento. A continuación estamos definiendo dos reglas. Una personaliza el color y otra la fuente. Observar que el nombre de la clase va precedido de un '.'.

```css
.color {
    color: rgb(255,255,255);
}

.fuente {
    font: bold 12px arial, sans-serif;
}
```

Después podemos tener un documento HTML en el que apliquemos diferentes estas reglas. En el ejemplo tenemos tres párrafos. El último de ellos aplica varias reglas. Si se quiere indicar más de una clase en el atributo class se separarán con espacios.

```html
    <p class="color">Hola mundo
    <p class="fuente">Hola mundo
    <p class="color fuente">Hola mundo
```


## ID
Finalmente, podemos utilizar el atributo ID para aplicar una regla a un elemento determinado. Este método debería ser utilizado únicamente cuando queremos aplicar un estilo a un elemento concreto de nuestro documento. Hay que tener en cuenta que el ID debe ser único en el documento por lo que, como se ha dicho, solo es aplicable cuando hay un determinado elemento al que se le quiere aplicar este estilo.

```css
#elparrafo {
    color: rgb(255,255,255);
}
```

En el CSS, cuando el selector hace referencia a un ID irá precedido del caracter #.

```html
    <p id="elparrafo">El párrafo especial
```

# Selector universal
El selector universal permite seleccionar elementos de cualquier tipo.

```css
* {
  color: green;
}
```

El anterior ejemplo pondría en verde todos los elementos que tengan dicha propiedad.

# Combinadores
Los combinadores permiten seleccionar elementos dependiendo de su contexto y su relación con otros elementos. En estos selectores, generalemnte utilizaremos dos o más selectores y diversos operadores que indicarán la relación buscada.

Este apartado lo vamos a seguir por la [documentación de mozilla](https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/Selectors/Combinators).

# Pseudoclases
Las pseudoclases permiten seleccionar un determinado elemento dependiendo de su estado. Ejempos de pseudoclases son:

- :hover
- :read-only
- :focus

La sintaxis sería

```css
    selector:pseudoclase { propiedad: valor;}
```

Este apartado lo vamos a seguir con la [documentación de mozilla](https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-classes). Probaremos al menos en ejercicios trabajando sobre los ejercicios HTML de la unidad anterior:

- :hover
- :focus
- :read-only
- :first-child
- :only-child
- :only-of-type
- :nth-child() (odd/even)
- :nth-of-type()

# Pseudoelementos
Respecto a los pseudoelementos, permiten seleccionar partes de un determinado elemento. Por ejemplo, podemos seleccionar la primera línea, la parte seleccionada. Este apartado lo veremos con la [documentación de mozilla](https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-elements). 

Revisaremos al menos algún ejemplo con:

- ::first-line
- ::first-letter
- ::placeholder


# Selectores de atributos
El selector de atributo permite seleccionar elementos en función de la presencia o valor de un determinado atributo de un elemento. 

```css
    a[title] {
        color: purple;
    }
```

Vamos a seguir este apartado por la [documentación de mozilla](https://developer.mozilla.org/es/docs/Web/CSS/Attribute_selectors).

# Referencias
- [Tabla de referencia de selectores](https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/Selectors): enlace a una lista con todos los selectores que podéis utilizar en un documento CSS.




