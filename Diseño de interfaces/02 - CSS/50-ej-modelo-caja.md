# Introducción.
Ejercicios relacionados con el **modelo de caja tradicional** de CSS.

## Ejercicio css5001
![Secciones 0](res/box1.jpg)

## Ejercicio css5002
![Secciones 1](res/box2.jpg)

## Ejercicio css5003
![Secciones 2](res/box3.jpg)

## Ejercicio css5004
![Secciones 3](res/box4.jpg)

## Ejercicio css5005
![Secciones 4](res/box5.jpg)

## Ejercicio css5006
![Secciones 4](res/box6.jpg)

## Ejercicio css5007
![Secciones 4](res/box7.jpg)

## Ejercicio css5008
Haz este ejercicio utilizando el modelo de caja flexible.

![Secciones 4](res/box6.jpg)

## Ejercicio css5009
Haz este ejercicio utilizando el modelo de caja flexible.

![Secciones 4](res/box7.jpg)


## Ejercicio css5010
Identifica la estructura de [menéame](http://www.meneame.net) y crea una página HTML donde se muestre una disposición similar del contenido utilizando los elementos HTML adecuados.


