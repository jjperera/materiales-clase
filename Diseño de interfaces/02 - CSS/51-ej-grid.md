# Introducción.
Ejercicios relacionados con el **grid** de CSS.

## Ejercicio css5101
![Grid 1](res/grid1.png)

## Ejercicio css5102
![Grid 2](res/grid2.jpg)

## Ejercicio css5103
![Grid 3](res/grid3.png)

## Ejercicio css5104
![Grid 4](res/grid4.png)

## Ejercicio css5105
![Grid 5](res/grid5.png)

## Ejercicio css5106
![Grid 6](res/grid6.jpg)

## Ejercicio css5107
![Grid 7](res/grid7.jpg)

## Ejercicio css5108
![Grid 8](res/grid8.png)

## Ejercicio css5109
![Grid 9](res/grid9.jpg)

