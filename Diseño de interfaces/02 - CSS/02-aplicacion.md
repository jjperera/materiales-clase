# Introducción
Vamos de describir en este documento de que modo podemos aplicar los estilos a nuestro documento. Hay tres formas en que podemos hacerlo:

- Estilos en línea.
- Estilos incrustados.
- Archivos externos.

# Estilos en línea
HTML permite especificar en línea estilos CSS a los diferentes elementos de nuestro documento. Para ello podemos utilizar el atributo **style**. En el siguiente ejemplo se aplica un estilo al elemento p del cuerpo del documento asignando el valor directamente al atributo style.

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <meta charset=utf-8>
        <meta name="author" content="Nombre del autor">
        <meta name="description" content="Estructura de un documento HTML5">
        <title>Estructura básica de un documento HTML</title>
    </head>
    <body>
        <!-- Saluda aplicando un estilo CSS al párrafo -->
        <p style="font-size: 20px">Hola mundo</p>
    </body>
</html>
```

# Estilos incrustados
Una opción para facilitar el mantenimiento y separar la estructura del documento de los estilos es definir los estilos incrustados. Esto implicaría definir los estilos en el encabezado del documento de modo que sean aplicados posteriormente.

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <meta charset=utf-8>
        <meta name="author" content="Nombre del autor">
        <meta name="description" content="Estructura de un documento HTML5">
        <title>Estructura básica de un documento HTML</title>

        <!-- Definición de los estilos a incluye en la página -->
        <style>
            p { 
                font-size: 20px ;
            }
        </style>

    </head>
    <body>
        <!-- Saluda aplicando un estilo CSS al párrafo -->
        <p>Hola mundo</p>
    </body>
</html>
```

# Archivos externos
Aunque poner en el encabezado los estilos ofrece una mejora sobre los estilos en línea, no está exento de problemas. Por ejemplo, tenemos una copia de los estilos en todos los documentos lo que hace que los documentos ocupen más espacio. Para evitar esta duplicidad y tener nuestros estilos centralizados en un único fichero, la solución es mover nuestras reglas a un archivo externo.

```css
    /* Contenido del fichero ejemplo.css */
    p { 
        font-size: 20px ;
    }
```

```html
<!DOCTYPE html>
<html lang=es>
    <head>
        <meta charset=utf-8>
        <meta name="author" content="Nombre del autor">
        <meta name="description" content="Estructura de un documento HTML5">
        <title>Estructura básica de un documento HTML</title>

        <!-- Carga la hoja de estilos -->
        <link rel="stylesheet" href="ejemplo.css">

    </head>
    <body>
        <!-- Saluda aplicando un estilo CSS al párrafo -->
        <p>Hola mundo</p>
    </body>
</html>
```

Esta va a ser la forma que vamos a utilizar generalmente para aplicar nuestros estilos a los documentos.