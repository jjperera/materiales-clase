# Introducción
CSS es un lenguaje que permite aplicar estilos visuales a los elementos en un documento. Entre los estilos que podemos aplicar se encuentra:

- Tamaño
- Color
- Fondo
- Borde
- Posición
- ...

Aunque el contexto de aplicación más extendido es el desarrollo web, hay otros contextos en que puede ser aplicado. Así, podemos utilizar hojas de estilos para personalizar aplicaciones implementadas utilizando el framework JavaFX.

# Reglas en CSS
Para definir como han de mostrarse los diferentes elementos de un documento, CSS se vasa en aplicar valores a determinadas propiedades. A continuación tenemos un ejemplo de como se vería una regla en CSS que asigna un valor a un color.

```css
    color: #FFFFFF;
```

Hemos asignado el valor hexadecimal FFFFFFh para asignar un valor al atributo color. En CSS los colores se pueden representar en hexadecimal o en decimal utilizando la función rgb. El equivalente utilizando esta función sería rgb(255,255,255). En ambos casos, definimos las componentes roja, verde y azul del color. En este caso, obtendríamos el color blanco.

Las propiedades pueden ser agrupadas utilizando para ellas las {}. A un grupo de una o más asignaciones se le denomina regla. Un ejemplo de una regla CSS sería:

```css
{
    color: rgb(255,255,255);
    font-size:24px;
}
```

# Selectores
Como es evidente, para que el programa que está aplicando los estilos sepa a que elemento se lo tiene que aplicar será necesario indicárselo de algún modo. Para ello se utilizan los selectores. La función del selector es determinar que elementos de nuestro documento se verán afectados por una regla determinada. Fijémonos en el siguiente ejemplo:

```css
p {
    color: rgb(255,255,255);
    font-size:24px;
}

span {
    color: rgb(255,0,255);
    font-size:12px;
}
```

En el ejemplo, aplicamos la primera regla a todos los elementos p y la segunda a todos los span. También se pueden especificar varios selectores en la misma regla separándolos con ','.

```css
p, span {
    color: rgb(255,255,255);
}
```

# Propiedades comunes
En este apartado voy a poner ejemplos de algunos ejemplos de propiedades muy utilizadas:

## font
Declara varios estilos de texto.

```css
{        
    font: bold 12px arial, sans-serif;

    /** Lo anterior se puede espeficicar utilizando todas las propiedades */
    font-style: normal;
    font-variant: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 3em;
    font-family: sans-serif;
}
```

## color
De esta propiedad hemos visto anteriormente un ejemplo. Permite asignar un color a un determinado elemento.

```css
p {
    color: rgb(255,255,255);
}
```

## background
Permite aplicar estilos al fondo de un elemento. Al igual que ocurre con la fuente, se puede utilizar una única propiedad o varias propiedades individuales.

```css
p {
    background: #000000 url('fondo.jpg') no-repeat;
}
```

## margin
Declara el margen externo de un elemento. El margen será espacio alrededor del elemento. En la [referencia](https://developer.mozilla.org/es/docs/Web/CSS/margin) de mozilla tenemos explicado como se interpretan los valores y otras formas en que podemos especificarlos utilizando propiedades individuales.

```css
p {
    margin: 10px 20px 10px 20px;
}
```

## padding
El relleno o padding hace referencia al margen interno de un elemento. Se refiere al espacio que rodea el contenido del elemento pero se encuentra dentro del borde.


## border
Declara el ancho, estilo y color del borde del elemento. Por ejemplo, con la siguiente definición declaramos un borde sólido de un pixel y de color #990000.

```css
p {
    border: 1px solid #990000;
}
```

