
# Propiedades
Ejercicios relacionados con las diferentes propiedades en CSS. Los ejercicios deben subirse al GIT del alumno en la carpeta css/ejercicios/nombreejer. 

El objetivo es resolver la parte de CSS. Puedes ver el código HTML para orientarte en los elementos que han utilizado o incluso utilizarlo como punto de partida pero, siempre que puedas, te recomiento seguir estos pasos:

1. Intenta resolverlo sin mirar nada.
2. Compara tu solución HTML con la solución planteada. Si hay diferencias, analiza como pueden afectar a la presentación.
3. Haz las correcciones necesarias.
4. Compara tu solución con la solución planteada. Si hubiera diferencias, reflexiona sobre ellas.
5. Haz las correcciones necesarias.

## cssprop01
Ejercicio [Ejercicios resueltos para prácticar 01/Ejemplo/Ejempo]

## cssprop02
Ejercicio [Ejercicios resueltos para prácticar 01/Bloques de texto/The Jargon file]

## cssprop03
Ejercicio [Ejercicios resueltos para prácticar 01/Texto en línea/Currículum Vitae (corto)]

## cssprop04
Ejercicio [Ejercicios resueltos para prácticar 01/Texto en línea/Currículum Vitae (largo)]

## cssprop05
Ejercicio [Ejercicios resueltos para prácticar 01/Texto en línea/Currículum Vitae (Contra Comic Sans)]

## cssprop06
Ejercicio [Ejercicios resueltos para prácticar 01/Clases/Greguerías 1]

- Cada línea de texto es un párrafo.
- Utiliza clases para asignar los diferentes estilos de párrafo

## cssprop07
Ejercicio [Ejercicios resueltos para prácticar 01/Clases/Películas de emojis]

Tienes en la sección recursos del tema CSS un enlace a un documento de referencia donde puedes ver cómo insertar emojis estándar en tu página web sin necesidad de recurrir a recursos externos como imágenes.

## cssprop08
Ejercicio [Ejercicios resueltos para prácticar 01/Bordes y márgenes exteriores e interiores/Refranes sobre educación]

El resultado es algo confuso. Te recomiendo mirar el documento HTML para partir de ahi. Trabaja luego con clases y propiedades de borde para ir formateando cada uno de los párrafos.

## cssprop09
Ejercicio [Ejercicios resueltos para prácticar 01/Bordes y márgenes exteriores e interiores/Citas de Edsger Dijkstra]

En este ejercicio trata de aproximar lo más que puedas. 

- Utiliza una propiedad para convertir el título H1 a mayúsculas.
- Utiliza las propiedades margin-left y margin-right para colocar los párrafos. Recuerda que puedes utilizar porcentajes.
- Juega con las propiedades padding y border para tratar de conseguir colocar el texto de forma similar.

## cssprop10
Ejercicio [Ejercicios resueltos para prácticar 01/Texto 2/Deja vu]

En este ejercicio, trabaja con los márgenes y las propiedades de texto para obtener el efecto deseado.

## cssprop11
Ejercicio [Ejercicios resueltos para prácticar 01/Imágenes de mapa de bits/Florencia]

Utiliza las propiedades float para colocar las imágenes.

## cssprop12
Ejercicio [Ejercicios resueltos para prácticar 01/Secciones/Citas de GK Chersterton]

## cssprop13
Ejercicio [Ejercicios resueltos para prácticar 01/Secciones/Citas de GK Chersterton]

## cssprop14
Ejercicio [Ejercicios resueltos para prácticar 01/Secciones/Necrológicas 2014]





