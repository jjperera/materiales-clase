# Profesor

- Juanjo Perera
- juanjoseperera@iescastelar.com

---
# Contenidos

- HTML
- CSS
- SASS
- Planificación de interfaces web
- Manejo del lienzo en HTML5
- Canvas
- Usabilidad
- Accesibilidad

---
# Pasos previos

- Herramientas para la visualización de temas
- Firefox Developer Edition
- Seguir los pasos en la sección de configuración del entorno de trabajo
- Crear una cuenta en gitlab del IES con nuestro nombre real
- Descargar el repositorio de la asignatura
- Crear un repositorio con el nombre indicado por el profesor
- Invitar al profesor como mainteiner

---