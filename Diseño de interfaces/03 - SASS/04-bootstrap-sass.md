
# Introducción
https://getbootstrap.com/docs/5.0/customize/overview/

Existen dos posibles vías para personalizar bootstrap.

- Extenderlo a partir de los archivos fuente (Vamos a trabajar con este método)
- Extenderlo a partir de los archivos compilados

# Descarga
https://getbootstrap.com/docs/5.0/getting-started/download/#package-managers

Vamos a descargar bootstrap a nuestro proyecto utilizando npm.

```cmd
> npm install bootstrap
```

Debería crear dentro de node_modules nuestro directorio con los fientes de bootstrap.

# Instalación de autoprefixer
Se recomienda la instalación de autoprefixer. Probarlo.


# Personalización utilizando SASS
https://getbootstrap.com/docs/5.0/customize/sass/

De acuerdo con el enlace, siempre que sea posible, tendremos que evitar modificar los archivos de la distribución de bootstrap. Lo que deberíamos hacer sería crear nuestra propia hoja de estilos, importar bootstrap y hacer las modificaciones y extensiones que necesitemos. Tendríamos que tener una estructura de directorios como la siguiente.

- proyecto/
  - scss
    - proyecto.scss -> Mi archivo scss donde incluyo bootstrap
  - node_modules
    - bootstrap
      - js
      - scss

Nosotros vamos a importar todo aunque lo recomendable es importar los módulos que necesitemos para que el css sea más pequeño y contenga lo que necestemos. Tal y como viene en la web, tendríamos que tener un archivo con la siguiente estructura.

```scss
// Custom.scss
// Option A: Include all of Bootstrap

// Include any default variable overrides here (though functions won't be available)

// Asigna valores a varias variables.
$body-bg: #000;
$body-color: #111;
$enable-rounded: false;

// Colores del tema principal. Vienen como variables.
$primary: #0074d9;
$danger: #ff4136;

// Importa bootstrap
@import "../node_modules/bootstrap/scss/bootstrap";

// Then add additional custom code here
```

Todas las variables en bootstrap incluyen default. De modo que pueden ser personalizadas.


# Compilar
Ahora para compilar nuestro archivo scss tendremos que ejecutar la línea de comandos desde el directorio scss.

```cmd
> sass erp.scss ..\css\erp.css
```

El archivo generado sería el css que tendríamos que incluir en nuestro proyecto.

# Personalización 
En este apartado incluyo enlaces a diversas variables que se pueden configurar.

## Options
https://getbootstrap.com/docs/5.0/customize/options/
En este apartado tenéis opciones globales que podéis utilizar para personalizar el tema.


## Color
https://getbootstrap.com/docs/5.0/customize/color/
Aquí tenéis algunas nociones sobre como están organizados los colores en los temas. Esto os puede ayudar a personalizar el color de vuestra aplicación.
