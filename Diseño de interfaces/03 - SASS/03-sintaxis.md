
# Variables
Su uso es muy popular, por ejemplo, en el contexto de los colores y las especificaciones de tamaño. En una variable se puede guardar el valor hexadecimal de un color o ajustar un tamaño fijo utilizando funciones matemáticas. Las variables se introducen en SASS con el símbolo del dólar ($).

```scss
    $bg-color: #df0174;
    $size: 1em;

    body {
        background-color: $bg-color;
        margin: $size * 2;
    }
```

En la denominación de los valores cromáticos en la forma de variables se han consolidado dos principios diferentes. 

- Para algunos desarrolladores es más práctico nombrar el color ($pink), 
- otros prefieren especificar qué propósito debe servir ($bg-color). 


# Valores por defecto
Podemos especificar valores por defecto para las variables en caso de que no tengan un valor ya especificado. Esto se hace añadiendo !default a la asignación de la variable. Asigna el valor únicamente si la variabla no ha sido definida o tiene valor null.

Las asignaciones en este ejemplo van a funcionar únicamente en caso de que no hayan sido asignadas previamente. 

```scss
    /* En alguna parte de mis scss */
    $text-color: #000000 !default;

    /* Posteriormente, al final, se carga esto. Asigno valores por defecto a variables no asignadas */
    $text-color: red;   /* La variable vale red */
    $background-color: $bg-color !default;  /* La variable vale bg-color */
```


# Importar
SASS tiene una directiva muy útil que permite incorporar otros archivos en la hoja de estilos. La información del archivo importado se utiliza como si formara parte del código fuente.

```scss
    @import "variables";
    @import "partials/styles";
    @import "partials/test-a", "partials/test-b";
```


# Parciales (Partials)
En el trabajo con SASS, lo que se importa más a menudo son los llamados partials. Se trata de fragmentos de código con los que se crean módulos que se pueden importar fácilmente una y otra vez. Al nombrar el archivo, **es importante preceder el nombre real con un guion bajo**, el cual informa al compilador de que este archivo no necesita una contrapartida en CSS.

Al importar los parciales el guion bajo se omite. 

Ejemplos de archivos:

- _colors.scss
- _variables.scss



# Mixings (Mezclas)
Reglas fijas que pueden invocarse en la hoja de estilo cuando sea necesario sin tener que volver a insertar el código completo. Esto ayuda a trabajar más rápido y a mantener el código más ligero. Una mixin puede contener todo lo que está permitido en SASS: reglas, parámetros o funciones, pero, incluso aunque tiene un espacio ilimitado, no conviene hacer mixins muy extensos para facilitar el mantenimiento.

```scss
    /* Defino el mixing */
    @mixin big-blue-text {
        font-family: Arial;
        font-size: 25px;
        font-weight: bold;
        color: #0000ff;        
    }

    /* Lo utilizo en una regla */
    .parrafoAzul {
        @include big-blue-text;
    }
```


# Extend
La regla extender permite ahorrar mucho trabajo. La directiva garantiza que todas las propiedades de una clase se transfieran a las de otra. Usando @extend se evita tener que redefinirlo todo. La directiva también funciona como una cadena. Una clase definida por @extend puede a su vez formar parte de una tercera clase.

```scss
    .button-scope {
        margin: 5px;
        border-radius: 2px;
    }
    .home-button {
        @extend .button-scope;
        background-color: $black;
    }
    .back-button {
        @extend .home-button;
    }
```

También podemos tener herencia múltiple

```scss
    $bg-color: #df0174;
    $size: 1em;

    body {
        background-color: $bg-color;
        margin: $size * 2;
        color: #df0174;
    }

    .caja-redondeada {
        margin: 5px;
        border-radius: 10px;
    }

    .caja-azul {
        background-color: blue;
    }
      
    .caja-roja {
        background-color: red;
    }

    .caja-verde {
        background-color: green;
    }

    .mi-caja {
        @extend .caja-redondeada;
        @extend .caja-roja;

        background-color: black;
    }
```

Este sería el resultado. Ahí vemos el punto donde se introduce la regla.

```css
    body {
    background-color: #df0174;
    margin: 2em;
    color: #df0174;
    }

    .caja-redondeada, .mi-caja {
    margin: 5px;
    border-radius: 10px;
    }

    .caja-azul {
    background-color: blue;
    }

    .caja-roja, .mi-caja {
    background-color: red;
    }

    .caja-verde {
    background-color: green;
    }

    .mi-caja {
    background-color: black;
    }
```

En cuanto a la precedencia de las reglas, hay que tener en cuenta, que las reglas extendidas tienen la precedencia correspondiente a la posición en el fichero de la regla extendida, ya que la posición en el CSS resultante no se va a modificar. **Vamos a verlo con el ejemplo**


# Anidación (Nesting)
En HTML se asume que el código se anida en una estructura jerárquica. CSS, en cambio, ignora esta función y obliga al usuario a declarar las propiedades una y otra vez. SASS devuelve la anidación a las hojas de estilo al permitir que las subcategorías hereden las propiedades de la categoría superior.

Por ejemplo, es posible definir la apariencia de los enlaces y especificar en una anidación cómo cambian de color cuando se pasa con el ratón por encima (hover) o cuando ya se han visitado.

```scss
/** Defino las propiedades de un enlace */
a {
  
  color: $blue;

  /** Pseudoclase visited */
  &:visited {
    color: $red;
  }
  
  /** Pseudoclase hover */
  &:hover {
    color: $purple;
  }
}
```

La anidación es una herramienta muy útil para mantener el código fuente de la hoja de estilo ligero y eficiente. Aún así no abuses de ella y trata de mantener el código sencillo.

# Anidamiento de declaraciones de propiedades
En CSS tenemos determinadas propiedades que comparten prefijo en lo que podríamos llamar una especie de Namespace. Ejemplos son **font-**family, **font-**size, **font-**weight. Todas estas propiedades empiezan por **font-**. SASS permite declarar estas propiedades evitando código redundante. Por ejemplo:

```scss
    enlarge {
        
        /* Tamaño de fuente */
        font-size: 14px;

        /* Propiedades de la transición */
        transition: {
            property: font-size;
            duration: 4s;
            delay: 2s;
        }

        /* define el estado para hover */
        &:hover { font-size: 36px; }
    }
```

# Funciones
SASS conoce numerosas funciones que facilitan el trabajo en la hoja de estilo. Se trata de workflows predefinidos que evitan tener que ejecutarlos manualmente. 

Incluye funciones relacionadas con:

- Colores
- Listas
- Cadenas
- Selectores
- Números
- Mapas
- Introspección
- Miscellaneous

Las funciones se insertan en el código siguiendo siempre el mismo patrón: cada función posee un nombre propio y ciertos parámetros que se encierran entre paréntesis separados por comas.

```scss
    $color-1: #ffff00;
    $color-2: #0000ff;

    body {
        background-color: mix($color-1, $color-2, 30%);
    }
```

En el siguiente [enlace](https://sass-lang.com/documentation/modules) disponemos de la lista de módulos donde podemos acceder a las funciones incluidas en SASS.

# Definir funciones
Permite definir funciones específicas para un proyecto, lo que agiliza las fases del trabajo que más se repiten. Con ello se asemejan a las mixins, con la diferencia de que, mientras estas entregan líneas de código como output, **las funciones solo entregan un valor**.

Las funciones se crean con la directiva **@function**, aunque en realidad necesitan siempre dos directivas: además de la inicial @function, se necesita un **@return** anidado con el cual se define el valor de salida.


```scss

    /* Define una constante para el numero de columnas */
    $column-count: 12;

    /** Función para calcular el ancho de columna */
    @function column-width($num) {
        @return $num * 100% / $column-count;
    }

    /** Clase que ocupa tres columnas de ancho. Calcula el ancho en función de ese 3 */
    .three-columns {
        width: column-width(3);
    }
```


# Bucles
En SASS se utilizan para crear bloques de instrucciones que se repiten hasta que tiene lugar una condición determinada. Existen tres directivas diferentes para crear bucles:

## @for


- Tras la directiva primero se especifica una variable cualquiera ($i)
- Si para el inicio se define un valor superior al elegido para el final, SASS cuenta hacia atrás. 
- En SASS #{} es una interpolación. Permite combinar el resultado de una expresión dentro de un bloque de css

```scss

    /* 1 a 4 */
    @for $i from 1 through 4 {
	    .width-#{$i} { width: 10em + $i; }
    }

    /* 1 a 3 */
    @for $i from 1 to 4 {
	    .height-#{$i} { height: 25em * $i; }
    }
```

El resultado del código anterior sería

```scss
    .width-1 {
        width: 11em;
    }
    .width-2 {
        width: 12em;
    }
    .width-3 {
        width: 13em;
    }
    .width-4 {
        width: 14em;
    }
    .height-1 {
        height: 25em;
    }
    .height-2 {
        height: 50em;
    }
    .height-3 {
        height: 75em;
    }
```

## @while
La directiva@while tiene una mecánica muy similar a la de @for pero, mientras que este último tiene puntos fijos de inicio y final, un bucle @while contiene una consulta de tipo lógico

```scss
    
    $i: 1;
    @while $i < 5 {
        .width-#{$i} { width: 10em + $i; }
        $i: $i + 1;
    }
```

## @each
La directiva @each funciona de manera diferente. Este bucle se basa en una lista de datos especificada por el usuario que el bucle recorrerá en cada vuelta.

```scss
    
    /* Define una lista */
    $list: dog cat bird dolphin;
    
    /* Crea el CSS para cada elemento en la lista */
    @each $i in $list {
    	.image-#{$i} { background-image: url('/images/#{$i}.png'); }
    }
```

Esto genera el siguiente código

```css
    .image-dog {
        background-image: url("/images/dog.png");
    }
    
    .image-cat {
        background-image: url("/images/cat.png");
    }
    
    .image-bird {
        background-image: url("/images/bird.png");
    }
    
    .image-dolphin {
        background-image: url("/images/dolphin.png");
    }
```

# La función @if
La función es fácil de explicar. Contiene tres parámetros: 

- la condición 
- primera salida. Se emite si el primer parámetro es verdadero
- segunda salida. Si la condición no se cumple.

```scss
    $black: #000000;
    $white: #ffffff;
    $text-color: $black;

    body {
    	background-color: if($text-color == $black, $white, $black);
    }
```

Otro ejemplo. Vemos como podemos activar o desactivar cierto estilo utilizando una variable.

```scss

    /* Esquinas redondeadas */
    $rounded-corners: false;

    /* Define el estilo para un botón */
    .button {
        border: 1px solid black;

        /* Si están activadas las esquinas redondeadas, las configura */
        border-radius: if($rounded-corners, 5px, null);
    }
```

# La directiva @if
Cuando la funcion @if no sea suficiente, la directiva @if permite definir casos más complejos y realizar diversas comprobaciones como haríamos en un programa normal.

En el siguiente ejemplo tenemos una combinación de un mixin con parámetros y un if

```scss
    
    $black: #000000;
    $white: #ffffff;
    $lightgrey: #d3d3d3;
    $darkgrey: #545454;

    /* Define un mixing que recibe un color como argumento */
    @mixin text-color($color) {
    
        @if ($color == $black) {
            background-color: $white;
    	} 
        @else if ($color == $white) {
            background-color: $black;
	    }
        @else if ($color == $lightgrey) {
            background-color: $black;
	    }
        @else {
            background-color: $white;
	    }
    }

    p {
        /* Calcula el color de fondo en función del color del texto */
        @include text-color($lightgrey);
    }
```

# Comentarios
Con SASS también es útil añadir comentarios al código fuente. Los comentarios contribuyen a que el código sea comprensible para cualquiera que lo lea en otro momento.

```scss
    /* 
       Esto es un comentario.
       Todo lo escrito entre estas marcas
       no se tendrá en cuenta. 
    */

    // Esta línea es un comentario.
    // Y esta línea también.
```

