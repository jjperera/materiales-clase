# Introducción
En primer lugar, vamos a revisar la instalación. Revisar los css y js que hay que incluir. Lo incluiremos todo con npm. Utilizaremos los fuentes minimizados.

Revisar estos apartados.

https://getbootstrap.com/docs/5.1/getting-started/introduction/
https://getbootstrap.com/docs/5.1/getting-started/download/
https://getbootstrap.com/docs/5.1/getting-started/contents/
https://getbootstrap.com/docs/5.1/getting-started/javascript/

# Personalización
En este apartado vamos a repasar la personalización de una aplicación que use bootstrap partiendo de sass.

https://getbootstrap.com/docs/5.1/customize/sass/
https://getbootstrap.com/docs/5.1/customize/options/
https://getbootstrap.com/docs/5.1/customize/color/


# Layout

https://getbootstrap.com/docs/5.1/layout/breakpoints/

https://getbootstrap.com/docs/5.1/layout/containers/

https://getbootstrap.com/docs/5.1/layout/grid/

https://getbootstrap.com/docs/5.1/layout/columns/

https://getbootstrap.com/docs/5.1/layout/gutters/

https://getbootstrap.com/docs/5.1/layout/z-index/

https://getbootstrap.com/docs/5.1/layout/css-grid/

# Menús
Enlaces adicionales aquí
    https://bootstrap-menu.com/
