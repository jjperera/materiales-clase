	<h1>Alta de una nueva familia de productos</h1>	
<!-- Copiamos y pegamos la plantilla el formulario de bootstrap v 5.0-->
<form name="altaFamilia" method="POST" action="indexAdminAlumnos.php?principal=interfaz/vista_admin/altaFamilia.php">
<div class="mx-auto">	
  <div class="mb-31 ">
    <label for="exampleInputEmail1" class="form-label">Nombre</label>
    <input type="text" name="nombre" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" maxlength="10">
    <div id="emailHelp" class="form-text">Nombre de la familia</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Descripción</label>
    <input type="text" name="descripcion" class="form-control" id="exampleInputPassword1">
  </div>
  
  <button name="alta" value="Guardar" type="submit" class="btn btn-primary">Submit</button>
</div>
</form>

<?php 

	//require_once "pojos/FamiliaProducto.php";
	//require_once "persistencia/FamiliasProductos.php";


	if (isset($_POST['alta'])){
		//Estos significa que el usuario ha pulsado el botón submit

		$tFamiliaProducto=FamiliasProductos::singletonFamiliasProductos();
		$nombre=$_POST['nombre'];
		$descripcion=$_POST['descripcion'];

//testear que no se repitan nombres de familias
		//controlar la unicidad de nombres
		$id=1; //cualquier valor xq lo genera la bd
		$idFamilia=1; ///algoritmo que elabore el idFamilia
		$activo=1; //constante cada vez que se trate de dar de alta una nueva familia (por defecto)
		$f=new FamiliaProducto($id,$idFamilia,$nombre,$descripcion,$activo);
		//pregunta de Alvaro

		$insertado=$tFamiliaProducto->addUnaFamiliaProducto($f);

		if ($insertado){
			echo "Se ha insertado satisfactoriamente";
		}
		else{
			echo "Ha habido algún error en la inserción de la familia";
		}

	}

 ?>   

