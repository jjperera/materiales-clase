 <!doctype html> 
 <html lang= "es" > 
    <head> <!-- Required meta tags --> 
        <meta charset= "utf-8" > 
        <meta name= "viewport" content= "width=device-width, initial-scale=1" > 
        <meta name="author" content="">
        
        <!-- Bootstrap CSS en la web--> 

        <link rel= "stylesheet" href= "css/erp.css"> 	
        
        <script src= "https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity= "sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin= "anonymous" defer></script>
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity= "sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin= "anonymous" defer></script> 
        <script src= "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity= "sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin= "anonymous" defer></script> 	
        
        <title>Ejemplo de utilización de webworkers con AJAX</title> 
    </head> 

    <body style="background-color: #F8F9CE"> 

        <form action="">
            <input id="resultado" type="text"/>            
            <button id="lanzarworker">Ejecutar</button>
        </form>
    </body> 
</html> 