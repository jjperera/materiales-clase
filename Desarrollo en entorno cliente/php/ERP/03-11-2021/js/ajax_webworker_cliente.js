
// Obtiene la referencia a los botones de control
const contador = document.getElementById('contador');
const iniciar = document.getElementById('iniciar');
const parar = document.getElementById('parar');

// Asigna los eventos
iniciar.addEventListener('click', iniciarWorker);
parar.addEventListener('click', pararWorker);

let worker = null;

// Funciones para iniciar y detener el worker
function iniciarWorker(e) {
    e.preventDefault();

    try {
        // Crea el worker
        worker = new Worker('js/ww_contador.js');
        
        // Asigna el gestor de eventos para cuando el worker envíe un mensaje
        worker.addEventListener('message', function(event) {
            console.log(event.data);

            // El valor pasado en el evento va al contador
            contador.value = event.data;        
        });

        // Pasa un mensaje al worker. 
        worker.postMessage(1);
    } catch(e) {
        console.log(e);
    }
}

function pararWorker(e) {
    e.preventDefault();

    worker.terminate();
    worker = undefined;
}
