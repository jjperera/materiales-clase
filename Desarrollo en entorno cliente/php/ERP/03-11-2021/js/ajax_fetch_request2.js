

// Obtiene el sitio donde poner el saludo
const saludo = document.getElementById('saludo');

// Petición sin parámetros

let error = false;

// Hace una petición al servidor y muestra la respuesta
fetch('ajax_fetch_saludo.html')
.then(response => {
    if(response.ok == false) { // Guarda la información del error
        error = true;
    } 
    
    // Esto lo hago si o si por tener que devolver una promesa para que
    // funcione el siguiente then. Para un procesamiento más elaborado
    // de errores, necesitaría dar otro tratamiento
    return response.text();
})  // Obtiene el texto
.then(texto => {  // Muestra el texto en consola si no hay error
    if(!error) 
        console.log(texto)
});   


