
console.log("Inicio web worker");

// Variables que definen el estado del contador
let actual = 0;

// Define una función que va a generar el temporizador
function temporizador() {
    console.log("inicio temporizador");

    // Incremento el valor del temporizador
    actual++;

    // Envío un mensaje con el valor actual
    postMessage(actual);

    // Hago que se vuelva a llamar al temporizador en 1 segundo
    setTimeout(temporizador, 1000);

    console.log("fin temporizador");
}


onmessage = function(e) {
    console.log("onmessage "+ e.data);
    
    // Toma el valor de los datos
    actual = Number(e.data);

    // Primera llamada a la función
    temporizador();    
}

console.log("fin web worker");