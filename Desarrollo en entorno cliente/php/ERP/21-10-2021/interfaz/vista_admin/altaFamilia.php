<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Formulario de Alta de una nueva familia</title>
</head>
<body>
	<h1>Alta de una nueva familia de productos</h1>	
	<form name="altaFamilia" method="POST" action="altaFamilia.php">
		Nombre: <input type="text" name="nombre">
		Descripción: <input type="text" name="descripcion">
		<input type="submit" name="alta" value="Guardar">
		<input type="reset" name="reset" value="Limpiar Formulario">

	</form>

<?php 
	require_once "../../pojos/FamiliaProducto.php";
	require_once "../../persistencia/FamiliasProductos.php";

	if (isset($_POST['alta'])){
		//Estos significa que el usuario ha pulsado el botón submit

		$tFamiliaProducto=FamiliasProductos::singletonFamiliasProductos();
		$nombre=$_POST['nombre'];
		$descripcion=$_POST['descripcion'];

//testear que no se repitan nombres de familias
		//controlar la unicidad de nombres
		$id=1; //cualquier valor xq lo genera la bd
		$idFamilia=1; ///algoritmo que elabore el idFamilia
		$activo=1; //constante cada vez que se trate de dar de alta una nueva familia (por defecto)
		$f=new FamiliaProducto($id,$idFamilia,$nombre,$descripcion,$activo);
		//pregunta de Alvaro

		$insertado=$tFamiliaProducto->addUnaFamiliaProducto($f);

		if ($insertado){
			echo "Se ha insertado satisfactoriamente";
		}
		else{
			echo "Ha habido algún error en la inserción de la familia";
		}



	}


 ?>


</body>
</html>