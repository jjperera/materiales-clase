<?php 

require_once 'Conexion.php';
require_once '../../pojos/FamiliaProducto.php';

	class FamiliasProductos {
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonFamiliasProductos(){
			if (!isset(self::$instancia)){
				$miclase = __CLASS__;
				self::$instancia = new $miclase;

			}
			return self::$instancia; //this->$instancia

		}


		//////// programamaos cada una de las funciones que necesitemos
		///// de ataque a la base de datos
		///// CRUD (Create, Read,Update y Delete)

		//Create====Insert


		public function addUnaFamiliaProducto(FamiliaProducto $f){
			//Esta función da de alta una familia nueva en la tabla familias_productos
			try {
				$consulta="INSERT INTO familias_productos (id, id_familia, nombre, descripcion, activo) VALUES (null,?,?,?,?)";
				$idFamilia=$f->getIdFamilia();
				$nombre=$f->getNombre();
				$descripcion=$f->getDescripcion();
				$activo=$f->getActivo();

				$query=$this->db->preparar($consulta);
				@$query->bindParam(1,$f->getIdFamilia());
				$query->bindParam(2,$nombre);
				$query->bindParam(3,$descripcion);
				$query->bindParam(4,$activo);

				$query->execute(); //ejecuta la consulta

				$insertado=true; //
				
			} catch (Exception $e) {
				//echo "Se ha producido un error";
				$insertado=false;	
			}
			return $insertado;
		}


		


}

 ?>