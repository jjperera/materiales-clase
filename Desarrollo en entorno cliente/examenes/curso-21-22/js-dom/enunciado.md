
# Desarrollo Web en Entorno cliente (10 minutos)
Examen de la primera evaluación. Lee atentamente los siguientes puntos antes de comenzar.

- Lee atentamente los enunciados de los ejercicios. Será necesario seguir las instrucciones para que puedan ser corregidos. No seguir las instrucciones implicará  una calificación de 0.
- No pierdas el tiempo haciendo lo que no se pide. Hacer lo que no se pide no subirá la nota.
- Para la realización de los ejercicios el alumno se basará únicamente en los contenidos trabajados durante el curso o lo que se indique en el propio enunciado.
- Es imprescindible nombrar los diferentes elementos como se indica para que se corrija el ejercicio. Cuando no se den indicaciones, quedará a criterio del alumno.
- Se valorarán negativamente aquellos programas que no hagan lo que se pide.
- El alumno debe asegurarse de que ha subido correctamente las tareas siguiendo las instrucciones. En caso contrario obtendrá un 0 en el ejercicio.
- Evita quedarte atascado. Si no sabes resolver algún apartado, pasa al siguiente. 
- Consejo. Antes de abordar la resolución de un ejercicio, lee todo el enunciado del examen atentamente y plantea todas las cuestiones que necesites. Esto ayudará además a que ordenes tu trabajo.
- Si tienes dudas durante el transcurso del examen puedes llamar al profesor.
- Utiliza comentarios para explicar lo que haces, sobre todo cuando pueda haber dudas sobre tu planteamiento. No hacerlo podría conllevar penalizaciones.


## Criterios generales.
Los siguientes criterios serán de aplicación únicamente una vez para todo el examen. Se restará la puntuación indicada de la calificación final.

- No escribir comentarios para explicar lo que se va haciendo: -1
- No referenciar los archivos js en el head o no hacerlo correctamente: -1
- No utilizar correctamente var, let y const: -0.5
- No declarar como constantes las variables que no van a ser modificadas: -0.5
- Asignar eventos directamente en el HTML: -1
- Asignar eventos asignando una función a las propiedades de los elementos: -0.25
- Utilización de eval cuando no se justifica: -1
- Deja en el programa código que no se invoca o no se utiliza: -1
- Lleva a cabo conversiones de tipo no adecuadas o no aplicar las conversiones necesarias: -0.5
- No divide el trabajo en forma de funciones siempre que sea necesario: -0.5
- Modificar el documento HTML facilitado más allá de lo necesario para resolver el ejercicio: -2
- Utilizar código que no es óptimo: -1


## Descripción.
El ejercicio consiste en implementar una especie de hoja de cálculo con unas funcionalidades mínimas. El objetivo es trabajar con algunas de las principales funciones que tenemos disponibles en JavaScript

![Captura de la página](res/pagina.jpg)

Para facilitar su implementación, se facilitan junto con este enunciado los siguientes archivos:

- [hojacalculo.html](solucion/hojacalculo.html): En este archivo no hay que tocar nada, salvo insertar los elementos script necesarios.
- [hojacalculo.css](solucion/hojacalculo.css): Solo para ajustar un poco el contenido del HTML. Este archivo no es necesario tocarlo para nada.

## Entrega
La entrega será un archivo zip comprimido con el nombre en el siguiente formato:

- daw2-apellidos-nombre-examennov.zip

El archivos que contendrá:
    - hojacalculo.html: archivo principal. El alumno solo tendrá que referenciar sus scripts dejando el resto del código sin tocar.
    - hojacalculo.css
    - hojacalculo.js (alumno)
    - calculadora.js (alumno)
    - registro.js (alumno)

---

# Ejercicio 1. Calculadora. (1.75 puntos) (15 min)
Vamos a crear una clase sencilla con la finalidad de definir un objeto y utilizarlo. La calculadora será una clase que tendrá las siguientes características:

- La definición de la clase estará en el archivo **calculadora.js**
- Se utilizará la sintáxis **class** de JavaScript
- La clase se llamará **Calculadora**
- Tendrá los siguientes atributos:
  - **resultado**: se **inicializará a 1** y será donde se almacenará el resultado de cada operación.
- Tendrá las siguientes operaciones/métodos con el nombre indicado:
  - +(operando): sumará operando a resultado
  - -(operando): restará operando a resultado
  - *(operando): multiplicará resultado por operando
  - /(operando): dividirá resultado por operando. En caso de división por cero, **lanzará un mensaje de error** 'No se puede dividir por cero' y no se hará la operación.

## Ayuda
Para definir un método con el nombre + se puede utilizar la siguiente sintaxis:

    '+' (parametros) { }

Nótese que el nombre del método en este caso va entre comillas. 

## Penalizaciones
- No instanciar la calculadora correctamente desde el resto de apartados (solo si se usa class): -0.5
- Definir un objeto como una expresión: -0.5
- Definir el objeto a partir de función constructora: -0.5
- Definir funciones sueltas sin un objeto en el fichero calculadora.js: -1
- Definir funciones dentro del fichero hojacalculo.js: -1.5
- Llevar a cabo los cálculos en línea: 0 puntos

---

# Ejercicio 2. Registro. (1.75 puntos) (15 min)
En nuestra hoja de cálculo vamos a implementar un registro sencillo que permita consultar las operaciones que se vayan realizando. El registro podría llevarse a cabo en cualquier elemento pero vamos a crear una clase que será la encargada de gestionar todas las operaciones de registro. Para facilitar la implementación, vamos a asumir que el elemento utilizado para registrar los mensajes va a ser siempre un textarea.

La clase tendrá las siguientes características:

- La definición de la clase estará en el archivo **logger.js**
- Se utilizará la sintáxis **class** de JavaScript
- La clase se llamará **Logger**
- Tendrá los siguientes **atributos**
  - **elemento**: será el elemento html donde se quiere mostrar el registro. Este elemento será utilizado posteriormente para ir registrando los mensajes. Por hacerlo más sencillo, tiene que ser un elemento de tipo textarea. No es necesario validar que el elemento pasado como argumento sea del tipo correcto.
- Tendrá los siguientes métodos:
  - **constructor**: permite inicializar el objeto asignándole el elemento en el que se va a registrar la información.
  - **inicializar**: vacía el registro.
  - **registrarOperacion(operador, operando)**: registra un mensaje de la forma "operador operando". No añade salto de línea. No es necesario hacer ningún formato especial más. PAra el operador + y el operando 5 registraría "+5".
  - **registrarResultado(resultado)**: Registra el resultado y un salto de línea. Suponiendo que resultado sea 10, mostrará "=10\n"
  - **registrarMensaje(mensaje)**: mostrará el mensaje pasado como argumento y un salto de línea.

## Penalizaciones
- No instanciar el registro correctamente desde el resto de apartados (solo si se usa class): -0.5
- Definir un objeto como una expresión: -0.5
- Definir el objeto a partir de función constructora: -0.5
- Definir funciones sueltas sin un objeto en el fichero logger.js: -1
- Definir funciones dentro del fichero hojacalculo.js: -1.5
- Llevar a cabo los cálculos en línea: 0 puntos


# Ejercicio 3.1. Inicialización. (1 puntos) (10 minutos)
En este ejercicio vamos a crear la estructura de nuestro fichero hojacalculo.js. Tendremos que llevar a cabo las siguientes operaciones:

- Definir variables (con el modificador adecuado) para los siguientes elementos html que se encuentran adecuadamente identificados. Estos elementos serán utilizados en el resto de apartados.
  - cuerpo: elemento cuerpo en el html
  - total: elemento total en el html
  - registro: elemento registro en el html
  - operador: elemento operador en el html
  - logger: instancia de la clase Logger

- Cuando se termine de cargar la página, tendrán que llevarse a cabo las siguientes acciones:
  - Se pondrán todas las celdas a 0.
  - Se registrará el mensaje "Hoja de cálculo inicializada" utilizando la instancia del Logger.

## Penalizaciones
- No declarar ninguna variable. 0
- No declarar alguna variable. -0.5
- No inicializar la página. -0.5

# Ejercicio 3.2. Botón inicializar. (0.5 puntos) (5 minutos)
Se va a configurar el botón "Inicializar campos" para que haga de nuevo una inicialización de la página web. Dicho botón deberá:

- Poner **todas las celdas a 0**
- **registrará el mensaje** "Hoja de cálculo inicializada"

## Penalizaciones
- Se duplica el código del 3.1. -0.25
- No se ponen las celdas a 0. -0.25
- No se registra el mensaje. -0.25

# Ejercicio 3.3. Total por fila (2.5 puntos) (30 min)
En este ejercicio vamos a calcular el total por fila. Para ello vamos a utilizar gestionar el evento click de modo que cuando se pulse sobre uno de los botones = en las columnas, se aplique la operación seleccionada a todos los elementos y se guarde el resultado en la columna de la derecha. 

En este apartado tendremos que hacer lo siguiente:

- Asignar el gestor de evento necesario de modo que al hacer click en cualquiera de los botones = (menos el de totales) se ejecute la operación seleccionada para las celdas de la misma fila.
- La operación a realizar será la que esté seleccionada en el combo
- Se creará una instancia de calculadora. Esta instancia se utilizará para llevar a cabo las operaciones. La instancia no será reutilizada para cada ejecución del evento.
- Tener en cuenta que tras la inicialización la calculadora tendrá el valor 1. Esto es así para facilitar la implementación.
- No será necesario tener en cuenta el orden de los campos, pero sí es importante que se seleccionen todos para realizar la operación.
- Para seleccionar los campos a operar se tendrá que utilizar alguna de las funciones disponibles del tipo getElement y similares.
- Para seleccionar elementos que no sean de solo lectura, se puede utilizar el selector "input:not([readonly])".
- Se registrará cada una de las operaciones realizadas utilizando la instancia del logger.
- Se registrará el resultado utilizando la instancia del logger.
- En caso de excepción, se debe:
  - Mostrar el mensaje de error en el logger
  - Volver a lanzar la excepción y, por lo tanto, no terminar la operación
- Si la operación finaliza correctamente, se guardará el resultado en la casilla correspondiente a la derecha del botón.

## Penalizaciones
- Se asigna el gestor de evento a todos los elementos en lugar de al padre: -0.5
- No se selecciona el padre correcto: -0.5
- No se detiene la propagación del evento hacia el padre: -0.5
- No se registran los mensajes en el logger: -0.5
- No se gestiona correctamente la excepción: -0.5
- No se obtienen todos los elementos con una llamada a función getElement o similares sino que se obtienen los elementos independientes: -1
- No se utilizan bucles para realizar la operación: -1

# Ejercicio 3.4. Total general (1.5 puntos) (20 min)
Al pulsar sobre el botón se calculará el total genral. Esto debe hacerse del siguiente modo:

- Antes de empezar se registrará el mensaje "Calcular total general" en el logger
- Calcular los totales para cada fila.
- Calcular el total a partir de los totales de cada fila.
- El operador a utilizar será el que esté seleccionado.
- Se creará una instancia de calculadora. Esta instancia se utilizará para llevar a cabo las operaciones.
- Tener en cuenta que tras la inicialización la calculadora tendrá el valor 1. Esto es así para facilitar la implementación.
- Se registrará cada una de las operaciones realizadas.
- Se registrará el resultado.
- Mostrar en el registro un mensaje del tipo Total de fila XX = Total calculado.
- En caso de excepción, se debe:
  - Mostrar el mensaje de error en el logger
  - Volver a lanzar la excepción y, por lo tanto, no terminar la operación
- Si la operación se hace correctamente, se guardará el resultado en la casilla correspondiente a la derecha del botón.

## Penalizaciones
- Se duplica código en lugar de definir e invocar funciones: -1
- No se registran los mensajes indicados en el logger: -1
- No se gestiona correctamente la excepción: -0.5
- No se obtienen todos los elementos con una llamada a función getElement o similares sino que se obtienen los elementos independientes: -1
- No se utilizan bucles para realizar la operación: -1


# Ejercicio 3.5. Detección de errores. (1 punto) (10 minutos)
Cuando se introducen datos, podría ocurrir que nos equivoquemos y no introduzcamos números. En este apartado vamos a marcar los campos con errores. Para ello vamos a:

- Asignar el evento necesario para que cuando cambia un campo se ejecute una función.
- Si el valor de la celda no es un número válido se pondrá el fondo rojo.
- Si el valor de la celda es válido, se pondrá el color de fondo a inherit.
- Cuando una celda cambie se registrará un mensaje de la forma:
   - Celda FILA, COLUMNA = Nuevo valor / Error. En caso de que no sea un número se motrará error. 

## Penalizaciones
- No utilizar el evento correcto. -0.5
- Asignar el gestor de evento a todos los campos en lugar de utilizar el padre adecuado. -0.5
- No detiene la propagación de eventos. -0.5
- No muestra el mensaje de log como se indica. -0.5
- No cambia el color de fondo: -0.5





