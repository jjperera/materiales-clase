
/* Calculadora que tiene las operaciones para suma y resta */
class Calculadora {
    resultado = 1;  //IMPORTANTE

    '+' (operando) { 
        this.resultado += operando;
    }

    '-' (operando) {
        this.resultado -= operando;
    }

    '*' (operando) {
        this.resultado *= operando;
    }

    '/' (operando) {
        if(operando == 0) {
            throw 'No se puede dividir por cero'
        }
        this.resultado /= operando;
    }
}

