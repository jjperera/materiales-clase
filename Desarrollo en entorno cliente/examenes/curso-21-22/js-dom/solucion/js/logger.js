
class Logger {

    // Elemento HTML donde se va a ir almacenando el registro
    elemento =  null;

    constructor(elemento) {
        this.elemento = elemento;
        this.inicializar();
    }

    /* Pone a cero el registro */
    inicializar() {
        this.elemento.value = '';
    }

    /* Registra una nueva operación. Las operaciones nuevas no implican un retorno de carro */
    registrarOperacion(operador, operando) {
        this.elemento.value = this.elemento.value + operador + operando;
    }

    /* Registrar el resutado implica un regorno de carro */
    registrarResultado(resultado) {
        this.elemento.value = this.elemento.value + '=' + resultado + '\n';
    }

    /* Registrar mensaje y un retorno de carro */
    registrarMensaje(mensaje) {
        this.elemento.value = this.elemento.value + mensaje + '\n';
    }
}