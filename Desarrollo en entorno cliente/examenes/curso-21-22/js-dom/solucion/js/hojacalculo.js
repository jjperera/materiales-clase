

// Obtiene referencias a algunos elementos que se van a utilizar posteriormente
const cuerpo = document.getElementById("cuerpo");
const total = document.getElementById("total");
const registro = document.getElementById("registro");
const operador = document.getElementById("operador");
const logger = new Logger(registro.getElementsByTagName('textarea')[0]);

// Asigna un gestor de eventos para inicializar nuestra hoja de cálculo
window.addEventListener('load', onLoad);


/******************************************************************************************/
/* Funciones ******************************************************************************/
/******************************************************************************************/

/* Inicializa todos los campos a 0 */
function inicializarHojaCalculo() {

    // Pone todas las celdas a 0
    const celdas = document.getElementsByTagName('input');
    for(let celda of celdas) {
        celda.value = '0';
    }

    // Pone a 0 el registro
    logger.inicializar();
    logger.registrarMensaje('Hoja de cálculo inicializada');
}


/* 
 * Suma una fila.
 *
 * Recibe el botón que se ha pulsado
 * Hacerlo con un único gestor de evento al 100%. Si se usa uno por fila menos puntos.
 */
function onFilaClick(e) {

    // Detiene la propagación del evento
    e.stopPropagation();

    // Obtiene la fila sobre la que se ha hecho click
    const boton = e.target;    

    // Si no es un botón termina aquí
    if(boton.className != 'operar') {
        return;
    }

    // Calcula el total para la fila pasada como argumento
    calcularTotalFila(boton.parentNode);
}

function calcularTotalFila(fila) {

    // Registra la operación a realizar
    logger.registrarMensaje('Calcular total fila : '+fila.className);

    // Obtiene todas las celdas en la fila
    const celdas = fila.querySelectorAll("input:not([readonly])");

    // Calcula el resultado
    const resultado = operar(celdas);

    const res = fila.querySelector('.RES');
    res.value = resultado;
}


function onTotalGeneralClick(e) {

    // Registra la operación a realizar
    logger.registrarMensaje('Calcular total general');

    // Detiene la propagación del evento
    e.stopPropagation();    

    // Obtiene todos los botones
    const filas = cuerpo.getElementsByTagName('div');
    for(let fila of filas) {
        // Calcula el total para esta fila
        calcularTotalFila(fila);
    }

    // Ahora calcula el total general a partir de los totales parciales
    const celdas = cuerpo.getElementsByClassName('RES');

    // Calcula el resultado
    const resultado = operar(celdas);

    const res = total.querySelector('.RES');
    res.value = resultado;
}

/* Hace la oepracion en operador para todas las celdas pasadas como argumento */
function operar(celdas) {
    
    try {
        // Suma todas las celdas utilizando la calculadora    
        const calculadora = new Calculadora();
        for(let celda of celdas) {        
            calculadora[operador.value](Number(celda.value));

            logger.registrarOperacion(operador.value, celda.value);
        }

        // Almacena el resultado
        logger.registrarResultado(calculadora.resultado);
        return calculadora.resultado;
    } catch(error) {
        logger.registrarMensaje('ERROR: '+error);
        throw error;
    }
}

function onCeldaChange(e) {

    // Detiene la propagación del evento
    e.stopPropagation();

    // Si la celda no contiene un valor numérico, la pone en rojo
    const celda = e.target;
    if(isNaN(celda.value)) {
        celda.style['background-color'] = 'red';
    } else {
        celda.style['background-color'] = 'inherit';
    }
}


/******************************************************************************************/
/* Gestores de eventos ********************************************************************/
/******************************************************************************************/
function onLoad() {
    
    // Gestor de eventos para el botón inicializar
    document.getElementById('inicializar').addEventListener('click', inicializarHojaCalculo);

    // Se puede asignar el evento asignándolo al cuerpo.
    // Hacerlo asignándolo a cada uno puntúa menos
    cuerpo.addEventListener('click', onFilaClick);

    // Calcula el total para todos los elementos
    total.getElementsByClassName('operar')[0].addEventListener('click', onTotalGeneralClick);

    // Asigna el gestor de eventos a todas las celdas
    cuerpo.addEventListener('change', onCeldaChange);

    // Llama a inicializar la hoja de cálculo
    inicializarHojaCalculo();
}

