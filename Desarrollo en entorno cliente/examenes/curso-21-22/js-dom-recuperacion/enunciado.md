
# Recuperación Desarrollo Web en Entorno cliente (10 minutos)
Examen de la primera evaluación. Lee atentamente los siguientes puntos antes de comenzar.

- Lee atentamente los enunciados de los ejercicios. Será necesario seguir las instrucciones para que puedan ser corregidos. No seguir las instrucciones implicará una calificación de 0.
- No pierdas el tiempo haciendo lo que no se pide. Hacer lo que no se pide no subirá la nota.
- Para la realización de los ejercicios el alumno se basará únicamente en los contenidos trabajados durante el curso o lo que se indique en el propio enunciado.
- Es imprescindible nombrar los diferentes elementos como se indica para que se corrija el ejercicio. Cuando no se den indicaciones, quedará a criterio del alumno.
- Se valorarán negativamente aquellos programas que no hagan lo que se pide.
- El alumno debe asegurarse de que ha subido correctamente las tareas siguiendo las instrucciones. En caso contrario obtendrá un 0 en el ejercicio.
- Evita quedarte atascado. Si no sabes resolver algún apartado, pasa al siguiente. 
- Consejo. Antes de abordar la resolución de un ejercicio, lee todo el enunciado del examen atentamente y plantea todas las cuestiones que necesites. Esto ayudará a que ordenes tu trabajo.
- Si tienes dudas durante el transcurso del examen puedes llamar al profesor.
- Utiliza comentarios para explicar lo que haces, sobre todo cuando pueda haber dudas sobre tu planteamiento. No hacerlo podría conllevar penalizaciones.


## Criterios generales.
Los siguientes criterios serán de aplicación únicamente una vez para todo el examen. Se restará la puntuación indicada de la calificación final.

- No escribir comentarios para explicar lo que se va haciendo: -1
- No referenciar los archivos js en el head o no hacerlo correctamente: -1
- No utilizar correctamente var, let y const: -0.5
- No declarar como constantes las variables que no van a ser modificadas: -0.5
- Asignar eventos directamente en el HTML: -2
- Asignar eventos asignando una función a las propiedades de los elementos: -1
- Utilización de eval cuando no se justifica: -1
- Deja en el programa código que no se invoca o no se utiliza: -1
- Lleva a cabo conversiones de tipo no adecuadas o no aplicar las conversiones necesarias: -0.5
- No divide el trabajo en forma de funciones siempre que sea necesario: -0.5
- Modificar el documento HTML facilitado más allá de lo necesario para resolver el ejercicio: -2
- Utilizar código que no es óptimo: -1


## Descripción.
El ejercicio consiste en implementar una página parecida a una hoja de cálculo que va a hacer únicamente operaciones de descomposición y composición de un número. El objetivo es trabajar con algunas de las principales funciones que tenemos disponibles en JavaScript

![Captura de la página](res/pagina.jpg)

Para facilitar su implementación, se facilitan junto con este enunciado los siguientes archivos:

- [examen.html](enunciado/examen.html): En este archivo no hay que tocar nada, salvo insertar los elementos script necesarios.
- [examen.css](enunciado/examen.css): Solo para ajustar un poco el contenido del HTML. Este archivo no es necesario tocarlo para la resolución del ejercicio.
- [examen.js](enunciado/examen.js): Archivo js con algunos comentarios e instrucciones para organizar la resolución del ejercicio.


## Entrega
La entrega será un archivo zip comprimido con el nombre en el siguiente formato:

- daw2-apellidos-nombre-recuperacion1.zip

El archivos que contendrá:
- examen.html: archivo principal. El alumno solo tendrá que referenciar sus scripts dejando el resto del código sin tocar.
- examen.css: se entrega el archivo sin modificar
- examen.js (alumno)
- logger.js (alumno)

---

# Ejercicio 1. Registro. (2 puntos) (20 min)
En nuestra hoja de cálculo vamos a implementar un registro sencillo que permita consultar las operaciones que se vayan realizando. El registro podría llevarse a cabo en cualquier elemento pero vamos a crear una clase que será la encargada de gestionar todas las operaciones de registro. El elemento donde se van a registrar los mensajes va a ser una tabla, de modo que, los diferentes mensajes que se van a registrar van a ser filas nuevas en la tabla.

La clase tendrá las siguientes características:

- La definición de la clase estará en el archivo **logger.js**
- Se utilizará la sintáxis **class** de JavaScript
- La clase se llamará **Logger**
- Tendrá los siguientes **atributos**
  - **elemento**: será el elemento html donde se quiere mostrar el registro. Este elemento será utilizado posteriormente para ir registrando los mensajes. No es necesario validar que el elemento pasado como argumento sea del tipo correcto. Este elemento será de tipo tabla.
  - **registroVacio**: Variable que se utilizará para controlar cuando el registro no contiene mensajes. Si vale true, el registro está vacío.
- Tendrá los siguientes métodos:
  - **constructor**: permite inicializar el objeto asignándole el elemento en el que se va a registrar la información. Deberá además inicializar el logger.
  - **inicializar**: vacía el registro. Cuando el registro esté vacío debe mostrar el mensaje "El registro está vacío".
  - **registrarMensaje(mensaje)**: Añade un mensaje al registro en forma de una nueva fila de la tabla. El mensaje estará compuesto por dos columnas. 
      - Timestamp: Marca de tiempo de cuando se ha añadido este mensaje. Puedes utilizar la expresión (new Date()).toLocaleString('es-ES') para obtener el valor.
      - Texto: El texto en el mensaje. Es el texto que se pasa como argumento. 
  - **registrarResultadoFila(fila, valor)**: Pasado el número de fila y el valor asignado registra un mensaje que tendrá el formato: Fila $FILA:$Valor.

## Ejemplo
Aquí tenéis un ejemplo del registro de mensajes

![Registro](res/registro.jpg)

## Penalizaciones
- No instanciar el registro correctamente desde el resto de apartados (solo si se usa class): -0.5
- Definir un objeto como una expresión: -0.5
- Definir el objeto a partir de función constructora: -0.5
- Definir funciones sueltas sin un objeto en el fichero logger.js: -1
- Definir funciones dentro del fichero examen.js: -1.5
- Llevar a cabo los cálculos en línea: 0 puntos

--- 

# Ejercicio 2. Inicialización. (1.5 puntos) (15 minutos)
En este ejercicio vamos a crear la estructura de nuestro fichero examen.js. Tendremos que llevar a cabo las siguientes operaciones:

- **2.1 Definir variables** (con el modificador adecuado) para los siguientes elementos html que se encuentran adecuadamente identificados. Estos elementos serán utilizados en el resto de apartados.
  - **calcular**: botón calcular en el html
  - **operacion**: Select en el html que selecciona el tipo de operación
  - **semilla**: Campo en el html que permite introducir el valor a partir del cual se van a realizar las operaciones.
  - **cuerpo**: elemento cuerpo del html
  - **registro**: elemento registro en el html
  - **logger**: instancia de la clase Logger que se va a utilizar para registrar algunos mensajes.

- **2.2** Además, cuando se termine de cargar la página, tendrán que llevarse a cabo las siguientes acciones:
  - **2.3** Se pondrán todas las celdas a ''. (tiene que ser una función)
    en el apartado correspondiente.
  - **2.4**Se registrará el mensaje "Aplicación inicializada" utilizando la instancia del Logger.

## Penalizaciones
- No declarar ninguna variable. 0
- No declarar alguna variable. -0.5
- No poner las celdas a 0 -0.5
- No registrar el mensaje -0.5
- No estructurar en funciones -0.5
- No hacer la inicialización cuando termina de cargar la página -0.5

---

# Ejercicio 3. Botón inicializar. (1 puntos) (15 minutos)
Se va a configurar el botón "Inicializar campos a blanco" para que haga de nuevo una inicialización de la página web. Dicho botón deberá:

- 3.1 Cuando se haga click en el botón, se inicializan los campos del mismo modo que en 2.3. Se debe reutilizar la función definida previamente en el ejercicio 2.
- 3.2 **registrará el mensaje** "Aplicación inicializada"
- 3.3 Poner **todas las celdas a ''**

## Penalizaciones
- Se duplica el código del ejercicio 2.3 en lugar de definir una función -0.5
- No se asigna el gestor de eventos 0
- No se ponen las celdas a ''. -0.5
- No se registra el mensaje. -0.5

---

# Ejercicio 4. Gestión de cambios. (1 puntos) (15 minutos)
Vamos a hacer que nuestro formulario reaccione a diversos cambios realizados en los campos de entrada para lo que tendremos que asignar algunos gestores de eventos.

- 4.1 Cuando cambiamos el tipo de operación seleccionada en el combo deberemos 
  - 4.1.1 inicializar los campos del formulario del mismo modo que en el 2.3. Deberás reutilizar la función.
  - 4.1.2 Registrar  mensaje "Se ha cambiado un campo".
  - 4.1.3 Registrar mensaje "Inicializando formulario".
- 4.2 En el momento que se cambie el contenido de un campo y antes de abandonarlo, se deberá hacer lo mismo que en el punto 4.1 Se debe reutilizar todo el código que sea posible.
- 4.3 Si se cambia el valor del campo semilla, al abandonar el campo se debe mostrar en el registro el mensaje "semilla cambiada a : valor". No olvides definir una función

## Penalizaciones
- Se duplica el código del ejercicio 2.3 en lugar de definir una función -0.5
- No se asigna gestor de eventos en el combo. -0.25
- No se asigna gestor de eventos a los campos. -0.25
- Se asigna el gestor de eventos a los campos individuales en lugar de al contenedor. -0.25
- No se asigna gestor de eventos en el campo semilla. -0.25
- No utilizar el evento adecuado. -0.25
- No se registran los mensajes indicados. -0.25 por mensaje.
- Se duplica código. -0.25

---

# Ejercicio 5. Calcular (2.5 puntos) (30 min)
En este ejercicio se va a llevar a cabo el cálculo. Se deberán implementar las operaciones componer y descomponer. Antes de describir el enunciado describo las dos operaciones.

## Operación descomponer
La operación descomponer, dado un número, lo va descomponiendo en la siguiente fila entre el número de casillas que hay de modo que la suma de todas las casillas de una fila inmediatamente inferior es igual al valor de todas las casillas de la fila actual. La casilla más a la derecha contendrá el número de campos en la fila actual. El número de partida será el valor en semilla. A continuación pongo una captura de pantalla del estado del formulario a partir del valor semilla 5 para descomponer. Incluyo el registro que tendrá que generarse a medida que se lleva a cabo la operación.

![Descomponer](res/descomponer.jpg)

## Operación componer
La operación componer empezará por la última fila y lo que hará es que el valor de todas las casillas de la fila inmediatamente superior será igual a la suma de todas las casillas de la fila actual. A continuación tienes una captura.

![Descomponer](res/componer.jpg)

## Enunciado del ejercicio.
De acuerdo con la descripción anterior, deberás resolver los siguientes apartados.

- 5.1 Cuando se haga click en el botón calcular se debe llamar a una función. Dicha función, en función de la opción seleccionada en el combo, llamará a componer o descomponer.
- 5.2 Implementa una función descomponer. Dicha función debe recibir como parámetro la fila en que va a empezar la descomposición. Recuerda que la operación va de arriba a abajo. En la explicación tienes un ejemplo de la salida para un número determinado. Esta función debe ser invocada desde el apartado 5.1
- 5.3 Implementa una función componer. Dicha función debe recibir como argumento la fila en que va a empezar la composición. Recuerda que la ejecución va de abajo a arriba. Esta función debe ser invocada desde el apartado 5.1

## Penalizaciones
- No se asigna gestor de eventos al botón calcular. -0.5
- No se implementa la función descomponer. -1
- No se implementa la función componer. -1
- No se invoca a la función correcta en función de la opción en el combo. -0.5
- Las funciones no funcionan correctamente -1

---

# Ejercicio 6. Calcular desde fila (2 puntos) (30 min)
En este ejercicio vamos a reutilizar las funciones del ejercicio 5 para implementar la funcionalidad de cada una de las filas. Debes tener en cuenta, que en este caso lo que se hará será:

- Composición desde la fila seleccionada hasta la primera. 
- Descomposición desde la fila inferior a la fila en que se ha hecho click hasta la última.

Para un 5, la salida obtenida pulsando sobre la 5ª fila sería:

![Ejercicio 6](res/ejercicio6.jpg)

En este ejercicio tendrás que llevar a cabo las siguientes tareas.

- 6.1 Asignar un gestor de eventos, de modo que al pulsar sobre los botones en las filas se comience la operación en la fila en que se encuentra el botón seleccionado. Deberás crear una función que gestione dicho evento.
- 6.2 Implementar la función que va a llevar a cabo el cálculo a partir de la fila indicada. Dicha función debe:
    - Utilizar las funciones componer y descomponer creadas anteriormente.
    - Inicializar los campos antes de comenzar. Esto lo hará también llamando a la función implementada anteriormente.
    - Llamar a componer desde la fila en que se ha hecho click.
    - Llamar a descomponer desde la fila inmediatamente inferior a la fila en que se ha hecho click.

## Penalizaciones
- Se asigna el gestor de eventos a los botones individuales en lugar de al contenedor. -0.5
- No crear una función para gestionar el evento. -0.5
- No implementar la función correctamente -2


