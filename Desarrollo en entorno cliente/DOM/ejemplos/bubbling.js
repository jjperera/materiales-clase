
"use strict";

// Importante. Debe invocarse en el onloads
console.log("Secuencia de inicialización iniciada");

function areaOnClick(e) {
    
    // Posición del ratón
    let x = e.offsetX;
    let y = e.offsetY;

    // Asigna el valor
    document.getElementById('x').textContent = x;
    document.getElementById('y').textContent = y;

    console.log("Target actual : "+e.currentTarget.id);
    console.log("Target : "+e.target.id);

    // Con esto paramos la propagación
    e.stopPropagation();
}

/** 
 * Inicializa la página
 */ 
function inicializar() {
    document.getElementById('area1').addEventListener('click', areaOnClick);
    document.getElementById('area2').addEventListener('click', areaOnClick);
}

// Cuando termine de cargarse la página va a inicializar el script
window.addEventListener('load', inicializar);

console.log("Secuencia de inicialización finalizada");
