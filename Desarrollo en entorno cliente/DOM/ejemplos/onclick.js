
"use strict";

// Importante. Debe invocarse en el onloads
console.log("Secuencia de inicialización iniciada");

function areaOnClick(e) {
    
    // Posición del ratón
    let x = e.offsetX;
    let y = e.offsetY;

    // Asigna el valor
    document.getElementById('x').textContent = x;
    document.getElementById('y').textContent = y;
}

/** 
 * Inicializa la página
 */ 
function inicializar() {
    document.getElementById('area').addEventListener('click', areaOnClick);
}

// Cuando termine de cargarse la página va a inicializar el script
window.addEventListener('load', inicializar);

console.log("Secuencia de inicialización finalizada");
