
# Introducción
En esta unidad vamos a poner el foco en la programación de JavaScript en el navegador. Cuando un programa se ejecuta en este entorno de ejecución, tenemos acceso a diversos objetos que no tendremos disponibles en otros. En el siguiente gráfico se muestra la jerarquía de objetos que nos vamos a encontrar en el navegador.

![DOM y BOM](res/DOMBOM.svg)

Aquí podemos distinguir entre dos ramas o tipos de objetos. Aquellos que son utilizados para describir el documento cargado en ese momento y los que permiten acceso a el entorno de ejecución. O sea, el navegador.

# BOM
El BOM (Browser Object Model) hace referencia a los objetos que tenemos disponibles para acceder a las funciones del navegador. Entre algunos de los objetos tendremos:

- window: el raíz de la jerarquía
- history: para acceder a la historia del navegador
- location: la barra de direcciones

Estos objetos permiten, como se indica, acceder a funcionalidades que no tienen tanto que ver con el documento en sí, sino con la plataforma donde el script se ejecuta. 

# DOM
El DOM (Document Object Model) hace referencia a el modelo de objetos del documento. Digamos que cuando nuestro navegador carga un documento, genera una jerarquía de objetos que representan el documento. Estos objetos son visibles al código JavaScript que se ejecuta en ese documento, de modo que, puede llevar a cabo diversas operaciones con dicho código. 

En la siguiente imagen tenemos el dom correspondiente a un documento ejemplo. Como podemos ver, el DOM tendrá una estructura arborea del mismo modo que el HTML.

![DOM](res/DOM.png)
