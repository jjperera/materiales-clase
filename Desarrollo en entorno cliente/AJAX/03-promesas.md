# Promises
Es un objeto ulizado en computación asíncrona. Representa un valor que puede estar disponible en el futuro.

# El objeto Promise
Podemos crear un objeto Promise utilizando la siguiente sintaxis

```javascript
Promise p = new Promise( /* ejecutor */ function(resolver, rechazar) { ... } );
```

Tenemos que tener en cuenta que:
- La función que pasamos como argumento recibe dos parámetros:
  - resolver: función que se llama desde el ejecutor para resolver la promesa.
  - rechazar: función que se llama desde el ejecutor para rechazar la promesa.

La función ejecutor es ejecutada por la implementación de la Promesa que pasa además como 
argumento las funciones resolver y rechazar. 

La función ejecutor seguirá los siguientes pasos:

1. Ejecuta un trabajo
2. Cuando finaliza la ejecución del trabajo llama a la función resolver
3. Si ha ocurrido un error rechaza la promesa

# Estados
La Promesa puede estar en los siguientes estados

- pendiente: no se ha cumplido o rechazado
- cumplida: se ha completado la operación satisfactoriamente
- rechazada: se ha rechazado la operación debido a un fallo

# El método then()
Este método *retorna una promesa*. Recibe como parámetro dos funciones para los casos
de éxito y fallo. Esta es la sintaxis:

```javascript
  // Indica que hacer en caso de que la promesa se cumpla o rechace
  // Se puede omitir el método para el rechazo
  p.then(alCumplir[, enRechazo]);

  // Mismo ejemplo pero definiendo las funciones como expresión de funcuón
  p.then(function(value) {
     // cumplimiento

     // Devuelve algo

  }, function(reason) {
     // rechazo

     // Devuelve la razón del rechazo
  });
```

## Tener en cuenta que
- then Retorna un Promise, de modo que se pueden encadenar .then. 
- alCumplir recibe como argumento el valor de cumplimiento
- enRechazo recibe como argumento la razón del rechazo

## Valor de retorno
Se puede devolver un valor o una promesa. La promesa devuelva puede estar pendiente o 
resuleva. Si está pendiente se ejecutará de forma asíncrona.

# El método catch())
Retorna una PRomise y se ejecuta solo cuando la Promise se marca como Reject. Es equivalente 
a la segunda función en el then.

```javascript
p.catch(onRejected);

p.catch(function(reason) {
   // rejection
});
```

Ejemplos:

```javascript
var p1 = new Promise(function(resolve, reject) {
  resolve('Success');
});

p1.then(function(value) {
  console.log(value); // "Success!"
  throw 'oh, no!';
}).catch(function(e) {
  console.log(e); // "oh, no!"
}).then(function(){
  console.log('after a catch the chain is restored');
}, function () {
  console.log('Not fired due to the catch');
});

// The following behaves the same as above
p1.then(function(value) {
  console.log(value); // "Success!"
  return Promise.reject('oh, no!');
}).catch(function(e) {
  console.log(e); // "oh, no!"
}).then(function(){
  console.log('after a catch the chain is restored');
}, function () {
  console.log('Not fired due to the catch');
});
```

```javascript
// Hacer un throw llamará al método catch
var p1 = new Promise(function(resolve, reject) {
  throw 'Uh-oh!';
});

p1.catch(function(e) {
  console.log(e); // "Uh-oh!"
});

// Los errores que se lancen dentro de funciones asíncronas actuarán como errores no capturados
var p2 = new Promise(function(resolve, reject) {
  setTimeout(function() {
    throw 'Uncaught Exception!';
  }, 1000);
});

p2.catch(function(e) {
  console.log(e); // Nunca será llamado
});

// Errores lanzados después de resolve() serán omitidos
var p3 = new Promise(function(resolve, reject) {
  resolve();
  throw 'Silenced Exception!';
});

p3.catch(function(e) {
   console.log(e); // Nunca será llamado
});
```

# Ejemplos
A continuación algunos ejemplos.

```javascript
// Creo el objeto Promise. La función ejecutor se va a invocar inmediatamente
// en segundo plano.
let miPrimeraPromise = new Promise(
    // Defino mi función con los parámetros de entrada las funciones
    // resolve y reject
    (resolve, reject) => {

        // Defino un timeout de 250 milisegundos
        setTimeout(function(){
            
            // Al llamar a esta función (pasada como argumento al ejecutor)
            // se resuelve la promesa
            resolve("¡Éxito!"); 
        }, 250);
    });

// Con esta construcción defino lo que pasará
miPrimeraPromise.then((successMessage) => {
  // succesMessage es lo que sea que pasamos en la función resolve(...) de arriba.
  // No tiene por qué ser un string, pero si solo es un mensaje de éxito, probablemente lo sea.
  console.log("¡Sí! " + successMessage);
});
```


# Referencias
- https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Promise
- https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Using_promises


