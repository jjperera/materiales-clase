# Las palabras async y await
Estas están disponibles desde la versión ES2017. Permiten simplificar el uso síncrono
de promesas.

- *Modificador de función async*: permite declarar una función asíncrona.
   La declaración retorna una [AsyncFunction](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/AsyncFunction)
- *El operador await*: se utiliza para esperar a una Promise. Permite ejecutar una promesa de forma síncrona. Puede ser utilizado únicamente
   dentro de una función asíncrona. 


# El modificador de funcón async
[Referencia de Mozilla](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/async_function)

Este modificador permite permite declarar una función como asíncrona. Una función asíncrona
se va a ejecutr en segundo plano. 

Las funciones asíncronas tienen las siguientes características:

- Cuando son invocadas, devuelven un objeto Promise. 
- Cuando la función devuelva un valor en el futuro, el objeto Promise se resolverá al valor devuelto.
- Si la función genera una excepción, el Promise se rechazará con el valor generado.
- Dentro de una función async se puede utilizar la palabra reservada await. Esto pausa la ejecución de la función asíncrona y espera a que el el objeto Promise pasado se resuelva.
Reanuda la ejecución de la función async cuando el Promise se resuelve y retorna el valor resuelto.


```javascript
// Este código ejecutaría esta función en segundo plano
(async () => {

});

// Esto define una función asíncrona
async function mifuncion() {
  
}

// Ejecuta la función en segundo plano. 
mifuncion();
```


# El operador await
Se utiliza *dentro de una función async* para esperar a una Promise. Este operador:

- Pausa la ejecución de la función async donde se ha utilizado para esperar a que finalice la ejecución de la promise.  
- El valor de la expresión await será el que devuelva la promesa que ha terminado. 
- Si el valor de la expresión no es una Promesa, se convertirá a una promesa resuelta.


```javascript
// Esta función retorna una promesa que se resuelve cuando pasan 2 segundos
function resolveAfter2Seconds(x) {
  return new Promise(resolve => {
    
    // Se va a llamar a la función pasada como argumento pasados 2 segundos.
    setTimeout(() => {
      
      // Como timeout se llama a la funcion resolve de la promesa. 
      resolve(x);
    }, 2000);
  });
}

// Define una función asíncrona
async function f1() {

  // Espera a que finalice la función
  let x = await resolveAfter2Seconds(10);
  
  // Muestra un mensaje en el log
  console.log(x); // 10
}

// Muestra un mensaje en el log pasados 2 segundos
f1();
```

# Ejercicios

1. Cuenta atrás con promesas
