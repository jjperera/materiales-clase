# JSON
JSON es un formato ligero de intercambio de datos. Es fácil de leer y escribir para las personas y para las máquinas. En JavaScript disponemos de los siguientes métodos para serializar/deserializar objetos en formato JSON:

- https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON
- https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON




