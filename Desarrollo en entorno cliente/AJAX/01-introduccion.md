
# Mecanismos de comunicación asíncrona
Vamos a estudiar a lo largo del tema los siguientes conceptos

- JSON (JavaScript Object Notation): es una notación que permite describir objetos JavaScript en una cadena de texto.
- Promeses: Objetos utilizados para llevar a cabo operaciones asíncronas.
- AJAX (Asinchronous JavaScript and XML): Permite crear páginas dinámicas donde no es necesario refrescar todo el contenido de la página. Se intercambian datos en diferentes formatos como JSON, XML y otros formatos. Se pueden utilizar fiferentes APIs.
- Web workers: son procesos en JavaScript que se ejecutan en segundo plano.
- AJAX y jQuery: Utilizar jQuery permite también realizar este tipo de peticiones utilizando esta biblioteca.

