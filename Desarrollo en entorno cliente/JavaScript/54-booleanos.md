# Booleanos
Un booleano representa verdadero o falso.

# Valores que se evalúan como falso
Todo valor en JavaScript puede ser convertido a booleano. Los siguientes valores se convierten siempre a false:

- undefined
- null
- 0
- -0
- NaN
- ""

El resto de valores, incluyendo objetos y arrays se convierten en true.

Así por ejemplo las siguientes comparaciones serían equivalentes

```javascript

    if(object !== null) 
    if(object) 
```