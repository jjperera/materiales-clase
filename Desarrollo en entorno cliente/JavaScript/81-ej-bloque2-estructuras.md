# Bloque de ejercicios 2
Bloque de ejercicios básicos para trabajar con las estructuras que hemos visto en JavaScript. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/nombreejer. 

No incluyas el nombre descriptivo.

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
```

## js0202. Sumar elementos
Crea un programa que:

- Pida por consola números al usuario y los inserte en un array en el mismo orden hasta que el usuario introduzca una cadena vacía.
- Cuando termine la introducción debe.
  - Mostrar la longitud del array
  - Mostrar los elementos en el orden que se han introducido.
  - Mostrar los elementos en orden inverso.
  - Calcular la suma de los elementos del array.


## js0203. Ordenar palabras
Crea un programa que pida 3 palabras al usuario por consola, las ordene alfabéticamente y las muestre en orden alfabético y alfabético inverso.

## js0204. Adivina adivinanza.
Crea un programa que:

- Genere 10 númneros aleatorios de 0 a 20. (Utiliza Math)
- Pida al usuario 5 números.
- Al final, compruebe el número de aciertos. Mostrando al usuario:
  - Número de aciertos
  - Los números que ha acertado

## js0205. Lista de la compra.
Crea un programa que pida al usuario los nombres de elementos a añadir a la lista de la compra. El programa los va a ir añadiendo hasta recibir una cadena vacía. Debe además:

- En caso de que el elemento suministrado exista, Deberá indicarlo. Sino, mostrará un mensaje indicando que se ha añadido.
- Cuando se finalice la introducción (cadena vacía). Mostrará los artículos en la lista en orden alfabético.

## js0206. Filtra letra
Crea un programa que pida una cadena de texto al usuario. Las letras en la cadena se considerarán letras prohibidas. Ahora pide palabras al usuario hasta que introduzca una cadena vacía. Para cada palabra, si la palabra contiene alguna de las letras de la palabra original, mostrará un error. En caso contrario indicará que la palabra es válida.

Al final, mostrará un resumen con todas las palabras introducidas indicando si son válidas o no.

## js0207. Football Manager
Vamos a crear un programa que permita gestionar las alineaciones de un equipo de fútbol. Para ello, vamos a tener dos partes:

- Primera. Donde se va a introducir la configuración del equipo. Se van a pedir los pares número, nombre de los jugadores del equipo hasta introducir una cadena vacía.
- Segunda. Donde se va a poder consultar. Se va a pedir que se introduzca el número y se va a mostrar el jugador que lo tiene. Esto se va a hacer hasta que se introduzca un cero.


