# Bloque de ejercicios 1
Bloque de ejercicios básicos para trabajar con las estructuras que hemos visto en JavaScript. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/nombreejer. 

No incluyas el nombre descriptivo.

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
```

## js0101. Contar 
Haz un programa que lea un número mayor que cero por pantalla. Si el número es incorrecto, deberá mostrar el error. En caso contrario, imprimirá todos los números de 0 hasta el número indicado.

## js0102. Factorial for
Pide un número y calcula el factorial de forma iterativa. Utilizando for.

## js0103. Factorial while
Pide un número y calcula el factorial de forma iterativa. Utilizando while.

## js0104. Adivina el número
Crea el programa adivina el número que:

- Genere un número aleatorio entre 1 y 100. (Utiliza el objeto Math para hacerlo)
- Permita al usuario introducir números de modo que:
  - Si el número es menor mostrará un mensaje que lo indique
  - Si el número es mayor mostrará un mensaje que lo indique
  - Si es igual el usuario gana la partida
- El usuario tendrá un número máximo de intentos que tendrá que ser definido en una constante.
 
## js0105. Tipos de motor. if/else
Pide el tipo de motor al usuario (indicando que los valores posibles son 1, 2, 3, 4) y:

a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.

b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.

c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.

d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.

e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.

f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.

## js0106. Tipos de motor. switch
Resuelve el ejercicio anterior utilizando la sentencia switch.

## js0107. do/while
Crea un programa que pida números al usuario hasta que el número introducido sea 0.

## js0108. Calculadora
Crea un programa que implemente una calculadora que cumpla lo siguiente: 

- Debe tener una pantalla que almacena siempre el valor actual. Inicialmente contiene un 0.
   - La pantalla se muestra siempre al inicio y después de cada operación
   - Todas las operaciones admiten dos operandos. El primero es la pantalla y el otro se pide al usuario. 
- Tendrá una memoria que permitirá almacenar un operando que se inicializará a 0.
- Debe soportar las operaciones: - +,-,*,/. 
- La operación C pone la pantalla a cero.
- La operación M guarda el valor en la pantalla en memoria.
- La operación R pone el valor en memoria en pantalla.
- Si cuando se está leyendo un operando se introduce R el operando será el valor en memoria.
- La operación q permite cerrar la calculadora. 
- La calculadora debe mostrar un error cuando se realicen operaciones no válidas.



