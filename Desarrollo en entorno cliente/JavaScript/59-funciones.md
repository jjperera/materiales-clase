

# Definición
Vías para definir una función

- La palabra reservada function: Es la forma más sencilla de definir una función.
- Expresión de función: Podemos definir una función en línea como si fuera una expresión.
- ES6+ arrow functions: Tienen una sintaxis más compacta. Son particularmente útiles cuando se pasa una función como argumento.

---
## function
Una declaración de consiste en la palabra **function** seguida de:

- Identificador de la función
- Una lista de 0 o más identificadores entre paréntesis. Estos van a ser los parámetros de la función.
- Las llaves {} que delimitan el cuerpo de la función.

A tener en cuenta
- **Una definición de una función puede considerarse una variable cuyo valor es el cuerpo de la función**
- **Si una función no retorna nada su resultado será undefined**
- **Las funciones pueden ser invocadas por código que aparece antes que la declaración de la función.

Las funciones pueden ser declaradas en:
- Nivel superior: son visibles a todo nuestro programa.
- Dentro de bloques: Existen únicamente dentro del bloque

Ejemplos

```javascript
// Suma dos números y retorna el resultado
function suma(a, b) {   // En los parámetros no es necesario indicar el tipo.

    return a+b;         // Importante notar que aunque no declaramos
                        // tipo de retorno, podemos retornar.                         
}

// Función suma números recursiva. Suma todos los números de cero hasta el parámetros
function sumaNumeros(a) {
    if(a == 0) {
        return 0;
    } else {
        return a + sumaNumeros(a-1);
    }
}
```
---
## Expresión de función (Function expressions) 
Son declaraciones de función que aparecen en el contexto de una **expresión o sentencia y el nombre es opcional**. 

A continuación tenemos algunos ejemplos

```javascript
// Asignamos una función a la variable. 
const cuadrado = function(x) { return x*x }

// Si queremos aplicar recursión, necesitamos el nombre. En otro caso es opcional
const funcion = function sumaNumeros(a) { return a == 0?0:sumaNumeros(a-1); }

// Podemos pasar así una función como un argumento
[3,1,1].sort(function(a,b) { return a-b; });

// Una expresión de función puede ser declarada y ejecutada inmediatamente 
// utilizando esta sintaxis
let cuadrado = ( function(x) { return x*x; }(2) );
```
Algunas cosas a tener en cuenta:
- El const no es obligatorio, pero es una buena práctica.
- **Una función declarada como expresión queda declarada en el momento que la expresión se ejecuta.**

---
## Funciones flecha (Arrow functions)
Estas funciones se caracterizan por utilizar una flecha (=>) para separar los parámetros del cuerpo de la función. Además, debemos tener en cuenta que :

- No se utiliza la palabra function.
- Las funciones flecha se comportan como expresiones por lo que no necesitan un nombre. 
- Deberían retornar siempre un valor.

Podríamos considerarlas parecidas a las lambda expressions de Java.

Una función flecha, en su forma general estará compuesta por:
- Parámetros
- =>
- Cuerpo
En su forma compacta:
- si la única sentencia que la forma es un return, se pueden omitir
tanto el return como los {}.
- Si solo recibe un parámetro, se pueden omitir los paréntesis
- Si no recibe parámetros, se deben incluir los paréntesis.

Algunos ejemplos

```javascript
// Función que retorna la suma de a y b
const suma = (a,b) => { return a+b; }

// Equivalente a la función anterior pero en forma compacta
const suma = (a,b) => a+b; 

// Función que calcula el valor de una función matemática f(x)
const f = x => 4*x**3 + x*x + 2x + 1;

// Una función que no recube parámetros. Podría operar con variables globales por ejemplo
const sinparametros = () => 10;

// Ejemplo de uso por ejemplo para una ordenación
[3,1,1].sort((a,b) => a-b);

```
A tener en cuenta:
- Nunca partas una función flecha entre los parámetros y =>. **La => debe ir siempre en la
misma línea que los parámetros.**
- Si el resultado es un literal de objeto se debe encerrar el return entre llaves para evitar ambiguedades. const f = x => { return { valor: x}; }


---
## Funciones anidadas
En JavaScript las funciones pueden definirse anidadas dentro de otras funciones. Por ejemplo:

```javascript
function hypotenuse(a, b) { 
   
   function square( x) { return x* x; } 
   
   return Math.sqrt( square( a) + square( b)); 
}
```

# Invocación
Una función puede ser invocada:

1. Como una función
2. A través de los métodos call() y apply() de una función.
3. Como un constructor
4. Como un método
5. Invocación implícita

En este tema, vamos a centrarnos en las invocaciones 1. Por ejemplo.

```javascript
// Podemos invocarlas como en cualquier otro lenguaje. Si no retorna nada, el resultado
// será undefined
const hp = hypotenuse(10, 5);
```
---
# Argumentos opcionales
Podemos permitir que los argumentos (de derecha a izquierda) sean opcionales. Por ejemplo:

```javascript
function suma(a, b) {
    if(b == undefined) { // Si el argumento no se pasa como argumento recibo undefined
        return a;
    } else {
        return a+b;
    }
}

const r1 = suma(1,2);   // r1 = 3
const r2 = suma(1);     // r2 = 1
```

---
# Valores por defecto
Cuando definimos una función, podemos indicar los valores por defecto de los parámetros

```javascript
function suma(a=0, b=0) {
    return a+b;
}

const r1 = suma(1,2);   // r1 = 3
const r2 = suma(1);     // r2 = 1
const r3 = suma();      // r3 = 0
```

---
# Número de argumentos variable
Utilizando ... de forma similar a como se hace en Java, podemos tener funciones que reciben
un número indeterminado de parámetros.

```javascript
function suma(... args) {
    let r = 0;
    for(let n of args) {
        r += n;
    }

    return r;
}

const r1 = suma(1,2);   // r1 = 3
const r2 = suma(1);     // r2 = 1
const r3 = suma();      // r3 = 0
```

Un ejemplo que combina valores por defecto y ... para crear una función para calcular el máximo.

```javascript
function max(arg1=-Infinity, ... numeros) {
    let maximo = arg1;
    for(let n of numeros) {
        maximo = n > maximo?n:maximo;
    }

    return maximo;
}
```

Antes de la versión de ECMAScript6 las funciones con número variable de argumentos podían utilizar el objeto arguments. Este permitía acceder a los argumentos en forma de array.

---
# Sintaxis Spread
Este tipo de sintaxis permite que un array pueda ser pasado a una función que espera parámetros individuales. Por ejemplo, para utilizar la función max definida anteriormente, si lo que tenemos es un array de enteros, esto es lo que podemos hacer.

```javascript
const numeros = [1, 4, 5, 8];

const r = max(...numeros); // El resultado debería ser 8
```

---
# Funciones como valores
En JavaScript, las funciones se pueden tratar como valores, de modo que pueden ser asignadas a variables y luego ser invocadas. Aquí tenemos algunos ejemplos

```javascript
const maximo = max; // Max es la función que hemos declarado en otros ejemplos

const r = max(1,2,3,4,5); // El resultado debería ser 5
```

---
# Definiendo propiedades de una función
Una función es un objeto, de modo que podemos definir propiedades. 

```javascript

max.contadorNumeros = 0;    // Puede ser declarado aquí.
                            // Esta propiedad cuenta el número de números que se han contado
                            // desde inicio dle programa.
function max(arg1=-Infinity, ... numeros) {
    let maximo = arg1;
    for(let n of numeros) {
        maximo = n > maximo?n:maximo;
        max.contadorNumeros++;
    }

    return maximo;
}
```
