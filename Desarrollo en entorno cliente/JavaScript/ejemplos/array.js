"use strict";

let a = new Array();

a[0] = 10;
a[2] = 20;

for(let v of a) {
    console.log(v);
}

let palabras = [ 'hola', 'caracola', 'bola'];

palabras = palabras.sort();

for(let e of palabras) {
    console.log(e);
}


palabras = palabras.reverse();

for(let e of palabras) {
    console.log(e);
}
