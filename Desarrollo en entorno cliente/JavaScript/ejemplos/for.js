"use strict";

for(let n = 0;n < 10;n++) {
    console.log(n);
}

for(let n = 0, i = 9;n < 10;n++, i--) {
    console.log(n);
    console.log(i);
}

// Define un array y lo inicializa
let data = [ 1, 2, 3, 4, 5, 6, 7, 8, 9];

// Variable para guardar la suma
let suma = 0;

// Para cada elemento de data una iteración
for(let elemento of data) {
    suma += elemento;
}

console.log(suma);

for(let ch of "cadena") {
    console.log(ch);
}


for(let propiedad in globalThis) {
    console.log(propiedad);
}
