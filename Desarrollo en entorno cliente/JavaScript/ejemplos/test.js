function suma(... args) {
    let r = 0;
    for(let n of args) {
        r += n;
    }

    return r;
}

function max(arg1=-Infinity, ... numeros) {
    let maximo = arg1;
    for(let n of numeros) {
        maximo = n > maximo?n:maximo;
    }

    return maximo;
}


const r1 = suma(1,2);   // r1 = 3
const r2 = suma(1);     // r2 = 1
const r3 = suma();      // r3 = 0

const r4 = suma(1,5,6,7,8,9,2);   // r1 = 3

console.log(r1);
console.log(r2);
console.log(r3);
console.log(r4);

const m = max(1,3,4,1,-5,9);      
console.log(m);


const numeros = [1, 4, 5, 8];

const r = max(...numeros); // El resultado debería ser 8

console.log(r);