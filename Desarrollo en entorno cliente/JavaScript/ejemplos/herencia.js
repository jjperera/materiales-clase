
class Gato {

    constructor(nombre) {
      this.nombre = nombre;
      console.log("Gato.constructor");
    }
  
    hablar() {
      console.log("Gato.hablar");
      console.log(this.nombre + ' hace ruido.');
    }
  }
  
  class Leon extends Gato {

    constructor(nombre, apellido) {
        super(nombre);
        this.apellido = apellido;
        console.log("Leon.constructor");
      }  

    hablar() {
      console.log("Leon.hablar");
      super.hablar();
      console.log(this.nombre + ' maulla.');
    }
  }

console.log("Inicio");
let leon = new Leon("paco");
leon.hablar();
console.log("Fin");