# Introducción
En JavaScript los tipos pueden dividirse en dos categorías:

- Tipos primitivos: que incluyen número, cadenas de texto y booleanos. 
- Objetos: todo valor que no es un número una cadena, un booleano, un símbolo, *null* o *undefined* es un objeto.

En este bloque nos vamos a centrar principalmente a los tipos primitivos.

# Algunas definiciones y características

## Recolección de basura
Al igual que en Java, aquí tenemos un recolector de basura que se va a encargar de liberar aquellos recursos que ya no son utilizados.

## Funciones y clases como valores
En JavaScript las funciones y clases son valores que pueden ser manipulados por los programas JavaScript.

## Soporte de programación con estilo POO
JavaScript soporte un estilo de programación orientado a objetos. Esto significa que en lugar de tener definidas fucniones para operar con valores de varios tipos, los tipos definen métodos para trabajar con valores. Por ejemplo, para ordenar los elementos de un array a, no pasamos el array a la función sort, sino que invocamos al método sort del array.

```javascript
    a.sort();
```

## Conversión de tipo
JavaScript convierte valores de un tipo a otro. Si tu programa espera una cadena y recibe un número, el número será convertido en una cadena de forma automática. Más adelante veremos las reglas de conversión.


# Números
El tipo primario de JavaScript para representar números es Number. Este es utilizado para:

- Representar enteros en el rango -9007199254740992 a 9007199254740992. Si se necesita representar enteros más allá de este rango se puede perder precisión. Se debe tener en cuenta de todos modos que ciertas operaciones operan con enteros de 32 bits como veremos más adelante.
- Aproximar reales

Number utiliza número en coma flotante de doble precision (64 bits) definidos por el IEEE 754 para representar ambos tipos de números. Cuando un número aparece en un programa, estamos hablando de un literal numérico. JavaScript permite utilizar diferentes formatos como veremos a continuación.

## Literales enteros
Literales en base 10

```javascript
3
100
10000
```

Además de base 10, se pueden utilizar bases 2, 8 o 16 para representar enteros:

```javascript
0x13                                    // Número hexadecimal
0xCACAFEA                               // Número hexadecimal
0b10111110101111101100101011111110      // Número en binario
0o57405740                              // Número en octal
```
Para facilitar la lectura de los literales enteros se permite utilizar '_' para separar grupos de números. Aunque no está en versión definitiva (versión 2020) esta funcionllidad ya está implementada en los navegadores principales y Node.

```javascript

1_000_000                   // Separdor de miles
0xCA_FE                     // Separador de bytes
0b0000_1111_0000_1111       // Separador de nibble (4 bits)

```

## Literales reales
Se puede utilizar el '.' o notación exponencial
```javascript
3.14             // 3.14
1.0              // 1.0
.10              // 0.10
6.5e13           // 6.5 x 10^13
10.123_123_555   // Se puede utilizar _ como separador de miles en la parte decimal
```

## Enteros largos con BigInt
Definido en ES2020, el tipo numérico BigInt permite representar números enteros de cualquier longitud. Para identificar un literal BigInt, terminaremos el número en el carácter 'n'. Por defecto se considera que están escritos en base 10, pero se pueden especificar también en otras bases como los números enteros normales.

```javascript
    1234n           // Decimal
    0b1010101010n   // Binario
    0o12475n        // Octal
    0x10n           // Hexadecimal
```

Existe la función *BigInt(Expr)* para convertir una expresión compatible en un valor BigInt. Hemos de tener en cuenta también que **no podemos mezclar Number y BigInt en operaciones aritméticas**. Esto no es aplicable a las comparaciones, que sí funcionan correctamente.

## Trabajando con enteros

Operadores
Math.

# Cadenas de texto
Em JavaScript, para representar texto se utilizan cadenas. Una cadena es una secuencia de valores de 16 bits donde cada uno de esto valores representa un carácter Unicode. La longitud de una cadena es el número de caracteres que contiene. El primer carácter está en la posición 0. En JavaScript no tenemos un tipo especial para almacenar un carácter. Un carácter puede ser representado como una cadena de longitud 1.

Internamente JavaScript utiliza UTF-16. Si necesita representar caracteres que necesitan más de 16 bits para ser representados se siguen las regls de UTF-16 pero nos podemos encontrar con cadenas que arrojan una longitud mayor que el número real de caracteres debido a esto.

## Literales de tipo cadena
Para delimitar cadenas se puede emplear '," o `. Tenemos que tener en cuenta que:

- Dentro de '' se pueden utilizar " y `.
- Dentro de "" se puede utilizar ` y '.
- Dentro de `` se puede utilizar " y '.

Algunos ejemplos

```javascript
""                  // Cadena vacía
'Hola Mundo!!' 
"Hola Mundo"
'saludo = "hola mundo";' 
```

Las cadenas se pueden concatenar con '+' o con el terminador \

```javascript
"Esto es una cadena\
que se estoemde a dos líneas"
```

## Secuencias de escape
El carácter \ permite incluir en nuestras cadenas caracteres que de otra forma no se pueden representar. 

|Secuencia|Carácter representado|
|---------|---------------------|
| \0      | El carácter NUL     |
| \b      | Backspace           |
| \t      | Tabulación          |
| \n      | Nueva línea         |
| \v      | Tabulación vertical |
| \f      | Salto de línea      |
| \r      | retorno de carro    | 
| \"      | "                   | 
| \'      | '                   | 
| \\      | \                   | 
| \xnn    | xx son dígitos hexadecimales | 
| \unnnn    | nnnn código unicode | 

