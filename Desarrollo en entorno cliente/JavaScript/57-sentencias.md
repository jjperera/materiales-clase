
# Sentencias
Las sentencias van separadas por ';'.
Los bloques de sentencias van entre { }


# Condicionales

## if

```javascript
    if(expresion) {
        sentencia;
        sentencia;
        sentencia;
    }
```

## else if

```javascript
    if(expresion) {

        sentencia;
        sentencia;
        sentencia;
    } else if(expresion) {

        sentencia;
        sentencia;
        sentencia;
    } else if(expresion) {

        sentencia;
        sentencia;
        sentencia;
    } else {

        // Si no se cumple ninguna de las condiciones se ejecuta este bloque
        sentencia;
        sentencia;
        sentencia;        
    }
```

## switch

```javascript
    switch(expresión) {
        case expresión: 
                sentencia;
                sentencia;
                sentencia;
                break;
        case expresión:
                sentencia;
                sentencia;
                sentencia;
                break;
        default:
                sentencia;
                sentencia;
                sentencia;
                break;
    }
```

# Bucles

## while
```javascript
    while(expresión) {
        sentencia;
        sentencia;
        sentencia;
    }
```

## do/while

```javascript
    do {
    
        sentencia;
        sentencia;
        sentencia;

    } while(expresión);
```

# for

```javascript
    for(inicialización; prueba; incremento) {
        sentencia;
        sentencia;
        sentencia;
    }
```

Tenemos que tener en cuenta que inicialización, prueba e incremento son tres expresiones. El equivalente en el bucle while sería:

```javascript
    inicialización;
    while(prueba) {
        
        sentencia;
        sentencia;
        sentencia;

        incremento;
    }
```

Notar que se pueden declarar variables directamente en el for. También se puede dejar vacío cualquiera de las tres expresiones del bucle. En el incremento, si se quiere controlar más de un incremento, se deben separar por una ','.

Para mostrar los números de 0 a 9 podemos utoilizar el siguiente código. 

```javascript
    for(let n = 0;n < 10;n++) {
        console.log(n);
    }
```

Otros ejemplos válidos son:

```javascript
    for(let n = 0;n < 10;n++) {
        console.log(n);
    }
```

Los siguientes ejemplos serían válidos

```javascript

for(let n = 0, i = 9;n < 10;n++, i--) {
    console.log(n);
    console.log(i);
}

// Equivalente a while(true) {}
for(;;;) {

}
```

## for/of
Aunque utiliza la palabra reservada for, es un tipo de bucle completamente diferente. Este bucle funciona con objetos iterables. Por ejemplo, un array o una cadena son iterables.  Veamos un ejemplo:

```javascript
// Define un array y lo inicializa
let data = [ 1, 2, 3, 4, 5, 6, 7, 8, 9];

// Variable para guardar la suma
let suma = 0;

// Para cada elemento de data una iteración
for(let n of data) {
    suma += n;
}

console.log(suma);
```

Una cadena es iterable caracter a caracter de modo que se pueden recorrer los caracteres de una cadena del siguiente modo:

```javascript
for(let ch of "cadena") {
    console.log(ch);
}

```

## for/in
Este for se puede utilizar para recorrer todos los nombres de las propiedades de un objeto. Tiene la siguiente sintaxis:

```javascript
    for(variable in object) {
        sentencia;
        sentencia;
        sentencia;
    }
```

Por ejemplo, para mostrar todas las propiedades del objeto global, podríamos utilizar:

```javascript
    for(let propiedad in globalThis) {
        console.log(propiedad);
    }
```

# Saltos
En este apartado solo voy a comentar los que ya conocéis de Java. Tienen el mismo significado con pequeñas diferencias. 

- break;
- continue;
- return expression;
- throw expresion; : En este caso se puede devolver un valor de cualquier tipo. También hay clases en JavaScript que representan errores. Por ejemplo se puede lanzar "new Error("Mensaje");"

# Gestión de excepciones

## try/catch/finally
Funciona de modo similar a java pero con algunas diferencias. A continuación tenemos un ejemplo.


```javascript
try {
    // Puedo lanzar un valor de cualquier tipo. En este caso lanzo directamente una cadena
    throw "hola";
    
} catch(e) {
    // e es como un parámetro de una función. No tengo que declarar el tipo. 

    // Como es una cadena lo que se ha lanzado, lo puedo imprimir por pantalla.
    console.log(e);

} finally {

    // Esto se va a ejecutar si o si después del try y el catch.
    console.log("finally");
}
```

# Otras sentencias

## with
Permite acceder a las propiedades de un objeto como si fueran variables en un contexto determinado. La sintaxis es:

```javascript
    with(objeto) {
        sentencia;
        sentencia;
        sentencia;
    }
```

Permite por ejemplo 

```javascript

// Tengo que utilizar la f para acceder a los atributos
let f = document.forms[0]; 
f.name.value = ""; 
f.address.value = ""; 
f.email.value = "";

// Las propiedades del form son como variables locales
with(document.forms[0]) {
    name.value = ""; 
    address.value = ""; 
    email.value = "";
}
```

## use strict
En realidad es una directiva para el intérprete. Esta directiva puede aparecer úniamente al inicio de un script o al inicio del cuerpo de una función antes de la primera sentencia. Poner esta directiva indica que el script utiliza código estricto. Las funciones definidas dentro de un bloque de cófigo estricto se considera que usan también las mismas restricciones. Lo mismo aplica para el código pasado a la función eval();

Disponéis de más información del modo estricto en la [web de mozilla](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Strict_mode).

Ejemplo:

```javascript
"use strict";

a = 10; // No he declarado a. Debería lanzar un error
```

## Declaraciones
Se pueden hacer con

- const es una constante
- let declara variables. Utilizaremos siempre let.
- var declara variables