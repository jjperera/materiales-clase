# Bloque de ejercicios 3
Bloque de ejercicios básicos para trabajar con las estructuras que hemos visto en JavaScript. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/nombreejer. 

No incluyas el nombre descriptivo.

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
    const prompt = require('prompt-sync')();
```

## js0301. Factorial
Define la función factorial(x) que lo implemente de forma recursiva. 

## js0302. Definición de funciones. Calculadora I. 
En este y los siguientes ejercicios, vamos a implementar una calculadora utilizando diferentes formas de utilizar las funciones. De ese modo, la lógica será la misma y únicamente tendréis que cambiar la forma de trabajar. La calculadora tendrá las siguientes características:

- El menú principal debe ser una función.
- Tendra una pantalla o acumulador. El valor inicial será 0.
- Implementará los siguientes operadores con 2 operandos. (pantalla más uno solicitado)
   - +,-,*,/,%
   - factorial
   - x elevado a y
- Implementará los siguientes operadores con 1 operando. (pantalla)
- El resultado de las operaciones se almacenará en el acumulador o pantalla.
- Tendrá una memoria que permitirá almacenar el contenido de la pantalla. El operador será 
  M
- Tendrá una operación que permitirá cargar el valor en memoria en la pantalla. R
- Tendrá una operación para poner a 0 tanto la pantalla como la memoria.

Será necesario también que exista un programa de prueba que realize al menos una vez cada una de las operaciones.

## js0303. Definición de funciones. Calculadora II. 
El ejercicio será similar al anterior, pero:

- Se deberán utilizar funciones como expresiones. Guardando todas las funciones de la calculadora en variables excepto el menú principal.
- Se utilizará un Map para obtener la función a ejecutar en función de la operación a realizar.

## js0304. Funciones flecha. Calculadora III. 
Este ejercicio se va a hacer exactamente del mismo modo que el anterior, pero utilizando Funciones flecha. 

## js0304. Omitir argumentos. Calculadora IIII. 
Este ejercicio será igual que el ejercicio Calculadora II pero vamos a hacer que todas las funciones permitan omitir todos los argumentos. En caso de omitirlos el valor que tomarán será:

- El primer argumento, el valor de la pantalla.
- El segundo argumento será igual al valor de la pantalla también.

Si al pedir el segundo operando se introduce un '' no se pasará ningún valor como segundo argumento haciendo que se utilice el valor por defecto.

## js0305. Número de argumentos variable.
Crea las siguientes funciones, de modo que permitan un número de argumentos variable:

- sumaNumeros(): suma los números pasados como argumento
- multiplicaNumeros(): multiplica los números pasados como argumento
- minimo(): devuelve el mínimo de entre los valores pasados como argumento
- maximo(): devuelve el maximo de entre los valores pasados como argumento

En el programa principal, haz una prueba pasando los argumentos, y otra pasando un array utilizando la sintaxis spread.

## js0306. Propiedades de una función
Implementa las funciones del ejercicio anterior, pero añade una propiedad a cada función de modo que las llamadas sean acumulativas. Por ejemplo, dos llamadas a sumaNumeros() sumarán los números de las dos llamadas.

## js0307. Contador único
Definiendo una propiedad de una función. Crea una función contadorUnico() que permita obtener un número único diferente con cada invocación. Ejemplo:

1. contadorUnico() = 1
2. contadorUnico() = 2
3. contadorUnico() = 3
4. contadorUnico() = 4

Además, el contador deberá permitir definir utilizando una propiedad el valor máximo y el valor inicial. Pasar el valor máximo hará que en la siguiente llamada se retorne el valor inicial.

