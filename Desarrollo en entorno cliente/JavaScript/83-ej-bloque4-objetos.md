# Bloque de ejercicios 4. Objetos.
Bloque de ejercicios básicos para trabajar con objetos. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/nombreejer. 

No incluyas el nombre descriptivo.

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
    const prompt = require('prompt-sync')();
```

## js0401. Calculadora. Literal de objeto.
Vamos a crear de nuevo nuestra calculadora, pero vamos a utilizar objetos. La aplicación deberá cumplir.

- El menú principal debe ser una función.
- La calculadora estará implementada como un objeto.
- Tendra una pantalla o acumulador. El valor inicial será 0.
- Implementará los siguientes operadores con 2 operandos. (pantalla más uno solicitado)
   - +,-,*,/,%
   - factorial
   - x elevado a y
- Implementará los siguientes operadores con 1 operando. (pantalla)
- El resultado de las operaciones se almacenará en el acumulador o pantalla.
- Tendrá una memoria que permitirá almacenar el contenido de la pantalla. El operador será 
  M
- Tendrá una operación que permitirá cargar el valor en memoria en la pantalla. R
- Tendrá una operación para poner a 0 tanto la pantalla como la memoria.
- **La calculadora no debe interactuar en ningún momento con el usuario.**
- Cada vez que se realiza una operación, la calculadora generará un evento invocando a una función con el resultado.

Será necesario también que exista un programa de prueba que realize al menos una vez cada una de las operaciones.

## js0402. Calculadora. new.
Repite el ejercicio pero usa new.

## js0403. Calculadora. Object.create
Repite el ejercicio utilizando Object.create.

## js0404. Cuenta
Crea una clase llamada Cuenta que tendrá los siguientes atributos: titular y cantidad (puede tener decimales). El titular será obligatorio y la cantidad es opcional. Crea dos constructores que cumpla lo anterior.

Crea sus métodos get, set y toString.

Tendrá dos métodos especiales:

- ingresar: se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada.
- retirar: se retira una cantidad a la cuenta, si restando la cantidad actual a la que nos pasan es negativa, la cantidad de la cuenta pasa a ser 0.


## js0405. Persona
Haz una clase llamada Persona que siga las siguientes condiciones:

Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura. Por defecto, todos los atributos menos el DNI serán valores por defecto según su tipo (0 números, cadena vacía para String, etc.). Sexo sera hombre por defecto, usa una constante para ello.

- Se implantará un constructor que con los siguientes parámetros
  - nombre (obligatorio)
  - DNI (obligatorio)
  - sexo (obligatorio)
  - edad (opcional)
  - peso (opcional)
  - altura (opcional)

- Los métodos que se implementaran son:
  - calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2  en m)), si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0  y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.
  - esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.
  - validaDNI(): Invocado en el constructor. Comprueba si el DNI es correcto. No se pueden asignar DNIs incorrectos.

- Crea ahora una clase menú que permita gestionar personas.

- Crea el programa principal que invoca al menú.

