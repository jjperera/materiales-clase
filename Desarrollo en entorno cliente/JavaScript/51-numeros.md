# Números
El tipo primario de JavaScript para representar números es Number. Este es utilizado para:

- Representar enteros en el rango -9007199254740992 a 9007199254740992. Si se necesita representar enteros más allá de este rango se puede perder precisión. Se debe tener en cuenta de todos modos que ciertas operaciones operan con enteros de 32 bits como veremos más adelante.
- Aproximar reales

Number utiliza número en coma flotante de doble precision (64 bits) definidos por el IEEE 754 para representar ambos tipos de números. Cuando un número aparece en un programa, estamos hablando de un literal numérico. JavaScript permite utilizar diferentes formatos como veremos a continuación.

## Literales enteros
Literales en base 10

```javascript
3
100
10000
```

Además de base 10, se pueden utilizar bases 2, 8 o 16 para representar enteros:

```javascript
0x13                                    // Número hexadecimal
0xCACAFEA                               // Número hexadecimal
0b10111110101111101100101011111110      // Número en binario
0o57405740                              // Número en octal
```
Para facilitar la lectura de los literales enteros se permite utilizar '_' para separar grupos de números. Aunque no está en versión definitiva (versión 2020) esta funcionllidad ya está implementada en los navegadores principales y Node.

```javascript

1_000_000                   // Separdor de miles
0xCA_FE                     // Separador de bytes
0b0000_1111_0000_1111       // Separador de nibble (4 bits)

```

## Literales reales
Se puede utilizar el '.' o notación exponencial
```javascript
3.14             // 3.14
1.0              // 1.0
.10              // 0.10
6.5e13           // 6.5 x 10^13
10.123_123_555   // Se puede utilizar _ como separador de miles en la parte decimal
```

## Enteros largos con BigInt
Definido en ES2020, el tipo numérico BigInt permite representar números enteros de cualquier longitud. Para identificar un literal BigInt, terminaremos el número en el carácter 'n'. Por defecto se considera que están escritos en base 10, pero se pueden especificar también en otras bases como los números enteros normales.

```javascript
    1234n           // Decimal
    0b1010101010n   // Binario
    0o12475n        // Octal
    0x10n           // Hexadecimal
```

Existe la función *BigInt(Expr)* para convertir una expresión compatible en un valor BigInt. Hemos de tener en cuenta también que **no podemos mezclar Number y BigInt en operaciones aritméticas**. Esto no es aplicable a las comparaciones, que sí funcionan correctamente.

# Trabajando con números
Para operar, javascript permite:

- Utilizar los operadores aritméticos. + - * / %
- Utilizar las operaciones incluidas en el objeto Math. Por ejemplo Math.pow(2,2) va a calcular 2^2.

Tenéis las ferencias aquí:

- [Number](https://www.w3schools.com/jsref/jsref_obj_number.asp)
- [Math](https://www.w3schools.com/jsref/jsref_obj_math.asp)