# Introducción
En este apartado vamos a ver las estructuras de datos básicas de JavaScript.

---
# Array
Es un objeto global utilizado para la construcción de arrays. Estos son un objeto de tipo lista de alto nivel. Tenemos información aquí:

- [Documentación de mozilla](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array)
- [Referencia de w3schools](https://www.w3schools.com/jsref/jsref_obj_array.asp)

---
# Set
El objeto Set permite almacenar valores únicos de cualquier tipo sean valores primitivos o referencias a objetos. Tenemos más información aquí:

- [Documentación de mozilla](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Set)
- [Referencia de w3schools](https://www.w3schools.com/js/js_object_sets.asp)

---
# Map
El objeto Map es equivalente al objeto map de Java. Mantiene pares de clave/valor. Tenemos una referencia aquí:

- [Documentación de Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)
- [Referencia de w3schools](https://www.w3schools.com/js/js_object_maps.asp)

---
# Cadenas como arrays
Las cadenas se comportan como arrays de caracteres UTF-16 de solo lectura. De modo que es posible acceder a los caracteres individuale de una cadena de los siguientes modos:

```javascript

let cadena = "hola mundo";

let h = s.charAt(0);    // Obtengo la h utilizando el método charAt

let o = s[1]            // Obtengo la o utilizando el acceso con [] como si fuera un array

```

---
De cualquier modo, un objeto de tipo String no es un objeto de tipo Array de modo que el operador typeof aplicado a una cadena retornará string para cadenas. El método Array.isArray() va a retornar false si se le pasa una cadena.

Es decir, la ventaja que obtenemos es que podemos utilizar [] en lugar de charAt teniendo a nuestra disposición una sintaxis más compacta.

También tenemos que tener en cuenta que las cadenas son **valores de solo lectura** de modo que no pueden ser modificadas. 


