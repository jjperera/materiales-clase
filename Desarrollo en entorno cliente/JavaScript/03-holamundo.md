# Introducción
Como no podía ser de otro modo, vamos a crear nuestro primer programa en JavaScript como lo haríamos al más puro estilo de otros lenguajes de programación. 

# Hola Mundo!
Crea el archivo holamundo.js desde VisualStudio Code y escribe el siguiente fragmento de código.

```javascript
    console.log("Hola mundo!");
```

Para ejecutar el ejemplo, abre "Ejecutar/Ejecutar sin depuración" (CTRL+F5). Se abrirá un desplegable. Ahí debes seleccionar Node.js. Se mostrará el mensaje en la consola de depuración.




