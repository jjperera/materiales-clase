# Conversión
Tenemos dos tipos de conversión

## Implicita
La lleva a cabo JavaScript de forma automática. Dependiendo del objeto que necesitemos y el que suministremos tendremos un objeto resultante. 

## Explícita
Puede ser que queramos hacer una conversión de tipo explícita para tener un código más claro o tener un coltrol preciso de las operaciones que se pueden realizar. Para hacer una conversión explícita de tipos, JavaScriot permite usar las funciones Number(), String() y Boolean(). Estas reciben un valor y devuelven un valor del tipo que representan.

Ejemplo de conversión explícita

```javascript

    let cadena = "5";   // Es una cadena de texto
    let numero = new Number(5); // Ahora es un número

```

## Comparación estricta
Se puede utilizar el operador === para comparar valor y el tipo.

