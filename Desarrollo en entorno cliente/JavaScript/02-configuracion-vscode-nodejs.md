# Introducción
Para ejecutar nuestros programas escritos en JavaScript, vamos a necesitar un entorno de ejecución. Si bien más adelante vamos a utilizar el navegador, con todo lo que ello implica, de momento vamos a centrarnos en los conceptos básicos del lenguaje. Para ello, vamos a recurror a node.js que es un entorno de ejecución construido con el moder JavaScript V8 de chrome.

# Intalar node.js
El primer paso será instalar node.js. Descargaremos la versión 16 desde el siguiente enlace.

- [https://nodejs.org/es/](https://nodejs.org/es/)

Ejecutaremos el instalador. Para nuestra instalación no va a ser necesario cambiar ninguna de las opciones (todo siguiente). Tras la instalación nos interesan los iconos

- Node.js command prompt : que ejecuta un interfaz de comandos configurado.
- Node.js : intérprete de JavaScript

Dichos iconos apuntan a los programas que vamos a utilizar en las sesiones siguientes.

# Instalación de paquetes para entrada/salida por consola
Vamos a instalar en node un paquete que vamos a utilizar posteriormente en nuestras aplicaciones para leer datos de forma síncrona en consola. El objetivo es que tengamos algo que se utiliza de forma similar a la clase Scanner de Java. Para ello seguiremos los siguientes pasos:

1. Abre *Node.js command prompt*
2. Ejecuta el comando **npm install prompt-sync**. Esto debería crear un directorio con el paquete en $HOME/node_modules.

Si no aparecen errores ya tendremos el paquete instalado. Lo probaremos luego. 

Un ejemplo de uso sería el siguiente
```javascript
    /** Carga el módulo. Función de node.js para cargar módulos  */
    const prompt = require('prompt-sync')();

    /** Lee un nombre desde la consola de forma síncrona */
    const nombre = prompt('¿Cómo te llamas?');
```

# Comprobar integración con VS Code
En este punto, si hemos seguido los pasos correctamente, ya podríamos ejecutar programas en JavaScript utilizando Node.js y Visual Studio Code. Para ello, vamos a crear el siguiente programa de prueba:

```javascript
    console.log('Hola mundo');
```

Ahora para ejecutar el programa, abriremos el terminal integrado de VStudio Code y ejecutaremos

```cmd
    node holamundo.js
```

Si pusiéramos un punto de ruptura, se dentedrá el programa.



