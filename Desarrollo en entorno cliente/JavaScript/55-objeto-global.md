# El objeto global
El objeto global es un objeto que contiene las propiedades que van a ser identificadores globales disponibles para que nuestros programas los utilicen. Este objeto será inicializado cuando se inicia el intérprete de JavaScript. Las propiedades que incluye son:

- Contantes globales como undefined, Infinity y NaN
- Funciones globales como isNaN(), parseInt() y eval()
- Funciones constructoras como Date(), RexExp(), String(), Object(), Array()
- Objetos globales como Math o JSON

Para acceder a este objeto, desde la versión 2020 de ECMAScript se puede utilizar la referencia **globalThis**.

