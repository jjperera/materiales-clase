# Clases 
Las clases son introducidas en **ECMAScript 2015**. Son una **mejora sintáctica** sobre la utilización de herencia basada en prototipos. Esta nueva sintaxis no introduce un nuevo modelo de objetos. Es más bien otra forma de expresar lo mismo con una sintaxis más clara y cerca de aquello a lo que estamos acostumbrados en otros lenguajes.


# Definición de una clase
Se definen de forma similar a Java, pero debemos tener en cuenta que las propiedades son asignadas en el constructor. 

```javascript
/** Declara una clase Rectángulo */
class Rectangulo {

  /** El constructor de la clase. Define las propiedades alto y ancho */  
  constructor (alto, ancho) {
    this.alto = alto;
    this.ancho = ancho;
  }
  
  /** Declara un getter */
  get area() {
     return this.calcArea();
   }
  
  /** Método */
  calcArea () {
    return this.alto * this.ancho;
  }
}

/** Crea una instancia de la clase cuadrado */
const cuadrado = new Rectangulo(10, 10);

/** Retorna la propiedad area */
console.log(cuadrado.area); // 100
```

---
# Métodos estáticos
Al igual que en Java, es posible declarar métodos estáticos. Estos métodos no tienen acceso a ninguna instancia.

```javascript

class Punto {
  constructor ( x , y ){
    this.x = x;
    this.y = y;
  }

  /** Método estático que permite operar con dos puntos */
  static distancia ( a , b) {
    const dx = a.x - b.x;
    const dy = a.y - b.y;

    return Math.sqrt ( dx * dx + dy * dy );
  }
}

/** Crea dos puntos */
const p1 = new Punto(5, 5);
const p2 = new Punto(10, 10);

/** Muestra la distancia */
console.log (Punto.distancia(p1, p2)); // 7.0710678118654755
```

---
# Herencia
Podemos implementar la herencia con la palabra extends del mismo modo que hacemos en Java. A continuación tenemos un ejemplo.

```javascript

/** Esta clase representa un animal*/
class Animal {
  constructor(nombre) {
    this.nombre = nombre;
  }

  hablar() {
    console.log(this.nombre + ' hace un ruido.');
  }
}

/** Hereda de Animal. En este caso, redefine el método hablar */
class Perro extends Animal {
  hablar() {
    console.log(this.nombre + ' ladra.');
  }
}
```


---
# La palabra reservada super
En este ejemplo vamos a ver como se puede utilizar la palabra reservada super para llamar a funciones del objeto padre. Esto es especialmente interesante cuando redefinimos en las subclases dichas funciones.

```javascript
/**
 * Un clase Gato
 */
class Gato {
    
    /** 
     * Constructor de la clase. Recibe únicamente el parámetros nombre 
     */
    constructor(nombre) {
      this.nombre = nombre;
    }
  
    /**
     * Método que muestra un mensaje de texto
     */ 
    hablar() {
      console.log(this.nombre + ' hace ruido.');
    }
  }
  
  /**
   * Esta clase representa un leon que extiende a la clase Gato.
   */
  class Leon extends Gato {

    /**
     * Definimos nuestro propio constructor para la clase Leon (Esto no se requiere)
     */
    constructor(nombre, apellido) {
     
        // Llamamos al constructor de Gato con los parámetros adecuados.
        super(nombre);

        // Tratamos el resto de argumentos.
        this.apellido = apellido;
    }  

    /** Redefinimos el método hablar */
    hablar() {

      // Ejecutamos el método hablar de la clase Gato
      super.hablar();

      // Mostramos un mensaje
      console.log(this.nombre + ' maulla.');
    }
  }

// Crea un objeto de tipo Leon
let leon = new Leon("paco");

// Lo pone a hablar
leon.hablar();
```

---
# Referencias

- https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Classes
