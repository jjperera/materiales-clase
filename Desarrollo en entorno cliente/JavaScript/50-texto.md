# Introducción
El tipo para representar texto en JavaScript es string. Una cadena es una **secuencia ordenada immutable  de valores de 16 bits**. Tiene las siguientes características:

- La **longitud** de una cadena es el número de valores de 16 bits que contiene. 
- El **primer caracter** en las cadenas es el caracter en la posición **0**.
- La **cadena vacía** es una cadena con longitud 0.
- No existe un tipo para representar un caracter.

## Literales
Para delimitar cadenas se puede emplear '," o `. Tenemos que tener en cuenta que:

- Dentro de '' se pueden utilizar " y `.
- Dentro de "" se puede utilizar ` y '.
- Dentro de `` se puede utilizar " y '.

Algunos ejemplos

```javascript
""                  // Cadena vacía
'Hola Mundo!!' 
"Hola Mundo"
'saludo = "hola mundo";' 
```

Las cadenas se pueden concatenar con '+' o con el terminador \

```javascript
"Esto es una cadena\
que se estoemde a dos líneas"
```

## Secuencias de escape
El carácter \ permite incluir en nuestras cadenas caracteres que de otra forma no se pueden representar. 

|Secuencia|Carácter representado|
|---------|---------------------|
| \0      | El carácter NUL     |
| \b      | Backspace           |
| \t      | Tabulación          |
| \n      | Nueva línea         |
| \v      | Tabulación vertical |
| \f      | Salto de línea      |
| \r      | retorno de carro    | 
| \"      | "                   | 
| \'      | '                   | 
| \\      | \                   | 
| \xnn    | xx son dígitos hexadecimales | 
| \unnnn    | nnnn código unicode | 

# Trabajando con cadenas

- Se pueden concatenar con el operador \+
- Las cadenas pueden compararse con los operadores ===, !==, <, <=, >, >=. La comparación se lleva a cabo comparando los valores de 16 bits que la forman.
- La longitud de una cadena puede obtenerse invocando al length de la cadena. 
- JavaScript provee métodos para trabajar con cadenas. Estos métodos están disponibles en el objeto cadena.

Tenéis la [referencia completa aquí](https://www.w3schools.com/jsref/jsref_obj_string.asp).

