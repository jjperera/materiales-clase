# Introducción
En este apartado vamos a cubrir

- Estructura del archivo
- Comentarios
- Literales
- Identificadores y palabras reservadas
- Unicode
- ';' opcionales


# Estructura del archivo
Cuando escribimimos un programa en JavaScript tenemos que tener en cuenta los siguientes puntos:

 - El lenguaje es *case-sensitive*, es decir, distingue entre mayúsculas y minúsculas. 
 - JavaScript ignora los espacios entre tokens en los programas y en su mayor parte, ignora también los saltos de línea. Esto permite indentar los programas de la forma adecuada para facilitar su lectura.
 - Además del caracter espacio (*\u0020*) JavaScript trata los tabuladores, caracteres de control ASCII y algunos caracteres definidos como espacio en Unicode como un espacio. Del mismo modo reconoce como salgos de línea las secuencias (*\r*), (*\n*) o (*\r\n*).


# Comentarios

```javascript
// Esto es un comentario de una línea
/* Esto es un comentario de una línea */

/*
  Esto es un comentario
  multilinea
*/

/**
 * Esto es un comentario multilínea
 * Que luce más molón gracias al * al comienzo de cada línea.
 * 
 * Es totalmente opcional, pero se ve mejor.
 */

```

# Literales
Un literal es un valor que aparece directamente en un programa. A continuación tenemos todos los tipos de literales que podemos representar en JavaScript.

```javascript
23              // El número 12
10.2            // 10 coma 2
"Hola mundo"    // Una cadena de texto
'Adios'         // Otra cadena
true            // Un valor booleano
false           // El otro valor booleano
null            // Puntero nulo. Ausencia de referencia a un objeto.
```

# Identificadores
Un identificador es un **nombre**. Pueden ser utilizados para nombrar constantes, variables, propiedades, funciones y clases. Tienen que cumplir las siguientes características:

- Empezar con una letra, '_' o '$'
- Los caracteres a continuación pueden ser letras, numeros, '_' o '$'.

Como se puede ver, un identificador no puede empezar nunca por un número. Además, las palabras reservadas no pueden ser utilizadas como identificadores.

Ejemplos de identificadores válidos serían:

```javascript
n
i
_n
_i
mi_variable
$mi_variable
$valor
```

# Palabras reservadas
En la siguiente tabla tenemos las [palabras reservadas](https://www.w3schools.com/js/js_reserved.asp) del lenguaje JavaScript. Dichas palabras no pueden ser utilizadas como identificadores.

# Unicode
Los programas JavaScriot se escriben usando **Unicode** pero por asuntos relacionados con la portabilidad y facilitar la edición en determinados contextos, es bastante común utilizar únicamente las letras ASCII, dígitos e idiogramas en nuestros identificadores. Por ejemplo:

```javascript
const π = 3.14;
const sí = true;
```

## Secuencias de escape
JavaScript nos permite escribir caracteres Unicode empleando únicamente caracteres ASCII. Esto se puede hacer utilizando secuencias de escape. Por ejemplo:

```javascript
    let café = 1;   // Define una variable utilizando Unicode
    caf\u00e9       // La referencia usando una secuencia de escape (Siempre 4 dígitos)
    caf\u{E9}       // Otra forma de escribir la misma secuencia de escape
```

## Uso de caracteres no ASCII
Debemos tener en cuenta que en unicode pueden existir diferentes formas de representar el mismo caracter. Así por ejemplo:

```javascript
    const café = 1; // Codificado como caf\u{e9}
    const café = 2; // Codificado como cafe\u{301}
```

Las expresiones anteriores se ven iguales en un editor. Sin embargo, son variables con diferente nombre y así es como las ve JavaScript. Aunque tienen la misma representación, son identificadores distintos.

# El separador de sentencias
Para separar diferentes sentencias del lenguaje se utiliza el ';'. El uso de este terminador es opcional en algunos contextos:

- Cuando la siguiente sentencia empieza en la siguiente línea.
- Si la sentencia termina antes de un }

Pero vamos a considerar una buena práctica **terminar nuestras sentencias con ';', incluso cuando no se requiera**. 


