Las siguientes indicaciones de entrega servirán para todas las entregas de este módulo.

El contenido de las actividades estará recogido en un archivo de texto (.doc, .docx o .odt) o comprimido (.zip o .rar), con nombre del documento brs.utX.actY.nombre.apellidos.doc, siendo:
-	brs son las siglas del módulo
-	X el número de la unidad de trabajo que se está estudiando.
-	Y el número de la actividad.
-	Nombre del alumno.
-	Apellidos del alumno
El nombre del documento no deberá contener acentos.

Por ejemplo, siendo la entrega de unidad de trabajo 1, actividad 4 y alumno Jairo Fernández Alonso el documento a entregar sería brs.ut1.act4.jairofernandezalonso.doc.

El contenido del documento deberá de estar bien presentado, sin faltas de ortografía y manteniendo una estética correcta. Además, dentro del documento deberá de aparecer el nombre y apellido del alumno.

Si el contenido del documento ha sido obtenido de fuentes de Internet, deberá de ser indicado la url en la que se encontró.

Cualquier plagio de la tarea significará la calificación negativa de 0 tanto para el alumno que copia como para el alumno que ha dado la tarea, calificándose también con un 0 el Resultado de Aprendizaje que se trabaje en esta unidad para la evaluación continua.

Cualquier entrega que no cumpla las características indicadas no será corregida.


