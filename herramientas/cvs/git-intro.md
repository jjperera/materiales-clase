# Introducción

Generalmente, cuando estamos trabajando en un proyecto software, tenemos una jerarquía de directorios que contienen los archivos que forman parte de nuestro proyecto. Esto, si bien puede ser suficiente para proyectos pequeños o individuales, no hace sino complicar ciertos escenarios entre los que podríamos incluir algunos ejemplos, como la gestión de proyectos grandes donde podemos estar manteniendo varias versiones de un mismo programa y tenemos que hacer un seguimiento minucioso de los cambios u otros como el trabajo en equipo. 

Para satisfacer estas necesidades y otras contamos con los sistemas de control de versiones. Existen diferentes sistemas de control de versiones con diferentes enfoques, pero de algún modo, todos compartirán las siguientes características:

- Permiten hacer seguimiento de los cambios realizados a todos los archivos. Esto permite volver atrás, generar parches, analizar cambios llevacos a cabo en una determinada versión, saber quién ha realizado cada uno de los cambios, etc.
- Creación de ramas. Permite que se puedan seguir diferentes líneas de desarrollo y fusionarlas según sea necesario.

Para permitir llevar a cabo estas operaciones sobre el fuente de algún modo, el sistema de control de versiones asocia a un determinado directorio lo que podríamos considerar una base de datos que contiene toda esta información sobre nuestro código. Cómo se ha dicho anteriormente, existen diferentes enfoques. En nuestro caso, vamos a diferenciar entre dos:

- Sistemas de control de versiones centralizados, donde esta base de datos se encuentra en un servidor central. Un ejemplo de este sistema de control de versiones sería [subversion](https://subversion.apache.org/). 
- Sistema de control de versiones distribuidos. En estos scv, cada desarrollador tendría una copia local tanto del fuente como de esta base de datos con el historial de cambios. Luego, cada desarrollador puede fusionar sus cambios con otros repositorios. De este modo, varios desarrolladores pueden compartir un repositorio común donde van subiendo su trabajo. Un ejemplo de este sistema de control de versiones sería [git](https://git-scm.com/).

En este manual nos vamos a centrar en git. Si bien no vamos a profundizar mucho, vamos a ver lo que serían las operaciones esenciales que vamos a necesitar a lo largo del curso.

# Funcionamiento de git

En la siguiente imagen podemos ver el flujo de trabajo normal de GIT. En la imagen se distingue entre el trabajo en:

- **local**: cuando hacemos referenc a local, nos referimos a nuestra estación de trabajo. El ordenador donde estamos trabajando en el proyecto. En este ordenador es donde vamos a editar los archivos.
- **remoto**: El extremo remoto hace referencia a otro repositorio. Generalmente, será el repositorio en el servidor donde todos los desarrolladores enviarán su trabajo, aunque podríamos tener muchos remotos a donde enviar nuestros cambios.


![Funcionamiento de git](res/git-workflow.jpg)


En el diagrama se pueden ver además las principales operaciones que permiten mover nuestro trabajo entre las diferentes zonas donde se pueden encontrar. A continuación vamos a describir las diferentes zonas así como las operaciones que se pueden realizar.

- **Local**: Directorio de trabajo en mi ordenador. Al ser una copia local de mi repositorio contendrá, además del código fuente, toda la información relacionada con el repositorio.
  * **Working directory**: Este sería mi directorio de trabajo, mi proyecto, el código fuente. Trabajo con los archivos en mi proyecto con normlidad. Como haría con cualquier otro proyecto dentro de este directorio de trabajo, que sería el directorio del proyecto.
  * **Staging Area**: Es un área del repositorio donde podemos de algún modo ir añadiendo todos los cambios que queremos confirmar en nuestro siguiente commit. En este área podemos, por ejemplo, añadir el fichero *Prueba.java* utilizando el comando **add**. De algún modo, nos permite configurar un borrador de lo que queremos confirmar en el siguiente commit. 

    Al hacer un commit todo lo que tenemos en nuestra **Staging área** será guardado en el repositorio local. 
  * **Local repository**: Sería el repositorio en sí mismo. La base de datos con el histórico de cambios. 
- **Remoto**: El repositorio remoto será aquel donde estarán los cambios de todo el equipo de desarrolladores (normalmente). Con las operaciones pull y push podemos traernos los cambios subidos por otros desarrolladores a nuestro repositorio local o enviar los cambios al repositorio remoto.


Aunque son más operaciones las que pueden realizarse, nosotros vamos a centrarnos en este flujo de trabajo durante todo el curso. Este es el modo en que vamos a subir los trabajos.

# Herramientas
Del mismo modo que para gestionar una base de datos necesitamos un SGBD, para gestionar un repositorio necesitamos alguna herramienta. Para cada sistema de control de versiones puede haber diversas herramientas, pero lo que es importante es no confundir la herramienta con el sistema de control de versiones. Por ejemplo, en git, tenemos las siguientes alternativas:

- **Línea de comandos (CLI)**: Permite ejecutar todos los comandos sobre un repositorio desde la línea de comandos. Esta opción está disponible normalmente para todas las plataformas soportadas. Esta es la que podemos obtener desde [aquí](https://git-scm.com/downloads).
- **Interfaz gráfico (GUI)**: Programas para entorno gráfico que permiten llevar a cabo las operaciones con nuestro repositorio utilizando ratón y teclado. Existen multitud de altarnativas para los diferentes sistemas de control de versiones. Para git tenemos una lista de clientes gráficos en siguiente [enlace](https://git-scm.com/downloads/guis).
- **Entorno de desarrollo (IDE)**: En la actualidad, los entornos de desarrollo, además de permitir gestionar nuestro proyecto, permiten ejecutar las operaciones sobre el repositorio.
- **Webs**: Ejemplos de estos sitios web sería [github](https://github.com/) o [gitlab](https://about.gitlab.com/). Estos sitios web ofrecen una serie de servicios alrededor de este sistema de control de versiones.

De cualquier modo, sea cual sea el método utilizado para gestionar nuestro repositorio, por debajo siempre estará nuestro repositorio y el API o el CLI de git para controlar el acceso al mismo y **las operaciones serán siempre las mismas**. Así pues debemos evitar confundir lo que es git con la herramienta que estemos utilizando.


# Operaciones
Las operaciones esenciales que debemos conocer y que vamos a utilizar a lo largo del curso son:

## Obtener o crear un repositorio
Aquí vamos a tener dos operaciones:

### init
El comando [init](https://git-scm.com/docs/git-init) permite inicializar un repositorio en un directorio actual. Sería el equivalente a nuestro create database. Estaríamos creando un repositorio local vacío en el directorio de nuestro proyecto. 

   ```bash
   # 1. Entra el directorio del proyecto
   cd /home/user/proyecto
   
   # 2. Crea el repositorio local (no tenemos actualmente repositorio remoto).
   #    A partir de este momento se pueden ejecutar otros comandos de git
   git init 
   
   # 3. Añade el directorio actual al stagging area
   git add .

   # Confirma los cambios que pasarán al repositorio local. Deberemos escribir un mensaje de log
   git commit
```

### clone
El comando [clone](https://git-scm.com/docs/git-clone) hace una copia de un repositorio a un nuevo directorio. Además, deja configurado como remoto el repositorio origen. Este comando lo podemos utilizar para obtener una copia de trabajo local de un repositorio remoto.

   ```bash
   # 1. Accede al directorio donde quiere guardar el repositorio que va a clonar
   cd /home/user
   
   # 2. Copia el repositorio del núcleo Linux en el directorio "elnucleolinux"
   #    El directorio se creará
   git clone git://git.kernel.org/pub/scm/.../linux.git elnucleolinux
   
   # 3. Entra en el directorio 
   cd elnucleolinux

   # Compila el proyecto
   make
```

## Operaciones básicas con nuestro repositorio
En este apartado incluyo las operaciones básicas que deberemos realizar en nuestro trabajo diario. Las pongo en el que considero el orden lógico.

### status
El comando [status](https://git-scm.com/docs/git-status) podemos utilizarlo para conocer los cambios que hemos realizado en nuestro directorio de trabajo. Nos dirá por ejemplo, si tenemos archivos no versionados, cambiados o en el área de stagging.

   ```bash
   # 1. Accede al directorio del proyecto
   cd /home/user/miproyecto

   # 2. Obtiene información
   git status

   Salida del comando...
   ```

### diff
El comando [diff](https://git-scm.com/docs/git-diff) permite básicamente comparar ficheros. La diferencia con una simple comparación, es que podemos hacer comparaciones entre diferentes versiones de un fichero, así como el fichero de trabajo y diferentes versiones. Nos permite de algún modo analizar los cambios realizados. Por ejemplo, un uso normal sería comprobar que cambios he realzado antes de hacer un commit de modo que pueda confirmar que los cambios son correctos.

### add
[add](https://git-scm.com/docs/git-add) añade un fichero o directorio al área de stagging dejándolo listo para ser enviado al repositorio con el siguiente commit.

   ```bash
   # 1. Accede al directorio del proyecto
   cd /home/user/miproyecto

   # 2. Añade el archivo
   git add Prueba.java

   # 3. Añade el directorio y subdirectorios
   git add src/java

   ```


### reset
Podemos utilizar [reset](https://git-scm.com/docs/git-reset) para eliminar un archivo del stagging área. Esto puede darse cuando cambiemos de idea y no queramos que unos cambios entren en el siguiente commit. Para hacerlo podemos usar una sintaxis como la siguiente:

   ```bash
   # 1. Añade el fichero a la zona de preparación
   git add Prueba.java
   # 2. Elimina el archivo de la zona de preparación
   git reset -- Prueba.java
   ```

### commit
Finalmente, el comando [commit](https://git-scm.com/docs/git-commit) guardará los cambios en el repositorio.

   ```bash
   # 1. Envía los cambios al repositorio local
   git commit 
   ```

Se nos pedirá que introduzcamos un mensaje de log.

## Trabajando con repositorios remotos
Aquí, como vimos en la figura, hay operaciones que podemos utilizar para enviar cambios a un repositorio remoto o bien para traer cambios desde un remotro repositorio local. Un escenario donde podríamos necesitarlo sería un equipo de desarrollo trajando en un mismo proyecto. Aquí hay dos comandos que vamos a utilizar. Ambos pueden ser utilizados sin argumentos.

### push
Permite enviar nuestros cambios al repositorio remoto. Los demás podrían a partir de ese momento hacer pull para dercargarlos y fusionarlos con sus repositorios locales.

### pull
Utilizado para fusionar los cambios en otro repositorio con nuestro repositorio local. Lo utilizaremos para descargar los cambios realizados por otros que han sido enviados al servidor con el comando push.

# Conclusión
En este documento hemos revisado muy rápidamente algunas de las funciones de git que vamos a utilizar durante el curso. Para complementar esta información, añadiremos algunas actividades iniciales donde vamos a probar estos comandos haciendo una configuración inicial de nuestro entorno de trabajo.



