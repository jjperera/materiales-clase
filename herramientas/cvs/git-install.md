# Introducción
En este documento vamos a ver unos pasos básicos para instalar el CLI de git.

# Descarga e instalación
Para empezar, descargaremos el CLI de git desde la página [https://git-scm.com/downloads](https://git-scm.com/downloads) y llevaremos a cabo el proceso de instalación. Será suficiente con las opciones por defecto.

# Entorno
Para su correcto funcionamiento, deberemos asegurarnos de que el directorio de instalación está incluid en el PATH del sistema. Si lo tenemos añadido, podemos habrir un intérprete de comandos y ejecutar el comando git.

```bat
> git
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]
```

El instalador debería configurar el path por nosotros.

# Integración con la shell
Si hemos indicado al instalador que integre git con la shell, aparecerán en nuestro menú contextual del explorador de archivos las opciones en la captura de pantalla.

![Git shell](res/git-shell.jpg)

- **Git GUI Here**: Hace referencia a abrir un pequeño interfaz gráfico que apunte al directorio actual.
- **Git Bash Here**: Abre un intérprete de comandos configurado para lanzar comandos de git. Para utilizar el CLI, esta debería ser nuestra opción preferida, ya que garantizamos que nuestro intérprete de comandos estará correctamente configurado.


# El intérprete de comandos
Si abrimos el intérprete de comandos, nos encontraremos con una shell bash configurada para trabajar con git. En la capgtura por ejemplo, he lanzado un comando git status.

![Git bash](res/git-bash.jpg)