# Introducción
Son varios los entornos de desarrollo que podemos utilizar para llevar a cabo las tareas de desarrollo dependiendo de los lenguajes de programación y tecnologías utilizadas. En este módulo vamos a trabajar con tecnologías web. En principio, vamos a utilizar [Visual Studio Code de Microsoft](https://code.visualstudio.com/). 

# Descarga e instalación
La descarga la haremos desde la página web oficial. [Visual Studio Code de Microsoft](https://code.visualstudio.com/). Una vez descargado lo instalaremos. No tiene requisitos especiales. Es recomendable que marquéis las opciones:

- Añadir "abrir con code" a los archivos
- Añadir "abrir con code" a los directorios

# Paquete de idioma español
En este punto, tendríamos instalado el IDE en inglés. Si instalamos el [Spanish Languaje Pack for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=MS-CEINTL.vscode-language-pack-es) quedará traducido al castellano. Al acceder a la web, tenemos que tener ya instalado Visual Studio Code y abrir el enlace con dicho IDE. Hecho esto, deberemos reiniciarlo para aplicar los cambios.

# Complementos
Para facilitar la codificación, instalaremos los siguientes módulos:

- HTML CSS Support

# Pantalla principal
Una vez instalado, tendremos a la izquierda las principales opciones. En la siguiente captura se muestra el explorador de archivos. Este explorador permite acceder a las principales funciones.

![Visual Studio Code](res/vs-mainscreen.jpg)

De arriba a abajo, las opciones que aparecen son:

- **Explorador**: desde aquí tenemos acceso a los archivos. Podemos 
   - **Abrir una carpeta**, que para nosotros será el equivalente a un proyecto, o clonar un repositorio si no la tenemos ya clonada. De cualquier modo, tendremos una carpeta con los archivos que vamos a utilizar. 
   - **Clonar repositorio**: Podemos también obtener una copia local de un repositorio. VS Code tiene soporte para git.
- **Buscar**: Para llevar a cabo búsquedas en nuestro proyecto.
- **Control de versiones**: Nos ofrecerá acceso a funciones de control de versiones.
- **Depuración y Ejecución**: Esta nos permitirá ejecutar y depurar nuestros programas.
- **Extensiones**: Las extensiones nos van a permitir ampliar las funcionalidades del IDE.


