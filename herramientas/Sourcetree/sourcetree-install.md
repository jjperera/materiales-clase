# Introducción
Aunque no es imprescindible utilizar un interfaz gráfico de usuario, existen múltiples alternativas que podemos utilizar para gestionar un repositorio git. Escoger uno u otro dependerá de nuestros gustos y necesidades. Incluso puede que no nos sintamos cómodos con ninguno y recurramos al interfaz CLI. Lo ideal, en todo caso, sería escoger aquel que se adapta mejor a nuestro flujo de trabajo. 
Entre todas estas alternativas tenemos [Sourcetree](https://www.sourcetreeapp.com/) que va a ser el interfaz de usuario que vamos a utilizar a lo largo del curso. Ojo, ya que eso no lo convierte en el mejor ni mucho menos. Incluso es recomendable que lo comparéis con otros y veáis que todos tienen mucho en común, aunque también son muy diferentes.

# Descarga e instalación
1. Descargaremos el programa desde la página web [Sourcetree](https://www.sourcetreeapp.com/). 
2. Ejecutar el asistente de instalación
3. El primer paso es el registro. De momento lo vamos a saltar. Pincharemos en skip.

    ![registration](res/sc-registration.jpg)

4. En el siguiente paso del asistente, tendremos que seleccionar las herramientas para descargar y instalar (Pick tools to download ans install). 
   * Tendremos que **desmarcar la casilla Mercurial**.
   * En opciones avanzadas, marcar la primera casilla "configure automatic line..."

5. Finalmente, daremos nuestros datos. Pondremos nuestro nombre real completo y nuestra dirección de correo real. Estos datos serán utilizados en los commits para registrar quien ha realizado el cambio. 

6. Finalmente, ya que no tenemos una clave ssh que queramos que sea utilizada en nuestras conexiones, haremos clic en no.

# Pantalla principal
Una vez instalado, procederemos a ejecutarlo. Dispondremos de las siguientes opciones en la pantalla principal:

![pantalla principal](res/sc-mainscreen.jpg)

- Clone: Para clonar un repositorio remoto a una copia local.
- Add: Para añadir al programa una copia de trabajo local que tengamos en nuestro equipo.

Además, la opción local nos mostrará todos los repositorios que hemos añadido, de modo que nos sea relativamente sencillo acceder a cada uno de ellos.

# Acciones principales.
Finalmente, una vez abramos un repositorio, se nos mostrará una pantalla como la siguiente, donde tenemos disponibles todas las operaciones que podemos realizar sobre el repositorio. 

![Repositorio](res/sc-repo.jpg)

En clase repasaremos todas estas opciones.