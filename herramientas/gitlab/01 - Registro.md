# Introducción
[Gitlab](https://gitlab.com/) es una de las webs donde podemos tener nuestro repositorio en la nube sin coste. Nosotros vamos a utilizarla para almacenar nuestro repositorio. De este modo, tendremos sobre git la web para administrar de forma sencilla el acceso a nuestro proyecto. Además contaremos con las herramientas de colaboración montadas en torno a este sistema de control de versiones de modo que se puede utilizar como una oportunidad más para ver algunas de las herramientas que pueden utilizar otros desarrolladores en sus proyectos.

# Registro
El registro en gitlab no tiene nada de particular y es similar al proceso que podríamos seguir en cualquier otra web. 

## 1 Pulsar sobre Register now
En este enlace se abrirá el formulario para que introduzcamos nuestros datos. 

![Register now](res/gl-registernow.jpg)

## 2 Formulario de datos del usuario
En este formulario tendremos que introducir los datos requeridos para crear la cuenta. Es importante que:
- El nombre sea tu **nombre real**.
- Escribe tu nombre competo con apellidos del modo que se ve en la captura.

Escribir los datos de este modo ayudará al profesor a identificarte.

![User data](res/gl-userdata.jpg)

## 3 Indica tu perfil de usuario
Puedes seleccionar el perfil que quieras. Para el uso que vamos a hacer el perfil *Software Developer* estará bien.

## 4 Opciones iniciales.
Tras rellenar todos los formularios se habrá creado la cuenta y se mostrará una pantalla como la siguiente.

![Wellcome](res/gl-wellcome.jpg)

Desde aquí se nos invita a probar algunas de las opciones que haríamnos tras crear un usuario. En nuestro caso no vamos a seleccionar ninguna. **Antes de crear nuestro proyecto tendremos que ser invitados al grupo de alumnos por el profesor**.