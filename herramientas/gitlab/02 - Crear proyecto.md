# Introducción
Una vez que hayamos sido invitados al grupo de alumnos, podremos pasar a crear nuestro proyecto. Este proyecto es **donde vamos a trabajar a lo largo del curso**.

# Pasos para crear el proyecto correctamente
Para que el profesor tenga acceso al proyecto que creemos tenemos que seguir los siguientes pasos.
## Acceso a mis grupos
Accediendo al menú superior que vemos en la captura accederemos a la lista de grupos de los que soy miembro. Este grupo es importante, ya que va a simplificar compartir el proyecto con el profesor. Haciendo clic en el enlace "Your groups" veremos la lista que estamos buscando.

![Your groups](res/gl-yourgroups.jpg)

## 2 Seleccionar el grupo
El profesor indicará el grupo del que tenemos que ser miembros. En el ejemplo, el grupo es DAW2-2021-2022 indicando que será el grupo del segundo curso de desarrollo de aplicaciones web correspondiente al curso 2021/22. Ya que en este caso sería el grupo correcto, lo seleccionaremos para abrirlo.

![DAW2](res/gl-daw2.jpg)

## 3 Crear el proyecto
El proyecto lo vamos a crear dentro del grupo. Si vemos la captura, se nos muestra el nombre del grupo en la parte superior, la lista de proyectos en el grupo en la parte inferior y, en la parte superior derecha, un botón para crear un nuevo proyecto. Pincharemos en este botón para **crear un nuevo proyecto dentro del grupo**.


![New Project](res/gl-newproject.jpg)

## 4 Crear proyecto en blanco
Se nos ofrecerán diferentes opciones. En nuestro caso vamos a crear un proyecto en blanco, ya que esteo se adapta a lo que necesitamos.

![Create blank project](res/gl-createblank.jpg)

## 5 Datos del proyecto
Ahora se nos van a solicitar los datos del proyecto. Es importante que sigamos las siguientes instrucciones a la hora de rellenar los campos. He remarcado las cosas a las que debemos prestar atención. Si hay algún error podrá ser corregido posteriormente, pero es recomendable que intentemos hacerlo todo correctamente.

- **Project name**: Pondremos **todo en minúsculas** y **separado por guiones** daw2-<nombre1nombre2>-<apellido1>-<apellido2>. Por ejemplo, para el nombre Juan José Perera Villalba, el nombre tendría que quedar algo así como daw2-juanjose-perera-villalba
- El proyecto deberá estar marcado como **privado** aunque, al heredar de un grupo, la visibilidad va a ser la misma que tenga el grupo, de modo que, no vais a poder cambiar nada.
- **IMPORTANTE** aunque no imprescindible. Para que podáis hacer un clone del repositorio es importante que marquéis la opción **"Initialize repository with a README"**. Si no lo hacéis será necesario llevar a cabo una secuencia de pasos diferente a la que seguiremos en clase. 

## Create project
Una vez hagáis clic en "Create project" se creará el proyecto y estará listo para trabajar. De modo que con esto habremos terminado con la parte de la web. Si habéis seguido correctamente el proceso, deberíais poder localizar el proyecto accediendo a **Projects -> Your projects**.


